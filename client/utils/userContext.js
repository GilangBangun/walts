import { createContext, useState } from "react";

export const UserContext = createContext({});

export default function UserCtxWrap({ children }) {
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(true);
  return (
    <UserContext.Provider value={{ user, setUser, loading, setLoading }}>
      {children}
    </UserContext.Provider>
  );
}
