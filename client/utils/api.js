import axios from "axios";

export default () => {
  const rawUserData = localStorage.getItem("user");
  let token = "";
  if (rawUserData) {
    token = JSON.parse(rawUserData).token;
  }
  let headers = {};
  if (token !== "") {
    headers = {
      Authorization: token,
    };
  }
  return axios.create({
    // baseURL: "http://localhost:8080/api/",
    baseURL: "https://walts-waskita.herokuapp.com/api/",
    headers,
  });
};
