import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  Checkbox,
  Stack,
  Link,
  Button,
  Heading,
  Image,
  Center,
  Text,
  useColorModeValue,
} from '@chakra-ui/react';


const Login = () => {
  return (
    <Flex
      minH={'100vh'}
      align={'center'}
      justify={'center'}
      bg={useColorModeValue('gray.50', 'gray.800')}>
      <Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={6}>
        <Stack align={'center'}>
          <Image width='120px' objectFit='cover'
          src='https://2.bp.blogspot.com/-DpOJ5_Ys9VY/W9wVNkZ4UnI/AAAAAAAAPhk/yWHgfRj01vILBybH6ReX-zdzCPe1tGFewCLcBGAs/s1600/Waskita.png'
          />  
          <Heading fontSize={'4xl'} >
          Login to your account
          </Heading>
          <Text fontSize={'lg'} color={'gray.600'}>
            Selamat datang di <Link to="/" color={'blue.400'}>WALTS</Link> ✌️
          </Text>
        </Stack>
        <Box
          rounded={'lg'}
          bg={useColorModeValue('white', 'gray.700')}
          boxShadow={'lg'}
          p={8}>
          <Stack spacing={4}>
            <FormControl id="email">
              <FormLabel>Username</FormLabel>
              <Input type="email" />
            </FormControl>
            <FormControl id="password">
              <FormLabel>Password</FormLabel>
              <Input type="password" />
            </FormControl>
            <Stack spacing={10}>
              <Stack
                direction={{ base: 'column', sm: 'row' }}
                align={'start'}
                justify={'space-between'}>
                <Checkbox>Remember me</Checkbox>
                <Link color={'blue.400'}>Lupa Password?</Link>
              </Stack>
              <Button
              to="/"
                bg={'blue.400'}
                color={'white'}
                _hover={{
                  bg: 'blue.500',
                }}>
                Sign in
              </Button>
            </Stack>
          </Stack>
        </Box>
      </Stack>
    </Flex>
  );
  
}

export default Login