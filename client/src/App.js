import Login from "./components/Login";
import Footer from "./components/Footer";
import Navbar from "./components/Navbar";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

export default function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Navbar />} />
        <Route exact path="/login" element={<Login />} />
        {/* <Route path="*" element={<NotFound/>}/> */}
      </Routes>
      <Footer />
    </Router>
  );
}
