import React, { useEffect, useState, useContext ,forwardRef } from 'react';
import {Input, InputGroup, InputLeftElement, FormLabel, Button, IconButton, border } from "@chakra-ui/react";
import DashboardTable from "../../components/DashboardTable";
import api from "../../utils/api";
import { UserContext } from "../../utils/userContext";
import {SearchIcon, CalendarIcon} from "@chakra-ui/icons";

import "react-datepicker/dist/react-datepicker.css";
import DatePicker from 'react-datepicker';
import { relative } from 'path';

export default function Search() {
    const { user, loading } = useContext(UserContext);

    const checkRole = () => {
    if (!loading) {
        if (user?.role?.roleName !== "ADMIN") {
        // nanti sesuaikan
        // router.push("/");
        }
    }
    };

    useEffect(() => {
    checkRole();
    }, [loading]);

    
    const [dataList, setDataList] = useState([]);
    const [searchInput, setSearchInput] = useState('');
    const [filteredResults, setFilteredResults] = useState([]);

    const handleChange = e => {
        const target = e.target.value;
        setSearchInput(target);
        setSearch(true);
        console.log("inputnya : " + target);

        setStartDate("");
        setEndDate("");

        if (target !== '') {
            const filteredData = dataList.filter((item) => item.listPengguna[1].namePengguna.toLowerCase().includes(target.toLowerCase()) || item.kode.toLowerCase().includes(target.toLowerCase()));
            setFilteredResults(filteredData);
            filteredPerson(filteredData);
        }
        else{
            setFilteredResults(dataList);
            filteredPerson(dataList);
        }

    };

    const [pesanan1, setPesanan1] = useState([]);
    const [pesanan2, setPesanan2] = useState([]);
    const [pesanan3, setPesanan3] = useState([]);
    const [pesanan4, setPesanan4] = useState([]);
    const [pesanan5, setPesanan5] = useState([]);
    const [pesanan6, setPesanan6] = useState([]);
    
    function filteredPerson(data) {
      const data_filter1 = data.filter( (data2) => data2.status=== ("Menunggu Konfirmasi"));
      setPesanan1(data_filter1);
      setpesanan1_temp(data_filter1);
      setpesanan1Temp(data_filter1);
  //             console.log("FILTER");
  //             console.log(data_filter1);

      const data_filter2 = data.filter( (data2) => data2.status=== ("Proses Fabrikasi"));
      setPesanan2(data_filter2);
      setpesanan2_temp(data_filter2);
      setpesanan2Temp(data_filter2);

      const data_filter3 = data.filter( (data2) => data2.status=== ("Sudah Dikemas"));
      setPesanan3(data_filter3);
      setpesanan3_temp(data_filter3);
      setpesanan3Temp(data_filter3);

      const data_filter4 = data.filter( (data2) => data2.status=== ("Proses Pengiriman"));
      setPesanan4(data_filter4);
      setpesanan4_temp(data_filter4);
      setpesanan4Temp(data_filter4);

      const data_filter5 = data.filter( (data2) => data2.status=== ("Sudah Sampai"));
      setPesanan5(data_filter5);
      setpesanan5_temp(data_filter5);
      setpesanan5Temp(data_filter5);

      const data_filter6 = data.filter( (data2) => data2.status=== ("Selesai"));
      setPesanan6(data_filter6);
      setpesanan6_temp(data_filter6);
      setpesanan6Temp(data_filter6);
    };

    const getData = async () => {
        try {
            const { data } = await api().get(`pengguna/detail/${user?.idPengguna}/pesanan`);

            setDataList(data.result);
            filteredPerson(data.result);

        } catch (error) {
            console.log("error getting list");
        }
    };

    useEffect(() => {
        getData();
    }, []);

    
    const [pesanan1_temp, setpesanan1_temp] = useState([]);
    const [pesanan2_temp, setpesanan2_temp] = useState([]);
    const [pesanan3_temp, setpesanan3_temp] = useState([]);
    const [pesanan4_temp, setpesanan4_temp] = useState([]);
    const [pesanan5_temp, setpesanan5_temp] = useState([]);
    const [pesanan6_temp, setpesanan6_temp] = useState([]);

    const [pesanan1Temp, setpesanan1Temp] = useState([]);
    const [pesanan2Temp, setpesanan2Temp] = useState([]);
    const [pesanan3Temp, setpesanan3Temp] = useState([]);
    const [pesanan4Temp, setpesanan4Temp] = useState([]);
    const [pesanan5Temp, setpesanan5Temp] = useState([]);
    const [pesanan6Temp, setpesanan6Temp] = useState([]);

    const [startDate, setStartDate] = useState("");
    const [endDate, setEndDate] = useState("");
    
    
    const handleChangeDateStart = date =>{
        setStartDate(date)
        console.log("waktu mulai "+ date);
        setEndDate("");

        const data_filter1 = pesanan1.filter((item) => new Date(item.deadlinePengiriman) - date > 0 );
        setpesanan1_temp(data_filter1);
        setpesanan1Temp(data_filter1);

        const data_filter2 = pesanan2.filter((item) => new Date(item.deadlinePengiriman) - date > 0 );
        setpesanan2_temp(data_filter2);
        setpesanan2Temp(data_filter2);

        const data_filter3 = pesanan3.filter((item) => new Date(item.deadlinePengiriman) - date > 0 );
        setpesanan3_temp(data_filter3);
        setpesanan3Temp(data_filter3);

        const data_filter4 = pesanan4.filter((item) => new Date(item.deadlinePengiriman) - date > 0 );
        setpesanan4_temp(data_filter4);
        setpesanan4Temp(data_filter4);

        const data_filter5 = pesanan5.filter((item) => new Date(item.deadlinePengiriman) - date > 0 );
        setpesanan5_temp(data_filter5);
        setpesanan5Temp(data_filter5);

        const data_filter6 = pesanan6.filter((item) => new Date(item.deadlinePengiriman) - date > 0 );
        setpesanan6_temp(data_filter6);
        setpesanan6Temp(data_filter6);
    }

    const handleChangeDateEnd = date =>{
        setEndDate(date)
        console.log("waktu akhir "+ date);

        const data_filter1 = pesanan1_temp.filter((item) => date - new Date(item.deadlinePengiriman) > 0 );
        setpesanan1Temp(data_filter1);

        const data_filter2 = pesanan2_temp.filter((item) => date - new Date(item.deadlinePengiriman) > 0 );
        setpesanan2Temp(data_filter2);

        const data_filter3 = pesanan3_temp.filter((item) => date - new Date(item.deadlinePengiriman) > 0 );
        setpesanan3Temp(data_filter3);

        const data_filter4 = pesanan4_temp.filter((item) => date - new Date(item.deadlinePengiriman) > 0 );
        setpesanan4Temp(data_filter4);

        const data_filter5 = pesanan5_temp.filter((item) => date - new Date(item.deadlinePengiriman) > 0 );
        setpesanan5Temp(data_filter5);

        const data_filter6 = pesanan6_temp.filter((item) => date - new Date(item.deadlinePengiriman) > 0 );
        setpesanan6Temp(data_filter6);
    }

    const [search, setSearch] = useState(false);

    const ExampleCustomInput = forwardRef(({ value, onClick }, ref) => (
        <div className='flex-container'>
            <IconButton icon={<CalendarIcon w={5} h={5}/>} onClick={onClick} ref={ref} bgColor={'white'} color={'#3B628B'}/> 
            <p className='mt-5 mr-5'>{value}</p>
        </div>
    ));

 // <DashboardTable pesanan1={pesanan1} pesanan2={pesanan2} pesanan3={pesanan3} pesanan4={pesanan4} pesanan5={pesanan5} pesanan6={pesanan6} input={searchInput}/>

    return(
        <div>
            <div>
                <div>
                    <InputGroup >
                        <InputLeftElement className='InputLeft' pointerEvents="none"  children={<SearchIcon className='SearchIcon' color="gray.300" size="xs"/>}/>
                        <Input variant='flushed' backgroundColor={'white'} type = "search" placeholder = "Cari Pesanan Berdasarkan Nama Vendor atau Kode Pesanan" onChange = {handleChange} mb={3}/>
                    </InputGroup>
                </div>
                
                <div>
                {search ===true ?
                <div>
                    <FormLabel mb={2} fontSize={15} color="#3B628B"><center>Filter Pesanan Berdasarkan Waktu Deadline Pengiriman</center></FormLabel>
                    <div className='flex-container'>
                        <style>
                            {`
                                .date-picker {
                                border: 1px solid lightgrey;
                                border-radius: 5px;
                                background-color: white;
                                
                                }

                                .flex-container{
                                display: flex;
                                justify-content: center;
                                color: #3B628B;
                                }
                                
                                .mt-5{
                                    margin-top:5px;
                                }

                                .mr-5{
                                    margin-right:5px;
                                }
                            `}
                        </style>
                        <div>
                            <DatePicker  
                                wrapperClassName="date-picker"
                                dateFormat="dd/MM/yyyy"
                                selected={startDate}
                                onChange={handleChangeDateStart}
                                selectsStart
                                startDate={startDate}
                                endDate={endDate}
                                customInput={<ExampleCustomInput />}
                            />
                        </div>
                        <div>
                                <p className='mt-5 mr-5'>
                                    <strong>&nbsp;-</strong>
                                </p>
                        </div>
                        <div>
                            <DatePicker  
                                wrapperClassName="date-picker"
                                dateFormat="dd/MM/yyyy"
                                selected={endDate}
                                onChange={handleChangeDateEnd}
                                selectsEnd
                                startDate={startDate}
                                endDate={endDate}
                                minDate={startDate}
                                customInput={<ExampleCustomInput />}
                            />
                        </div>
                    </div>
                </div> :<></>
            }</div>
            </div>
            <br/>
            <DashboardTable pesanan1={pesanan1Temp} pesanan2={pesanan2Temp} pesanan3={pesanan3Temp} pesanan4={pesanan4Temp} pesanan5={pesanan5Temp} pesanan6={pesanan6Temp}/>
        </div>
        
    );

}

