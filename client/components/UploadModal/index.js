import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Button,
  FormControl,
  FormLabel,
  Input,
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
  VStack,
  Stack,
  Text,
  Radio,
  RadioGroup,
} from "@chakra-ui/react";
import { useEffect, useState, useContext } from "react";
import api from "/utils/api";
import { useRouter } from "next/router";
import { EditIcon, ArrowBackIcon, CheckIcon } from "@chakra-ui/icons";

export default function UploadModal(props) {
  const { idPesanan, statusPesanan } = props;
  console.log("STATUS PESANAN", statusPesanan);
  const router = useRouter();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [bukti, setBukti] = useState({
    name: "",
    lastModified: null,
    lastModifiedDate: null,
    size: null,
    type: "",
    webkitRelativePath: "",
  });
  const [catatan, setCatatan] = useState("");
  const [hasil, setHasil] = useState("");
  const [createObjectURL, setCreateObjectURL] = useState(null);
  const [message, setMessage] = useState("");
  const [statusPesanan2, setStatusPesanan2] = useState("");

  const nextStatusPesanan = () => {
    if (statusPesanan == "Menunggu Konfirmasi") {
      setStatusPesanan2("Proses Fabrikasi");
    } else if (statusPesanan == "Proses Fabrikasi") {
      setStatusPesanan2("Sudah Dikemas");
    } else if (statusPesanan == "Sudah Dikemas") {
      setStatusPesanan2("Proses Pengiriman");
    } else if (statusPesanan == "Proses Pengiriman") {
      setStatusPesanan2("Sudah Sampai");
    } else if (statusPesanan == "Sudah Sampai") {
      setStatusPesanan2("Selesai");
    }
  };

  useEffect(() => {
    nextStatusPesanan();
  }, []);

  const uploadToClient = (e) => {
    try {
      if (e.target.files && e.target.files[0]) {
        const i = e.target.files[0];

        setBukti(i);
        setCreateObjectURL(URL.createObjectURL(i));
        console.log(i);
        console.log(bukti);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const uploadToServer = async (e) => {
    console.log(idPesanan.idPesanan);
    try {
      e.preventDefault();
      console.log(bukti);
      console.log(catatan);
      console.log(hasil);
      const body = new FormData();
      body.append("file", bukti);
      body.append("catatan", catatan);
      body.append("hasil", hasil);
      console.log("id dalem", idPesanan.idPesanan);

      await api().post(`/file/${idPesanan.idPesanan}`, body);
      console.log("berhasil dikirim");
      onClose();
      alert("Status pesanan berhasil diubah!");
      router.reload();
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <>
      <Button
        style={{ backgroundColor: "#FDE65A" }}
        onClick={onOpen}
        leftIcon={<EditIcon boxSize={3} />}
      >
        Ubah Status
      </Button>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Ubah Status Pesanan</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <Text>
              Ubah status pesanan dengan mengunggah bukti validasi untuk{" "}
              <span style={{ color: "#74C365", fontWeight: "bold" }}>
                {statusPesanan2}
              </span>
            </Text>
            <br />
            <FormControl>
              <FormLabel>
                Bukti Validasi <span style={{ color: "red" }}>*</span>
              </FormLabel>
              <img src={createObjectURL} />
              <input
                type="file"
                name="myImage"
                onChange={(e) => setBukti(e.target.files[0])}
                required
              />
            </FormControl>

            {statusPesanan === "Sudah Sampai" ? (
              <FormControl mt={4}>
                <FormLabel>
                  Pesanan akan? <span style={{ color: "red" }}>*</span>
                </FormLabel>
                <RadioGroup defaultValue="Diterima">
                  <Stack spacing={5} direction="row">
                    <Radio
                      colorScheme="green"
                      value="Diterima"
                      onChange={(e) => setHasil(e.target.value)}
                    >
                      Diterima
                    </Radio>
                    <Radio
                      colorScheme="red"
                      value="Ditolak"
                      onChange={(e) => setHasil(e.target.value)}
                    >
                      Ditolak
                    </Radio>
                  </Stack>
                </RadioGroup>
              </FormControl>
            ) : null}

            <FormControl mt={4}>
              <FormLabel>Catatan</FormLabel>
              <Input
                placeholder="Catatan"
                onChange={(e) => setCatatan(e.target.value)}
              />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button
              mr={3}
              onClick={onClose}
              leftIcon={<ArrowBackIcon boxSize={3} />}
            >
              Kembali
            </Button>

            <Button
              colorScheme="blue"
              mr={3}
              type="submit"
              onClick={uploadToServer}
              leftIcon={<CheckIcon boxSize={3} />}
              style={{ backgroundColor: "#3B628B", color: "white" }}
            >
              Submit
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
