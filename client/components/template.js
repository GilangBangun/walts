import { ChakraProvider, ColorModeScript, Container } from "@chakra-ui/react";
import RoleCtxWrap from "../utils/userContext";
import Login from "./login.js";

function Template({ Component, pageProps }) {
  return (
    <RoleCtxWrap>
      <ChakraProvider>
        <Login>
          {/* page start here */}
          <Component {...pageProps} />
        </Login>
      </ChakraProvider>
    </RoleCtxWrap>
  );
}
export default Template;
