import React, { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import {
  Link,
  Button,
  FormControl,
  FormLabel,
  Flex,
  useColorModeValue,
  Stack,
  Container,
  Center,
  Alert,
  AlertIcon,
  AlertTitle,
  CloseButton,
  AlertDescription,
  Heading,
  StackDivider,
  Input,
  Select,
} from "@chakra-ui/react";
import ReactLoading from "react-loading";
import Navbar from "../Navbar";
import { UserContext } from "../../utils/userContext";
import api from "../../utils/api";

export default function FormPengguna(props) {
  const router = useRouter();

  const [message, setMessage] = useState("");
  const [data, setData] = useState({
    namePengguna: props.dataPost ? props.dataPost.namePengguna : "",
    phoneNumber: props.dataPost ? props.dataPost.phoneNumber : "",
    username: props.dataPost ? props.dataPost.username : "",
    password: props.dataPost ? props.dataPost.password : "",
    npwp: props.dataPost ? props.dataPost.npwp : "",
    role: props.dataPost ? props.dataPost.role : "",
  });
  const [role, setRole] = useState([]);

  const getRole = async () => {
    try {
      const { data } = await api().get("role/list");
      setRole(data?.result);
      console.log(data);
    } catch (error) {
      setMessage("error getting list");
    }
  };

  useEffect(() => {
    getRole();
  }, []);

  const handleChange = (e) => {
    setData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };
  const storeData = async (e) => {
    await fetch("https://walts-waskita.herokuapp.com/api/pengguna/", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((response) => response.json())
      .then((json) => console.log(json));

    router.push("/users");
  };

  const handlerRegister = async (e) => {
    e.preventDefault();
    const payload = {};
    for (let index = 0; index < e.target.length - 1; index++) {
      const { name, value } = e.target[index];
      payload[name] = value;
    }
    payload.counter = 0;

    try {
      await api().post("/pengguna/register", payload);
      setMessage("berhasil di buat");
      router.push("/users");
    } catch (error) {
      setMessage(error.response.data.message);
    }
  };

  const updateData = async (e) => {
    try {
      await fetch(
        `https://walts-waskita.herokuapp.com/pengguna/update/${props.dataPost.idPengguna}`,
        {
          method: "PUT",
          body: JSON.stringify(data),
          headers: {
            "Content-type": "application/json; charset=UTF-8",
          },
        }
      );
    } catch (error) {
      console.log(error);
    }

    router.push("/users");
  };

  const handleButton = (action) => {
    if (action === "add") {
      return (
        <Button
          borderRadius={0}
          type="submit"
          variant="solid"
          width="full"
          style={{backgroundColor:'#3B628B', color: 'white'}}
          onClick={handlerRegister}
        >
          Register
        </Button>
      );
    }
    if (action === "update") {
      return (
        <Button
          borderRadius={0}
          type="submit"
          variant="solid"
          width="full"
          style={{backgroundColor:'#3B628B', color: 'white'}}
          onClick={updateData}
        >
          Update
        </Button>
      );
    }
  };

  return (
    <form>
      <FormControl>
        <Stack spacing={5}>
          <div>
            <FormLabel>
              Nama <span style={{ color: "red" }}>*</span>
            </FormLabel>
            <Input
              name="namePengguna"
              defaultValue={data.namePengguna}
              onChange={handleChange}
              bg="white"
              color="black"
              type="text"
              isRequired
            />
          </div>
          <div>
            <FormLabel>
              Nomor Telepon <span style={{ color: "red" }}>*</span>
            </FormLabel>
            <Input
              name="phoneNumber"
              defaultValue={data.phoneNumber}
              onChange={handleChange}
              bg="white"
              color="black"
              type="number"
              isRequired
            />
          </div>

          <div>
            <FormLabel>
              Role <span style={{ color: "red" }}>*</span>
            </FormLabel>
            <Select bg="white" name="role" onChange={handleChange}>
              {role.map((it) => (
                <option Long defaultValue={it.idRole}>
                  {it?.roleName}
                </option>
              ))}
            </Select>
          </div>
          <div>
            <FormLabel>NPWP</FormLabel>
            <Input name="npwp" bg="white" color="black" type="text" />
            <br />
          </div>

          <div>
            <FormLabel>Username</FormLabel>
            <Input
              name="username"
              defaultValue={data.username}
              onChange={handleChange}
              bg="white"
              color="black"
              type="text"
              isRequired
            />
            <br />
          </div>

          <div>
            <FormLabel>
              Password <span style={{ color: "red" }}>*</span>
            </FormLabel>
            <Input
              name="password"
              defaultValue={data.password}
              onChange={handleChange}
              bg="white"
              color="black"
              type="password"
              isRequired
            />
          </div>
        </Stack>
      </FormControl>
      <br />
      {handleButton(props.action)}
    </form>
  );
}
