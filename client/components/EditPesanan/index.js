import { Dialog, Transition } from "@headlessui/react";
import React, { useState, useEffect, Fragment } from "react";
import { useRouter } from "next/router";
import {
  Link,
  Button,
  FormControl,
  FormLabel,
  Flex,
  useColorModeValue,
  Stack,
  Container,
  Center,
  Alert,
  AlertIcon,
  AlertTitle,
  CloseButton,
  AlertDescription,
  Heading,
  StackDivider,
  Input,
  Select,
} from "@chakra-ui/react";
import Navbar from "../../components/Navbar";
import api from "/utils/api";

import "react-datepicker/dist/react-datepicker.css";

import DatePicker from "react-datepicker";

function EditPesanan({ pesananUpdateData }) {
  console.log("dapet", pesananUpdateData);

  const router = useRouter();
  const [display, setDisplay] = React.useState("none");
  const [message, setMessage] = useState("");
  const [role, setRole] = useState([]);
  const [pesananUbah, setPesananUbah] = useState({
    catatan: "",
    deadlinePengemasan: null,
    deadlinePengiriman: null,
  });
  const [pesanan, setPesanan] = useState({
    catatan: "",
    deadlinePengemasan: null,
    deadlinePengiriman: null,
    hargaTotal: null,
    idPesanan: null,
    kode: "",
    listPengguna: [
      {
        idPengguna: "",
        namePengguna: "",
        npwp: "",
        password: "",
        phoneNumber: "",
        role: {
          idRole: null,
          roleName: "",
        },
        username: "",
      },
      {
        idPengguna: "",
        namePengguna: "",
        npwp: "",
        password: "",
        phoneNumber: "",
        role: {
          idRole: null,
          roleName: "",
        },
        username: "",
      },
      {
        idPengguna: "",
        namePengguna: "",
        npwp: "",
        password: "",
        phoneNumber: "",
        role: {
          idRole: null,
          roleName: "",
        },
        username: "",
      },
    ],
    nomorKontrak: null,
    status: "",
    waktuDibuat: null,
  });

  useEffect(() => {
    setPesanan(pesananUpdateData.result);
  }, [pesananUpdateData]);

  const handlerUpdate = async (e) => {
    e.preventDefault();

    const payload = {};
    for (let index = 0; index < e.target.length - 1; index++) {
      const { name, value } = e.target[index];
      payload[name] = value;
    }
    console.log(payload);
    payload.counter = 0;

    try {
      await api().put(`/pesanan/update/${pesanan.idPesanan}`, payload);
      setMessage("berhasil di buat");
      const ask = alert("Pesanan berhasil diupdate");
      router.push(`/pesanan/${pesanan.idPesanan} `);
    } catch (error) {
      console.log(error.response.data.message);
      const ask = alert("Pengguna gagal diupdate!");
      setMessage(error.response.data.message);
    }
  };

  const handleChange = (e) => {
    const value = e.target.value;
    setPesanan({ ...pesanan, [e.target.name]: value });
  };



  return (
    <div>
      <Navbar>
        <Heading as="h2" size="lg">
          <Center>Update Pesanan </Center>
          <Center> {pesanan.kode} </Center>


        </Heading>
        <br />
        <Container maxW="container.md">
          <form onSubmit={handlerUpdate}>
            <FormControl>
              <Stack spacing={5}>
                <div>
                  <FormLabel>Catatan</FormLabel>
                  <Input
                    name="catatan"
                    bg="white"
                    color="black"
                    type="text"
                    onChange={handleChange}
                    value={pesanan?.catatan}
                    isRequired
                  />
                  <br />
                </div>
              </Stack>
            </FormControl>
            <br />
            <Button
              borderRadius={0}
              type="submit"
              variant="solid"
              width="full"
              style={{backgroundColor:'#3B628B', color: 'white'}}
            >
              Update
            </Button>
          </form>
        </Container>
      </Navbar>
      )
    </div>
  );
}

export default EditPesanan;
