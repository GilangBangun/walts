import ReactLoading from "react-loading";
import { UserContext } from "/utils/userContext";
import api from "/utils/api";
import { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import {
  Link,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  HStack,
  Td,
  Heading,
  Button,
  Center,
  Box,
  ButtonGroup,
  Select,
  Stack,
  FormControl,
  FormLabel,
  Input,
  FormHelperText,
  Container,
  Alert,
  AlertIcon,
  AlertTitle,
  CloseButton,
} from "@chakra-ui/react";
import { AddIcon } from "@chakra-ui/icons";
import Navbar from "../../components/Navbar";
import FormPengguna from "../../components/FormPengguna";
import ButtonGroupPesanan from "../../components/ButtonGroup/ButtonGroup";
import React from "react";
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";

export default function FormPesanan(props) {
  const [vendor, setVendor] = useState([]);
  const [transporter, setTransporter] = useState([]);

  const [message, setMessage] = useState("");

  const { user, loading } = useContext(UserContext);

  const username = user?.idPengguna;

  const router = useRouter();

  const [deadlinePengiriman, setDeadlinePengiriman] = useState(new Date());
  const [deadlinePengemasan, setDeadlinePengemasan] = useState(new Date());

  const getTransporter = async () => {
    try {
      const { data } = await api().get("pengguna/all");
      const data_filter = data.result.filter(
        (data2) => data2.role.idRole === 4
      );

      setTransporter(data_filter);
    } catch (error) {
      console.log("error getting list");
    }
  };


  const getVendor = async () => {
    try {
      const { data } = await api().get("pengguna/all");
      const data_filter = data.result.filter(
        (data2) => data2.role.idRole === 3
      );

      setVendor(data_filter);
    } catch (error) {
      console.log("error getting list");
    }
  };

  useEffect(() => {
    getTransporter();
    getVendor();
  }, []);

  const getRole = async () => {
    try {
      const { data } = await api().get("role/list");
      setRole(data?.result);
    } catch (error) {
      setMessage("error getting list");
    }
  };

  const handlerPesanan = async (e) => {
    e.preventDefault();
    const payload = {};
    for (let index = 0; index < e.target.length - 1; index++) {
      const { name, value } = e.target[index];
      payload[name] = value;
    }
    payload.counter = 0;

    try {
      await api().post("/pesanan/create", payload);      
      const ask = alert("Pesanan berhasil dibuat!");
      router.push("/pesanan");
    } catch (error) {
      console.log(error.response.data.message);
    }
  };

  const [display, setDisplay] = React.useState("none");

  return (
    <div>
      <Heading as="h2" size="lg">
        <Center>Buat Pesanan Logistik</Center>
        <Center> {user?.namePengguna} </Center>
      </Heading>

      <br />

      <Container maxW="container.md">
        {/* <Alert status="error" display={display}>
              <AlertIcon />
              <AlertTitle mr={1}>{message}</AlertTitle>
              <CloseButton position="absolute" right="8px" top="8px" />
            </Alert> */}
        <form onSubmit={handlerPesanan}>
          <FormControl>
            <Stack spacing={5}>
              <div>
                <FormLabel>
                  Vendor <span style={{ color: "red" }}>*</span>
                </FormLabel>
                <Select bg="white" name="vendor">
                  {vendor.map((it) => (
                    <option string value={it.idPengguna}>
                      {it?.namePengguna}
                    </option>
                  ))}
                </Select>
              </div>

              <div>
                <FormLabel>
                  Transporter <span style={{ color: "red" }}>*</span>
                </FormLabel>
                <Select bg="white" name="transporter">
                  {transporter.map((it) => (
                    <option string value={it.idPengguna}>
                      {it?.namePengguna}
                    </option>
                  ))}
                </Select>
              </div>

              <div>
                <FormLabel>
                  Nomor Kontrak <span style={{ color: "red" }}>*</span>
                </FormLabel>
                <Input
                  name="nomorKontrak"
                  bg="white"
                  color="black"
                  type="text"
                  isRequired
                />
              </div>
              <div>
                <FormControl >
                  <FormLabel>Deadline Pengemasan</FormLabel>
                  <div>
                    <style>
                      {`.date-picker input {
                                width: 100%;
                                height: 40px;
                                padding-left: 17px;
                                border: 1px solid #E2E8F0;
                                border-radius: 5px;}
                            .date-picker focus {
                                border-color:#cd3232;
                            }
                            `}
                    </style>
                    <DatePicker
                      wrapperClassName="date-picker"
                      dateFormat="dd/MM/yyyy"
                      selected={deadlinePengemasan}
                      onChange={(date) => setDeadlinePengemasan(date)}
                      name="deadlinePengemasan"
                    />
                  </div>
                </FormControl>
              </div>

              <div>
                <FormControl >
                  <FormLabel>
                  Deadline Pengiriman <span style={{ color: "red" }}>*</span>


                  </FormLabel>
                  <div>
                    <style>
                      {`.date-picker input {
                              width: 100%;
                              height: 40px;
                              padding-left: 17px;
                              border: 1px solid #E2E8F0;
                              border-radius: 5px;}
                          .date-picker focus {
                              border-color:#cd3232;
                          }
                          `}
                    </style>
                    <DatePicker
                      wrapperClassName="date-picker"
                      dateFormat="dd/MM/yyyy"
                      selected={deadlinePengiriman}
                      onChange={(date) => setDeadlinePengiriman(date)}
                      name="deadlinePengiriman"
                    />
                  </div>
                </FormControl>
              </div>
              <div>
                <FormLabel>Catatan </FormLabel>
                <Input name="catatan" bg="white" color="black" type="text" />
                <br />
              </div>

              <div>
                <Input
                  name="pembuat"
                  bg="white"
                  color="black"
                  type="text"
                  value={username}
                  hidden
                />
              </div>

              <div>
                <Input
                  name="status"
                  bg="white"
                  color="black"
                  type="text"
                  value={"Menunggu Konfirmasi"}
                  hidden
                />
              </div>
            </Stack>
          </FormControl>
          <br />
          <Button
            borderRadius={0}
            type="submit"
            variant="solid"
            width="full"
            style={{backgroundColor:'#3B628B', color: 'white'}}
            onClick={() => setDisplay("")}
          >
            Buat Pesanan
          </Button>
        </form>
      </Container>
    </div>
  );
}
