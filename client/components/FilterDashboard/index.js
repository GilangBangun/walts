import * as React from "react";
import {
  Link,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  HStack,
  Td,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
  Heading,
  Button,
  useDisclosure,
  Center,
  Container,
  Input,
  InputGroup,
  InputLeftElement,
  FormLabel,
  Box,
  StackDivider,
  Stat,
  StatLabel,
  StatNumber,
  StatHelpText,
  StatArrow,
  VStack,
} from "@chakra-ui/react";
import { AddIcon } from "@chakra-ui/icons";
import { useState, useMemo, useEffect, useContext, useRef } from "react";
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";

import ReactLoading from "react-loading";
import { Pie, Bar, defaults } from "react-chartjs-2";
import Chart from "chart.js/auto";
import { ArcElement } from "chart.js";
import NumberFormat from "react-number-format";
import { Title } from "chart.js";

import PesananCard from "../PesananCard";

import api from "../../utils/api";
import { UserContext } from "../../utils/userContext";

export default function FilterDashboard({
  totalHarga,
  status,
  periode,
  kategori,
  jumlah,
  nominal,
}) {
  const { user, loading } = useContext(UserContext);
  const statusTitle = ["Progress", "Diterima", "Ditolak"];

  const checkRole = () => {
    if (!loading) {
      if (user?.role?.roleName !== "ADMIN") {
        // nanti sesuaikan
        // router.push("/");
      }
    }
  };

  useEffect(() => {
    checkRole();
  }, [loading]);

  return (
    <div>
      <div>
        <style></style>
      </div>
      <div className="scrollmenu">
        <br />

        <HStack align="flex-start">
          <Container maxW="container.lg">
            <Container paddingBottom={10}>
              <Center>
                <div>
                  <Box
                    p="6"
                    backgroundColor={"white"}
                    shadow="md"
                    borderRadius={"lg"}
                  >
                    <div>
                      <Stat textAlign={"center"}>
                        <StatLabel>Total Harga Pesanan</StatLabel>
                        <StatNumber>
                          <NumberFormat
                            value={totalHarga}
                            displayType={"text"}
                            thousandSeparator={true}
                            prefix={"Rp "}
                          />
                        </StatNumber>
                      </Stat>
                    </div>
                  </Box>
                </div>
              </Center>
            </Container>

            <VStack width="100%">
              <HStack divider={<StackDivider borderColor="gray.200" />}>
                <Box
                  p="6"
                  backgroundColor={"white"}
                  shadow="md"
                  borderRadius={"lg"}
                  width="100%"
                >
                  <div>
                    <Center>Status Pesanan</Center>
                    <br></br>
                  </div>
                  <div>
                    <Pie
                      data={{
                        labels: statusTitle,
                        datasets: [
                          {
                            label: "Jumlah Pesanan",
                            data: status,
                            backgroundColor: [
                              "rgba(255, 99, 132, 0.2)",
                              "rgba(54, 162, 235, 0.2)",
                              "rgba(255, 206, 86, 0.2)",
                              "rgba(75, 192, 192, 0.2)",
                              "rgba(153, 102, 255, 0.2)",
                              "rgba(255, 159, 64, 0.2)",
                            ],
                            borderColor: [
                              "rgba(255, 99, 132, 1)",
                              "rgba(54, 162, 235, 1)",
                              "rgba(255, 206, 86, 1)",
                              "rgba(75, 192, 192, 1)",
                              "rgba(153, 102, 255, 1)",
                              "rgba(255, 159, 64, 1)",
                            ],
                            borderWidth: 3,
                          },
                          // {
                          //   label: 'Quantity',
                          //   data: [47, 52, 67, 58, 9, 50],
                          //   backgroundColor: 'orange',
                          //   borderColor: 'red',
                          // },
                        ],
                        hoverOffset: 4,
                      }}
                      height={200}
                      width={400}
                      options={{
                        plugins: {
                          legend: {
                            position: "right",
                          },
                          datalabels: {
                            formatter: (value, context) => {
                              console.log("HELLO WORLD");
                              console.log(
                                "hasil AAA",
                                context.chart.data.datasets[0].data
                              );
                              const datapoints =
                                context.chart.data.datasets[0].data;
                              function totalSum(total, datapoint) {
                                return total + datapoint;
                              }
                              const totalvalue = datapoints.reduce(totalSum, 0);
                              const percentageValue = (
                                (value / totalvalue) *
                                100
                              ).toFixed(1);
                              if (percentageValue > 10) {
                                return `${percentageValue}%`;
                              } else {
                                return null;
                              }
                            },
                            color: "black",
                            labels: {
                              title: {
                                font: {
                                  weight: "bold",
                                },
                              },
                            },
                          },
                        },
                        maintainAspectRatio: false,
                      }}
                    />
                  </div>
                </Box>

                <Box
                  p="6"
                  backgroundColor={"white"}
                  shadow="md"
                  borderRadius={"lg"}
                  width="100%"
                >
                  <div>
                    <Center>Jumlah Pesanan Perbulan</Center>
                    <br></br>
                  </div>
                  <div>
                    <Bar
                      data={{
                        labels: periode[0],
                        datasets: [
                          {
                            label: "Jumlah Pemesanan",
                            data: periode[1],
                            backgroundColor: ["#3c648c"],
                            borderColor: ["#3c648c"],
                            borderWidth: 3,
                          },
                          // {
                          //   label: 'Quantity',
                          //   data: [47, 52, 67, 58, 9, 50],
                          //   backgroundColor: 'orange',
                          //   borderColor: 'red',
                          // },
                        ],
                        hoverOffset: 4,
                      }}
                      height={200}
                      width={400}
                      options={{
                        maintainAspectRatio: false,
                        legend: {
                          labels: {
                            fontSize: 25,
                            render: "label",
                          },
                        },
                      }}
                    />
                  </div>
                </Box>
              </HStack>

              {/* HORIZONTAL STACK 2 */}
              <HStack divider={<StackDivider borderColor="gray.200" />}>
                <Box
                  p="6"
                  backgroundColor={"white"}
                  shadow="md"
                  borderRadius={"lg"}
                  width="100%"
                >
                  <div>
                    <Center>Jumlah Kategori Produk</Center>
                    <br></br>
                  </div>
                  <div>
                    <Bar
                      data={{
                        labels: kategori,
                        datasets: [
                          {
                            label: "Jumlah Pesanan",
                            data: jumlah,
                            backgroundColor: ["#3c648c"],
                            borderColor: ["#3c648c"],
                            borderWidth: 3,
                          },
                          // {
                          //   label: 'Quantity',
                          //   data: [47, 52, 67, 58, 9, 50],
                          //   backgroundColor: 'orange',
                          //   borderColor: 'red',
                          // },
                        ],
                        hoverOffset: 4,
                      }}
                      height={200}
                      width={400}
                      options={{
                        maintainAspectRatio: false,
                        legend: {
                          labels: {
                            fontSize: 25,
                            render: "label",
                          },
                        },
                      }}
                    />
                  </div>
                </Box>

                <Box
                  p="6"
                  backgroundColor={"white"}
                  shadow="md"
                  borderRadius={"lg"}
                  width="100%"
                >
                  <div>
                    <Center>Total Pengeluaran Per Kategori</Center>
                    <br></br>
                  </div>
                  <div>
                    <Bar
                      data={{
                        labels: kategori,
                        datasets: [
                          {
                            label: "Total Harga",
                            data: nominal,
                            backgroundColor: ["#3c648c"],
                            borderColor: ["#3c648c"],
                            borderWidth: 3,
                          },
                          // {
                          //   label: 'Quantity',
                          //   data: [47, 52, 67, 58, 9, 50],
                          //   backgroundColor: 'orange',
                          //   borderColor: 'red',
                          // },
                        ],
                        hoverOffset: 4,
                      }}
                      height={200}
                      width={400}
                      options={{
                        maintainAspectRatio: false,
                        legend: {
                          labels: {
                            fontSize: 25,
                            render: "label",
                          },
                        },
                      }}
                    />
                  </div>
                </Box>
              </HStack>
            </VStack>

            <br></br>
            <br></br>
            {/* <div>
            <Box p='6' backgroundColor={"white"} shadow='md' borderRadius={"lg"} width="100%">

              <PesananTable columns={columns} data={listPesanan} />

              </Box>
            </div> */}
          </Container>
        </HStack>
      </div>
    </div>
  );
}
