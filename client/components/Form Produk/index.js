import ReactLoading from "react-loading";
import { UserContext } from "/utils/userContext";
import api from "/utils/api";
import { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import {
  Link,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  HStack,
  Td,
  Heading,
  Button,
  Center,
  Box,
  ButtonGroup,
  Select,
  Stack,
  FormControl,
  FormLabel,
  Input,
  FormHelperText,
  Container,
  Alert,
  AlertIcon,
  AlertTitle,
  CloseButton,
} from "@chakra-ui/react";
import { AddIcon } from "@chakra-ui/icons";
import Navbar from "../../components/Navbar";
import FormPengguna from "../../components/FormPengguna";
import ButtonGroupPesanan from "../../components/ButtonGroup/ButtonGroup";
import React from "react";

export default function FormProduk(props) {
  const [vendor, setVendor] = useState([]);
  const [pesanan, setPesanan] = useState([]);

  const [message, setMessage] = useState("");

  const { user, loading } = useContext(UserContext);

  const username = user?.idPengguna;

  const checkRole = () => {
    if (!loading) {
      if (
        user?.role?.roleName === "TRANSPORTER" ||
        user?.role?.roleName === "VENDOR"
      ) {
        router.push("/");
      }
    }
  };

  useEffect(() => {
    checkRole();
  }, []);

  const getRole = async () => {
    try {
      const { data } = await api().get("role/list");
      setRole(data?.result);      
    } catch (error) {
      setMessage("error getting list");
    }
  };

  const handlerPesanan = async (e) => {
    e.preventDefault();
    const payload = {};
    for (let index = 0; index < e.target.length - 1; index++) {
      const { name, value } = e.target[index];
      payload[name] = value;
    }
    payload.counter = 0;

    try {
      await api().post("/pesanan/create", payload);
      setMessage("berhasil di buat");
      router.push("/pesanan");
    } catch (error) {
      // setMessage(error.response.data.message);
    }
  };

  const [display, setDisplay] = React.useState("none");

  return (
    <div>
      <Heading as="h2" size="lg">
        <Center>Tambah Produk</Center>
        <Center> Pesanan {id} </Center>
      </Heading>

      <br />

      <Container maxW="container.md">
        <Alert status="error" display={display}>
          <AlertIcon />
          <AlertTitle mr={1}>{message}</AlertTitle>
          <CloseButton position="absolute" right="8px" top="8px" />
        </Alert>
        <form onSubmit={handlerPesanan}>
          <FormControl>
            <Stack spacing={5}>
              <div>
                <FormLabel>Vendor *</FormLabel>
                <Select bg="white" name="vendor">
                  {vendor.map((it) => (
                    <option string value={it.idPengguna}>
                      {it?.namePengguna}
                    </option>
                  ))}
                </Select>
              </div>

              <div>
                <FormLabel>Transporter *</FormLabel>
                <Select bg="white" name="transporter">
                  {transporter.map((it) => (
                    <option string value={it.idPengguna}>
                      {it?.namePengguna}
                    </option>
                  ))}
                </Select>
              </div>

              <div>
                <FormLabel>Nomor Kontrak *</FormLabel>
                <Input
                  name="nomorKontrak"
                  bg="white"
                  color="black"
                  type="number"
                  isRequired
                />
              </div>

              <div>
                <FormLabel>Catatan </FormLabel>
                <Input name="catatan" bg="white" color="black" type="text" />
                <br />
              </div>

              <div>
                <Input
                  name="pembuat"
                  bg="white"
                  color="black"
                  type="text"
                  value={username}
                />
              </div>

              <div>
                <Input
                  name="status"
                  bg="white"
                  color="black"
                  type="text"
                  value={"Menunggu Konfirmasi"}
                />
              </div>
            </Stack>
          </FormControl>
          <br />
          <Button
            borderRadius={0}
            type="submit"
            variant="solid"
            width="full"
            style={{backgroundColor:'#3B628B', color: 'white'}}
            onClick={() => setDisplay("")}
          >
            Buat Pesanan
          </Button>
        </form>
      </Container>
    </div>
  );
}
