import { useEffect, useState, useContext } from "react";
import { UserContext } from "../../utils/userContext";
import { useRouter } from "next/router";
import axios, { AxiosResponse } from "axios";
import { Fab } from "@material-ui/core";
import Badge from "@material-ui/core/Badge";
import { purple } from "@mui/material/colors";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import React, { ReactNode, ReactText } from "react";
import {
  IconButton,
  Avatar,
  Box,
  CloseButton,
  Flex,
  HStack,
  VStack,
  Icon,
  useColorModeValue,
  Link,
  Drawer,
  DrawerContent,
  Text,
  useDisclosure,
  BoxProps,
  FlexProps,
  Menu,
  MenuButton,
  MenuDivider,
  MenuItem,
  MenuList,
  Td,
  Button,
  Tr,
  Th,
  Divider,
  ListItem,
  List,
  Spacer,
  Image,
} from "@chakra-ui/react";
import {
  FiHome,
  FiTrendingUp,
  FiCompass,
  FiStar,
  FiSettings,
  FiMenu,
  FiBell,
  FiChevronDown,
} from "react-icons/fi";
import { IconType } from "react-icons";
import NumberFormat from "react-number-format";

interface LinkItemProps {
  name: string;
  icon: IconType;
  href: string;
  active: boolean;
}
let LinkItems: Array<LinkItemProps> = [];

function NavItem({ icon, children, ...rest }: NavItemProps) {
  const router = useRouter();

  return (
    <Link style={{ textDecoration: "none" }} _focus={{ boxShadow: "none" }}>
      <Flex
        align="center"
        p="4"
        mx="4"
        borderRadius="lg"
        role="group"
        cursor="pointer"
        _hover={{
          bg: "#D2DBE7",
        }}
        {...rest}
      >
        {icon && <Icon mr="4" fontSize="16" _groupHover={{}} as={icon} />}
        {children}
      </Flex>
    </Link>
  );
}

interface MobileProps extends FlexProps {
  onOpen: () => void;
}

function SidebarContent({ onClose, ...rest }: SidebarProps) {
  const router = useRouter();
  let { user, loading } = useContext(UserContext) as any;

  if (user?.role?.roleName === "STAFF LOGISTIK") {
    LinkItems = [
      { name: "Home", icon: FiHome, href: "/", active: true },
      {
        name: "Pesanan Logistik",
        icon: FiCompass,
        href: "/pesanan",
        active: false,
      },
      { name: "Vendor", icon: FiStar, href: "/users", active: false },
      {
        name: "Dashboard",
        icon: FiTrendingUp,
        href: "/dashboard",
        active: false,
      },
      {
        name: "Settings",
        icon: FiSettings,
        href: "/ubahPassword",
        active: false,
      },
    ];
  }
  if (user?.role?.roleName === "ADMIN") {
    LinkItems = [
      { name: "Home", icon: FiHome, href: "/", active: true },
      {
        name: "Pesanan Logistik",
        icon: FiCompass,
        href: "/pesanan",
        active: false,
      },
      { name: "Pengguna", icon: FiStar, href: "/users", active: false },
      {
        name: "Dashboard",
        icon: FiTrendingUp,
        href: "/dashboard",
        active: false,
      },
      {
        name: "Settings",
        icon: FiSettings,
        href: "/ubahPassword",
        active: false,
      },
    ];
  }
  if (
    user?.role?.roleName === "VENDOR" ||
    user?.role?.roleName === "TRANSPORTER"
  ) {
    LinkItems = [
      { name: "Home", icon: FiHome, href: "/", active: true },
      {
        name: "Settings",
        icon: FiSettings,
        href: "/ubahPassword",
        active: false,
      },
    ];
  }

  return (
    <Box
      transition="3s ease"
      bg={useColorModeValue("gray.200", "gray.900")}
      borderRight="1px"
      borderRightColor={useColorModeValue("gray.200", "gray.700")}
      w={{ base: "full", md: 60 }}
      textColor={"#3B628B"}
      pos="fixed"
      h="full"
      {...rest}
    >
      <Flex alignItems="center" mx="8" justifyContent="space-between">
        <Text
          fontSize="2xl"
          fontFamily="Poppins"
          fontWeight="medium"
          color={"#3B628B"}
        >
          <Image
            src="../../static/logo_walts.png"
            alt="WALTS"
            padding={3}
            width={300}
          />
        </Text>
        <CloseButton display={{ base: "flex", md: "none" }} onClick={onClose} />
      </Flex>
      {LinkItems.map((link) => (
        <NavItem
          key={link.name}
          icon={link.icon}
          onClick={() => router.push(link.href)}
          style={{
            backgroundColor: router.asPath === link.href ? "#3B628B" : "",
            color: router.asPath === link.href ? "white" : "",
          }}
        >
          {link.name}
        </NavItem>
      ))}
    </Box>
  );
}

export default function Navbar({ children }: { children: ReactNode }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <Box minH="100vh" bg={useColorModeValue("gray.100", "gray.900")}>
      <SidebarContent
        onClose={() => onClose}
        display={{ base: "none", md: "block" }}
      />
      <Drawer
        autoFocus={false}
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        returnFocusOnClose={false}
        onOverlayClick={onClose}
        size="full"
      >
        <DrawerContent>
          <SidebarContent onClose={onClose} />
        </DrawerContent>
      </Drawer>
      {/* mobilenav */}
      <MobileNav onOpen={onOpen} />
      <Box ml={{ base: 0, md: 60 }} p="4">
        {children}
      </Box>
    </Box>
  );
}

interface SidebarProps extends BoxProps {
  onClose: () => void;
}

interface NavItemProps extends FlexProps {
  icon: IconType;
  children: ReactText;
}

function MobileNav({ onOpen, ...rest }: MobileProps) {
  let { user, loading } = useContext(UserContext) as any;
  const router = useRouter();
  const namePengguna = user?.namePengguna;
  const rolePengguna = user?.role?.roleName;
  const idPengguna = user?.idPengguna;

  interface Notifikasi {
    idNotifikasi: string;
    isTriggered: boolean;
    konten: string;
    nameNotifikasi: string;
    tipe: string;
    waktuKirim: string;
  }

  const [listNotif, setListNotif] = useState([]);

  const getNotif = async () => {
    axios
      .get(
        `https://walts-waskita.herokuapp.com/api/pengguna/detail/${user?.idPengguna}/notif`
      )
      .then((response: AxiosResponse) => {
        setListNotif(response.data.result);
      });
  };

  useEffect(() => {
    getNotif();
  }, []);

  const closeNotif = (idNotifikasi) => {
    try {
      axios
        .put(
          `https://walts-waskita.herokuapp.com/api/pesanan/closeNotif/${idNotifikasi}`
        )
        .then((response: AxiosResponse) => {
          getNotif();
        });
    } catch (error) {
      console.log(error);
    }
  };

  const loppPost = (listNotif) => {
    if (listNotif) {
      return listNotif.map((it, index) => (
        <>
          <ListItem w="100%">
            <Box
              padding={2}
              paddingLeft={5}
              paddingRight={5}
              _hover={{ backgroundColor: "#e1e8f0" }}
            >
              <Flex>
                <Link
                  fontWeight="bold"
                  color="#3B628B"
                  href={`/pesanan/${it?.nameNotifikasi}`}
                  isExternal
                >
                  Pesanan {it?.kodePesanan}
                </Link>

                <Spacer />
                <CloseButton
                  onClick={() => closeNotif(it?.idNotifikasi)}
                  size="sm"
                />
              </Flex>

              <Text>{it?.konten}</Text>
              <small>{it?.waktuKirim}</small>
            </Box>
          </ListItem>
          <Divider />
        </>
      ));
    }
  };

  return (
    <Flex
      ml={{ base: 0, md: 60 }}
      px={{ base: 4, md: 4 }}
      height="20"
      alignItems="center"
      bg={useColorModeValue("#3B628B", "gray.900")}
      borderBottomWidth="1px"
      borderBottomColor={useColorModeValue("gray.200", "gray.700")}
      justifyContent={{ base: "space-between", md: "flex-end" }}
      {...rest}
    >
      <IconButton
        display={{ base: "flex", md: "none" }}
        onClick={onOpen}
        variant="outline"
        color={"white"}
        border="none"
        aria-label="open menu"
        icon={<FiMenu />}
      />

      <Text
        display={{ base: "flex", md: "none" }}
        color="white"
        fontSize="2xl"
        fontFamily="monospace"
        fontWeight="bold"
      >
        <Image
          src="../../static/logo_walts.png"
          alt="WALTS"
          padding={3}
          style={{ width: "150px" }}
        />
      </Text>

      <HStack spacing={{ base: "10", md: "100" }} margin={5}>
        <Flex alignItems={"center"} ml={10}>
          <Menu>
            <MenuButton>
              <HStack>
                <Button color="white" variant="extended">
                  <Badge color="error" badgeContent={listNotif.length}>
                    <FiBell color="" size="20px"></FiBell>
                  </Badge>
                </Button>
              </HStack>
            </MenuButton>
            <MenuList>
              <List>
                {listNotif.length != 0 ? (
                  loppPost(listNotif)
                ) : (
                  <ListItem>
                    <Box padding={2} paddingLeft={5} paddingRight={5}>
                      <Text align={"center"}>No Notification</Text>
                    </Box>
                  </ListItem>
                )}
              </List>
            </MenuList>
          </Menu>
        </Flex>
      </HStack>

      <HStack spacing={{ base: "0", md: "6" }}>
        <Flex alignItems={"center"}>
          <Menu>
            <MenuButton
              py={2}
              transition="all 0.3s"
              _focus={{ boxShadow: "none" }}
            >
              <HStack>
                <Avatar size={"sm"} src="../../static/user.png" />
                <VStack
                  display={{ base: "none", md: "flex" }}
                  alignItems="flex-start"
                  spacing="1px"
                  ml="2"
                >
                  <Text fontSize="sm" color={"white"}>
                    {" "}
                    {user?.namePengguna}{" "}
                  </Text>
                  <Text fontSize="xs" color="#FDE65A">
                    <b>{user?.role?.roleName}</b>
                  </Text>
                </VStack>
                <Box display={{ base: "none", md: "flex" }}>
                  <FiChevronDown color="white" />
                </Box>
              </HStack>
            </MenuButton>
            <MenuList
              bg={useColorModeValue("white", "gray.900")}
              borderColor={useColorModeValue("gray.200", "gray.700")}
            >
              <MenuItem onClick={() => router.push("/ubahPassword")}>
                Ubah password
              </MenuItem>
              {user?.role?.roleName !== "ADMIN" ? (
                <></>
              ) : (
                <MenuItem onClick={() => router.push("/masterData")}>
                  Master Data
                </MenuItem>
              )}
              <MenuDivider />
              <MenuItem onClick={() => router.push("/logout")}>
                Sign out
              </MenuItem>
            </MenuList>
          </Menu>
        </Flex>
      </HStack>
    </Flex>
  );
}
function api() {
  throw new Error("Function not implemented.");
}

function createColor(arg0: string) {
  throw new Error("Function not implemented.");
}
