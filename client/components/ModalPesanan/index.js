import ReactLoading from "react-loading";
import { UserContext } from "/utils/userContext";
import api from "/utils/api";
import { useState, useEffect, useContext, useRef, React } from "react";
import Router from "next/router";
import NumberFormat from "react-number-format";
import {
  Link,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  HStack,
  List,
  ListItem,
  ListIcon,
  Td,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
  Heading,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  Box,
  ModalCloseButton,
  ModalFooter,
  useDisclosure,
  Center,
  Container,
  Stack,
} from "@chakra-ui/react";
import { AddIcon, ExternalLinkIcon, ChevronRightIcon } from "@chakra-ui/icons";
import DeleteModal from "../../components/DeleteModal";
import UploadModal from "../UploadModal";
import ModalPenilaian from "../ModalPenilaian";

export default function ModalPesanan(idPesanan) {
  if (!idPesanan.idPesanan) {
    return null;
  }
  const [count, setCount] = useState(0);
  const { user, loading } = useContext(UserContext);
  const [role, setRole] = useState(user?.role?.roleName);

  const [showAll, setShowAll] = useState(false);
  const [currentIdx, setCurrentIdx] = useState(0);
  const [showCurrent, setShowCurrent] = useState(false);

  const [message, setMessage] = useState("");
  const { isOpen, onOpen, onClose } = useDisclosure();
  const finalRef = useRef();

  const [detail, setDetail] = useState([]);

  const [validasi, setValidasi] = useState({
    catatan: "",
    fileValidasi: "",
    idValidasi: null,
    statusPesanan: "",
    waktuValidasi: "",
  });

  const [produk, setProduk] = useState({
    deskripsi: "",
    harga: null,
    idProduk: null,
    jumlah: null,
    kategori: "",
    mutu: null,
    namaProduk: "",
    pesanan: {
      catatan: "",
      deadlinePengemasan: null,
      deadlinePengiriman: null,
      hargaTotal: 0,
      idPesanan: 0,
      kode: "",
      listPengguna: {
        idPengguna: "",
        namePengguna: "",
        npwp: "",
        password: "",
        phoneNumber: "",
        role: {
          idRole: null,
          username: "",
          roleName: {
            idRole: null,
            roleName: "",
          },
        },
      },
      nomorKontrak: null,
      status: "",
      waktuDibuat: null,
      volume: null,
    },
  });

  const [utama, setUtama] = useState({
    catatan: "",
    deadlinePengemasan: null,
    deadlinePengiriman: null,
    hargaTotal: null,
    idPesanan: null,
    kode: "",
    listPengguna: {
      idPengguna: "",
      namePengguna: "",
      npwp: "",
      password: "",
      phoneNumber: "",
      role: {
        idRole: null,
        roleName: "",
      },
      username: "",
    },
    nomorKontrak: null,
    status: "",
    waktuDibuat: null,
  });

  const [status, setStatus] = useState("");

  const DetailUtama = () => {
    return (
      <>
        <Box borderRadius="lg" w="100%" h="100%" p={4} padding={8}>
          <Table variant="simple" colorScheme={"facebook"}>
            <Tbody>
              <Tr>
                <Td color="black">Kode</Td>
                <Td color="black">{utama.kode}</Td>
              </Tr>
              <Tr>
                <Td color="black">Nomor Kontrak</Td>
                <Td color="black">{utama.nomorKontrak}</Td>
              </Tr>
              <Tr>
                <Td color="black">Vendor</Td>
                <Td color="black">{utama.listPengguna[1]?.namePengguna}</Td>
              </Tr>
              <Tr>
                <Td color="black">Harga Total</Td>

                <Td color="black">
                  <NumberFormat
                    value={utama.hargaTotal}
                    displayType={"text"}
                    thousandSeparator={true}
                    prefix={"Rp "}
                  />{" "}
                </Td>
              </Tr>

              <Tr>
                <Td color="black">Deadline Pengemasan</Td>
                {utama.deadlinePengemasan === null ? (
                  <Td color="black"> - </Td>
                ) : (
                  <Td color="black"> {utama.deadlinePengemasan}</Td>
                )}
              </Tr>
              <Tr>
                <Td color="black">Deadline Pengiriman</Td>
                {utama.deadlinePengiriman === null ? (
                  <Td color="black"> - </Td>
                ) : (
                  <Td color="black"> {utama.deadlinePengiriman}</Td>
                )}
              </Tr>
              <Tr>
                <Td color="black">Transporter</Td>
                <Td color="black">{utama.listPengguna[2]?.namePengguna}</Td>
              </Tr>
              <Tr>
                <Td color="black">PIC</Td>
                <Td color="black">{utama.listPengguna[0]?.namePengguna}</Td>
              </Tr>
              <Tr>
                <Td color="black">Catatan</Td>
                <Td color="black">{utama.catatan}</Td>
              </Tr>
            </Tbody>
          </Table>
        </Box>
      </>
    );
  };

  const DetailStatus = () => {
    return (
      <>
        <Box
          borderRadius="lg"
          w="100%"
          h="100%"
          p={4}
          color="white"
          padding={8}
        >
          <Table variant="simple" colorScheme="facebook">
            <Thead style={{ backgroundColor: "#3B628B" }}>
              <Tr>
                <Th style={{ color: "white" }}>Status</Th>
                <Th style={{ color: "white" }}>Bukti Validasi</Th>
                <Th style={{ color: "white" }}>Catatan</Th>
              </Tr>
            </Thead>
            <Tbody>{loopValidasi(validasi)}</Tbody>
          </Table>
        </Box>
      </>
    );
  };

  const DetailProduk = () => {
    return (
      <>
        <Box
          borderRadius="lg"
          w="100%"
          h="100%"
          p={4}
          color="white"
          padding={8}
        >
          <Table size="md" variant="simple" colorScheme="facebook">
            <Thead style={{ backgroundColor: "#3B628B" }}>
              <Tr>
                <Th style={{ color: "white" }}>Nama</Th>
                <Th style={{ color: "white" }}>Kategori</Th>
                <Th style={{ color: "white" }}>Satuan</Th>
                <Th style={{ color: "white" }}>Jumlah</Th>
                <Th style={{ color: "white" }}>Harga</Th>
              </Tr>
            </Thead>
            <Tbody>{loppPost(produk)}</Tbody>
          </Table>
        </Box>
      </>
    );
  };

  const toggleCurrent = () => {
    if (!showCurrent) {
      setShowCurrent(true);
      setShowAll(false);
      return;
    }
  };

  const loppPost = (produk) => {
    return produk.map((it, index) => (
      <Tr key={it.idProduk}>
        <Td color="black">{it.namaProduk}</Td>
        <Td color="black">{it.kategori}</Td>
        <Td color="black">{it.volume}</Td>
        <Td color="black">{it.jumlah}</Td>
        <Td color="black">
          <NumberFormat
            value={it.harga}
            displayType={"text"}
            thousandSeparator={true}
            prefix={"Rp "}
          />{" "}
        </Td>
      </Tr>
    ));
  };

  const loopValidasi = (validasi) => {
    return validasi.map((it, index) => (
      <Tr key={it.idProduk}>
        {it.statusPesanan === "Proses Fabrikasi" ? (
          <Td color="black">Proses Fabrikasi</Td>
        ) : (
          <></>
        )}

        {it.statusPesanan === "Sudah Dikemas" ? (
          <Td color="black">Sudah Dikemas</Td>
        ) : (
          <></>
        )}

        {it.statusPesanan === "Proses Pengiriman" ? (
          <Td color="black">Proses Pengiriman</Td>
        ) : (
          <></>
        )}

        {it.statusPesanan === "Sudah Sampai" ? (
          <Td color="black">Sudah Sampai</Td>
        ) : (
          <></>
        )}

        {it.statusPesanan === "Selesai" ? (
          <Td color="black">Selesai</Td>
        ) : (
          <></>
        )}

        <Td color="black">
          <Link href={it.fileValidasi} isExternal>
            Lihat Bukti <ExternalLinkIcon mx="2px" />
          </Link>
        </Td>
        <Td color="black">
          {it.catatan === "" || it.catatan === null ? (
            <p>-</p>
          ) : (
            <p>{it.catatan}</p>
          )}
        </Td>
      </Tr>
    ));
  };

  const detailUtamaDisplay = () => {
    setDetail("utama");
  };
  const detailProdukDisplay = () => {
    setDetail("produk");
  };
  const detailStatusDisplay = () => {
    setDetail("status");
  };

  useEffect(() => {
    getUtama();
    getProduk();
    getValidasi();
    getDetail();
  }, []);

  const getUtama = async () => {
    try {
      const { data } = await api().get(`pesanan/detail/${idPesanan.idPesanan}`);
      setUtama(data?.result);
      setStatus(utama.status);
    } catch (error) {
      setMessage("error getting list liin");
    }
  };

  const getValidasi = async () => {
    try {
      const { data } = await api().get(
        `pesanan/detail/${idPesanan.idPesanan}/bukti`
      );
      setValidasi(data?.result);
    } catch (error) {
      setMessage("error getting list masa liin");
    }
  };

  const getProduk = async () => {
    try {
      const { data } = await api().get(
        `pesanan/detail/${idPesanan.idPesanan}/produk`
      );

      setProduk(data?.result);
    } catch (error) {
      console.log("error getting list");
    }
  };

  const getDetail = async () => {
    const res = await fetch(
      `https://walts-waskita.herokuapp.com/api/pesanan/detail/${idPesanan.idPesanan}`
    );
    const resulz = await res.json();
    setDetail(resulz.result);
  };

  const ButtonPenilaian = () => {
    return (
      <div>
        <ModalPenilaian
          idVendor={utama.listPengguna[1]?.idPengguna}
          idPesanan={utama.idPesanan}
        />
      </div>
    );
  };

  const ButtonUpload = () => {
    return (
      <div>
        {role === "VENDOR" &&
        (utama.status === "Menunggu Konfirmasi" ||
          utama.status === "Proses Fabrikasi") ? (
          <UploadModal idPesanan={idPesanan} statusPesanan={utama.status} />
        ) : null}

        {role === "TRANSPORTER" &&
        (utama.status === "Sudah Dikemas" ||
          utama.status === "Proses Pengiriman") ? (
          <UploadModal idPesanan={idPesanan} statusPesanan={utama.status} />
        ) : null}

        {(role === "STAFF LOGISTIK" ||
          role === "MANAGER LOGISTIK" ||
          role === "ADMIN") &&
        utama.status === "Sudah Sampai" ? (
          <UploadModal idPesanan={idPesanan} statusPesanan={utama.status} />
        ) : null}
      </div>
    );
  };

  const handlerOpened = async (e) => {
    try {
      await api().put(`pesanan/updateOpened/${idPesanan.idPesanan}`);
    } catch (error) {
      console.log(error.message);
    }
  };

  if (isOpen) {
    if (count < 1) {
      detailUtamaDisplay();
      setCount(1);
    }
    handlerOpened();
  }

  return (
    <>
      <div>
        <Button
          mt={4}
          onClick={onOpen}
          px={4}
          style={{ backgroundColor: "#3B628B", color: "white" }}
          rightIcon={<ChevronRightIcon boxSize={3} />}
        >
          Detail
        </Button>
        <Modal finalFocusRef={finalRef} isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent minH={600} minW={1000}>
            <ModalCloseButton />
            <ModalBody>
              <Center>
                <Box
                  marginTop={10}
                  borderRadius="lg"
                  backgroundColor={"gray.200"}
                  w="100%"
                  h="100%"
                  p={4}
                  padding={8}
                >
                  <Box marginBottom={10}>
                    <Center>
                      <Heading as="h5" size="lg" color="black">
                        Pesanan {utama.kode}
                      </Heading>
                    </Center>
                  </Box>
                  <Center>
                    <Stack direction="row" spacing={4} align="center">
                      <Button
                        variant="link"
                        onClick={detailUtamaDisplay}
                        color="black"
                      >
                        Detail Pesanan
                      </Button>
                      <Button
                        variant="link"
                        onClick={detailProdukDisplay}
                        color="black"
                      >
                        Detail Produk
                      </Button>
                      <Button
                        variant="link"
                        onClick={detailStatusDisplay}
                        color="black"
                      >
                        Bukti Pengubahan Status
                      </Button>
                    </Stack>
                  </Center>
                  <div>
                    <div>
                      {showAll &&
                        info.map((el, i) => <p key={`content-${i}`}>{el}</p>)}
                    </div>
                    <div className="w-11/12 mx-auto mt-4 flex flex-wrap">
                      {detail === "utama" && <DetailUtama />}
                      {detail === "produk" && <DetailProduk />}
                      {detail === "status" && <DetailStatus />}
                    </div>
                    {showCurrent ? <div>{info[currentIdx]}</div> : null}
                  </div>
                </Box>
              </Center>
            </ModalBody>

            <ModalFooter>
              {utama.status !== "Selesai" ? (
                <ButtonUpload />
              ) : (
                <>
                  {role !== "VENDOR" && role !== "TRANSPORTER" ? (
                    <ButtonPenilaian />
                  ) : null}
                </>
              )}
            </ModalFooter>
          </ModalContent>
        </Modal>
      </div>
    </>
  );
}
