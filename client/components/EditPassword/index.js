import { Dialog, Transition } from "@headlessui/react";
import React, { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import {
  Link,
  Button,
  FormControl,
  FormLabel,
  Flex,
  useColorModeValue,
  Stack,
  Container,
  Center,
  Alert,
  AlertIcon,
  AlertTitle,
  CloseButton,
  AlertDescription,
  Heading,
  StackDivider,
  Input,
  Hide,
  Select,
} from "@chakra-ui/react";
import Navbar from "../../components/Navbar";
import api from "/utils/api";
import { UserContext } from "/utils/userContext";

function EditPassword({ penggunaUpdateData }) {
  const router = useRouter();
  const [display, setDisplay] = useState("none");
  const [message, setMessage] = useState("");
  const [login, setLogin] = useState(false);
  const { user, loading } = useContext(UserContext);

  const [role, setRole] = useState([]);
  const [pengguna, setPengguna] = useState({
    username: "",
    password: "",
    namePengguna: "",
    phoneNumber: "",
    role: {
      idRole: null,
      roleName: "",
    },
    npwp: "",
  });

  const [penggunaUbah, setPenggunaUbah] = useState({
    username: "",
    password: "",
    namePengguna: "",
    phoneNumber: "",
    role: {
      idRole: null,
      roleName: "",
    },
    npwp: "",
  });

  useEffect(() => {
    setPengguna(penggunaUpdateData);
  }, [penggunaUpdateData]);

  const getRole = async () => {
    try {
      const { data } = await api().get("role/list");
      setRole(data?.result);
    } catch (error) {
      setMessage("error getting list");
    }
  };

  useEffect(() => {
    getRole();
  }, []);

  const handlerUpdate = async (e) => {
    e.preventDefault();
    const payload = {};

    const payload2 = {
      username: pengguna.username,
      password: e.target[0].value,
    };

    try {
        const { data } = await api().post("/pengguna/login", payload2);

            if (e.target[1].value != e.target[2].value) {
              const ask = alert("Password baru dan konfirmasi password baru tidak sama");
            }

            else {
              for (let index = 0; index < e.target.length - 1; index++) {
                const { name, value } = e.target[index];
                if ( name !== "") {
                            payload[name] = value;
                }
                else {
                    payload["username"] = pengguna.username;
                }
              }
            }

          try {
            await api().put(
              `https://walts-waskita.herokuapp.com/api/pengguna/update/${pengguna.idPengguna}`,
              payload
            );
            setMessage("berhasil di buat");
            const ask = alert("Pengguna berhasil diupdate");
            router.push("/users");
          } catch (error) {
            console.log("data error", penggunaUbah);
            setMessage(error.response.data.message);
            console.log(error.response);
            }

    }
    catch (e) {
        setLogin(false);
        const ask = alert("Password lama Anda salah");
        console.log("terdapat error");
        setMessage("Password lama Anda salah");
    }
  };

  const handleChange = (e) => {
    const value = e.target.value;
    setPengguna({ ...pengguna, [e.target.name]: value });
  };

  return (
    <div>
      <Navbar>
        <Heading as="h2" size="lg">
          <Center>Ubah Password {pengguna.namePengguna}</Center>
        </Heading>
        <br />
        <Container maxW="container.md">
          <form onSubmit={handlerUpdate}>
            <FormControl>
              <Stack spacing={5}>
                <div>
                  <FormLabel>
                    Password lama <span style={{ color: "red" }}>*</span>
                  </FormLabel>
                  <Input
                    name="passwordLama"
                    bg="white"
                    color="black"
                    type="password"
                    isRequired
                  />
                </div>

                <div>
                  <FormLabel>
                    Password baru <span style={{ color: "red" }}>*</span>
                  </FormLabel>
                  <Input
                    name="passwordKonfirmasi"
                    bg="white"
                    color="black"
                    type="password"
                    isRequired
                  />
                </div>

                <div>
                  <FormLabel>
                    Konfirmasi Password baru{" "}
                    <span style={{ color: "red" }}>*</span>
                  </FormLabel>
                  <Input
                    name="password"
                    bg="white"
                    color="black"
                    type="password"
                    onChange={handleChange}
                    isRequired
                  />
                </div>
              </Stack>
            </FormControl>
            <br />
            <Button
              borderRadius={0}
              type="submit"
              variant="solid"
              width="full"
              style={{backgroundColor:'#3B628B', color: 'white'}}
            >
              Ubah
            </Button>

              <div>
                  <Input
                    name="namePengguna"
                    type="hidden"
                    value={pengguna.namePengguna}
                  />
                </div>

                <div>
                  <Input
                    name="phoneNumber"
                    type="hidden"
                    value={pengguna.phoneNumber}
                  />
                </div>

                <div>
                  <input
                    type="hidden"
                    /*name="action"*/
                    name="role"
                    value={pengguna.role.idRole}
                  />
                </div>

                <div>
                  <Input
                    name="npwp"
                    type="hidden"
                    value={pengguna.npwp}
                  />
                </div>

                <div>
                  <Input
                    name="username"
                    type="hidden"
                    value={pengguna.username}
                  />
                </div>

          </form>
        </Container>
      </Navbar>
      )
    </div>
  );
}

export default EditPassword;
