import React from "react";
import { ButtonGroup, Button } from "@chakra-ui/react";
import { useState } from "react";

const ButtonGroupPesanan = ({ buttons }) => {
    const [clickedId, setClickedId] = useState(-1);

    const handleClick = (event, id) => {
        setClickedId(id);
        doSomethingAfterClick(event);
      };

  return (
    <> 
    
    {buttons.map((buttonLabel, i) => (
         <Button 
         spacing={4}
         key={i} 
         name={buttonLabel} 
         variant="link" 
         onClick={() => setClickedId(i)}
         className={i === clickedId ? "customButton active" : "customButton"}
         style={{ padding:10, marginHorizontal:10}}
         > 
         
         {buttonLabel}</Button>
      ))}
    
    </>
)
};

export default ButtonGroupPesanan;

