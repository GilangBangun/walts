import { Dialog, Transition } from "@headlessui/react";
import React, { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import {
  Link,
  Button,
  FormControl,
  FormLabel,
  Flex,
  useColorModeValue,
  Stack,
  Container,
  Center,
  Alert,
  AlertIcon,
  AlertTitle,
  CloseButton,
  AlertDescription,
  Heading,
  StackDivider,
  Input,
  Hide,
  Select,
} from "@chakra-ui/react";
import Navbar from "../../components/Navbar";
import api from "/utils/api";
import { UserContext } from "/utils/userContext";

function EditPengguna({ penggunaUpdateData }) {
  const router = useRouter();
  const [display, setDisplay] = React.useState("none");
  const [message, setMessage] = useState("");
  const { user, loading } = useContext(UserContext);

  const [role, setRole] = useState([]);
  const [pengguna, setPengguna] = useState({
    username: "",
    password: "",
    namePengguna: "",
    phoneNumber: "",
    role: {
      idRole: null,
      roleName: "",
    },
    npwp: "",
  });

  const [penggunaUbah, setPenggunaUbah] = useState({
    username: "",
    password: "",
    namePengguna: "",
    phoneNumber: "",
    role: {
      idRole: null,
      roleName: "",
    },
    npwp: "",
  });

  useEffect(() => {
    setPengguna(penggunaUpdateData.result);
    console.log("huhu", penggunaUbah);
  }, [penggunaUpdateData]);

  const getRole = async () => {
    try {
      const { data } = await api().get("role/list");
      setRole(data?.result);
    } catch (error) {
      setMessage("error getting list");
    }
  };

  const checkRole = () => {
    if (!loading) {
      if (user?.role?.roleName === "STAFF LOGISTIK") {
        router.push("/");
      }

      if (user?.role?.roleName === "VENDOR") {
        router.push("/");
      }

      if (user?.role?.roleName === "TRANSPORTER") {
        router.push("/");
      }
    }
  };

  useEffect(() => {
    checkRole();
    getRole();
  }, []);

  const handlerUpdate = async (e) => {
    e.preventDefault();

    const payload = {};
    for (let index = 0; index < e.target.length - 1; index++) {
      const { name, value } = e.target[index];
      payload[name] = value;
    }
    payload.counter = 0;

    try {
      await api().put(
        `https://walts-waskita.herokuapp.com/api/pengguna/update/${pengguna.idPengguna}`,
        payload
      );
      setMessage("berhasil di buat");
      const ask = alert("Pengguna berhasil diupdate");
      router.push("/users");
    } catch (error) {
      const ask = alert("Pengguna gagal diupdate, silahkan ulangi.");
      setMessage(error.response.data.message);
      console.log(error.response);
    }
  };

  const handleChange = (e) => {
    const value = e.target.value;
    setPengguna({ ...pengguna, [e.target.name]: value });
  };

  return (
    <div>
      <Navbar>
        <Heading as="h2" size="lg">
          <Center>Update Pengguna {pengguna.namePengguna}</Center>
        </Heading>
        <br />
        <Container maxW="container.md">
          <form onSubmit={handlerUpdate}>
            <FormControl>
              <Stack spacing={5}>
                <div>
                  <FormLabel>
                    Nama <span style={{ color: "red" }}>*</span>
                  </FormLabel>
                  <Input
                    name="namePengguna"
                    bg="white"
                    color="black"
                    type="text"
                    onChange={handleChange}
                    isRequired
                    value={pengguna?.namePengguna}
                  />
                </div>
                <div>
                  <FormLabel>
                    Nomor Telepon <span style={{ color: "red" }}>*</span>
                  </FormLabel>
                  <Input
                    name="phoneNumber"
                    bg="white"
                    color="black"
                    type="number"
                    onChange={handleChange}
                    value={pengguna?.phoneNumber}
                    isRequired
                  />
                </div>
                {/* <div>
                  <FormLabel>
                    Role <span style={{ color: "red" }}>*</span>
                  </FormLabel>
                  <Input
                    name="role"
                    bg="white"
                    color="black"
                    type="number"
                    onChange={handleChange}
                    value={pengguna?.role.idRole}
                    isRequired
                  />
                </div> */}

                {/* <div>
                  <FormLabel>
                    Role <span style={{ color: "red" }}>*</span>
                  </FormLabel>
                  <Select
                    bg="white"
                    name="role"
                    onChange={handleChange}
                    value={pengguna?.role.idRole}
                    isRequired
                    disabled
                  >
                    {role.map((it) => (
                      <option Long value={it.idRole}>
                        {it?.roleName}
                      </option>
                    ))}
                  </Select>
                </div> */}

                <div>
                  <input
                    type="hidden"
                    /*name="action"*/
                    name="role"
                    value={pengguna.role.idRole}
                  ></input>
                </div>

                <div>
                  <FormLabel>NPWP</FormLabel>
                  <Input
                    name="npwp"
                    bg="white"
                    color="black"
                    type="text"
                    onChange={handleChange}
                    value={pengguna?.npwp}
                  />
                  <br />
                </div>

                <div>
                  <FormLabel>
                    Username <span style={{ color: "red" }}>*</span>
                  </FormLabel>
                  <Input
                    name="username"
                    bg="white"
                    color="black"
                    type="text"
                    onChange={handleChange}
                    value={pengguna.username}
                    isRequired
                    disabled
                  />
                  <br />
                </div>

                {/* <div>
                  <input
                    type="hidden"
                    name="action"
                    name="username"
                    value={pengguna.username}
                  ></input>
                </div> */}

                <div>
                  <FormLabel>
                    Password <span style={{ color: "red" }}>*</span>
                  </FormLabel>
                  <Input
                    name="password"
                    bg="white"
                    color="black"
                    type="password"
                    onChange={handleChange}
                  />
                </div>
              </Stack>
            </FormControl>
            <br />
            <Button
              borderRadius={0}
              type="submit"
              variant="solid"
              width="full"
              style={{backgroundColor:'#3B628B', color: 'white'}}
            >
              Update
            </Button>
          </form>
        </Container>
      </Navbar>
      )
    </div>
  );
}

export default EditPengguna;
