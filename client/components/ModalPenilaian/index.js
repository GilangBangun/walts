import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Button,
  FormControl,
  FormLabel,
  Input,
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
  VStack,
  Stack,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
} from "@chakra-ui/react";
import { useEffect, useState, useContext, React } from "react";
import api from "/utils/api";
import { useRouter } from "next/router";
import { StarIcon } from "@chakra-ui/icons";

export default function ModalPenilaian(props) {
  const { idVendor, idPesanan } = props;
  const router = useRouter();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [message, setMessage] = useState("");
  const [display, setDisplay] = useState("none");

  const handlerPenilaian = async (e) => {
    e.preventDefault();
    const payload = {};
    for (let index = 0; index < e.target.length - 1; index++) {
      const { name, value } = e.target[index];
      payload[name] = value;
    }
    payload.counter = 0;

    try {
      await api().post(`/penilaian/${idVendor}/${idPesanan}`, payload);
      alert("Penilaian berhasil dikirimkan!");
      router.reload();
    } catch (error) {
      setMessage(error.response.data.message);
      alert("Penilaian gagal dikirimkan, silahkan ulangi!");
    }
  };

  return (
    <>
      <Button
        style={{ backgroundColor: "#FDE65A" }}
        onClick={onOpen}
        leftIcon={<StarIcon />}
      >
        Nilai Vendor
      </Button>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Nilai Vendor</ModalHeader>
          <ModalCloseButton />
          <form onSubmit={handlerPenilaian}>
            <ModalBody pb={6}>
              <FormControl>
                <div>
                  <FormLabel>
                    Nilai <span style={{ color: "red" }}>*</span>
                  </FormLabel>
                  <NumberInput
                    name="nilai"
                    step={5}
                    defaultValue={80}
                    min={0}
                    max={100}
                  >
                    <NumberInputField />
                    <NumberInputStepper>
                      <NumberIncrementStepper />
                      <NumberDecrementStepper />
                    </NumberInputStepper>
                  </NumberInput>
                </div>
                <br />
                <div>
                  <FormLabel>Komentar </FormLabel>
                  <Input
                    name="feedback"
                    bg="white"
                    color="black"
                    type="text"
                    placeholder="Komentar terhadap vendor/pesanan"
                  />
                  <br />
                </div>
              </FormControl>
            </ModalBody>
            <br />
            <ModalFooter>
              <Button
                type="submit"
                variant="solid"
                width="full"
                style={{ backgroundColor: "#3B628B", color: "white" }}
                onClick={() => setDisplay("")}
              >
                Submit
              </Button>
            </ModalFooter>
          </form>
        </ModalContent>
      </Modal>
    </>
  );
}
