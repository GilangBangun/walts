import { useEffect, useContext } from "react";
import { UserContext } from "../utils/userContext";
import api from "../utils/api";
import ReactLoading from "react-loading";
import { useRouter } from "next/router";
import { Container } from "@chakra-ui/react";

import React from "react";

export default function Login({ children }) {
  const { user, loading, setUser, setLoading } = useContext(UserContext);
  const router = useRouter();
  useEffect(() => {
    getUser();
  }, []);

  const getUser = async () => {
    try {
      const { data } = await api().get("pengguna/self");
      setUser(data?.result);
    } catch (error) {
      if (error.response?.status !== 403) {
        console.log(error);
      }
    }
    setLoading(false);
  };

  const handleAuth = () => {
    console.log("username: ", user?.username);
    if (user?.username === undefined && router.pathname != "/login") {
      router.push("/login");
      console.log("masuk redirect login");
    }
    return children;
  };

  return (
    <>
      {loading ? (
        <div>
          <Container
            maxW="container.xl"
            marginTop={10}
            centerContent
            minHeight="80vh"
          >
            <h1>Loading</h1>
            <ReactLoading type="spin" />
          </Container>
        </div>
      ) : (
        handleAuth()
      )}
    </>
  );
}
