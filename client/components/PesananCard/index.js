import Image from "next/image";
import {
  Box,
  Center,
  Heading,
  Text,
  Stack,
  Avatar,
  useColorModeValue,
  Badge,
  Flex,
  Spacer,
} from "@chakra-ui/react";
import { CalendarIcon } from "@chakra-ui/icons";
import api from "../../utils/api";
import ModalPesanan from "../ModalPesanan";
import { useEffect, useRef, useContext } from "react";

const PesananCard = (props) => {
  const {
    vendor,
    kode,
    catatan,
    pic,
    nomorKontrak,
    idPesanan,
    isUpdate,
    isLatePengiriman,
    deadlinePengiriman,
    latePengiriman,
    isRejected,
  } = props;

  if (deadlinePengiriman != null) {
    const current = new Date();
    const date = `${current.getDate()}/${
      current.getMonth() + 1
    }/${current.getFullYear()}`;
    const pengiriman = new Date(deadlinePengiriman);

    const a = new Date();
    const b = new Date(deadlinePengiriman);
  }

  return (
    <Center py={3}>
      <Box
        maxW={"400px"}
        w={"full"}
        bg={useColorModeValue("white", "gray.900")}
        boxShadow={"xl"}
        rounded={"md"}
        p={4}
        overflow={"hidden"}
      >
        <Stack>
          <Flex>
            <Text
              color={"#FD5B68"}
              textTransform={"uppercase"}
              fontWeight={800}
              fontSize={"sm"}
              letterSpacing={1.1}
            >
              {vendor}
            </Text>
            <Spacer />
            <Box>
              {isUpdate === true ? (
                <Badge ml="15" colorScheme="blue">
                  New
                </Badge>
              ) : (
                <></>
              )}
            </Box>
            <Box>
              {latePengiriman >= 1 && latePengiriman < 3 ? (
                <Badge ml="15" colorScheme="yellow">
                  TELAT
                </Badge>
              ) : (
                <></>
              )}

              {latePengiriman >= 3 && latePengiriman < 5 ? (
                <Badge ml="15" colorScheme="orange">
                  TELAT
                </Badge>
              ) : (
                <></>
              )}
              {latePengiriman >= 5 ? (
                <Badge ml="15" colorScheme="red">
                  TELAT
                </Badge>
              ) : (
                <></>
              )}

              {isLatePengiriman === true ? (
                <Badge ml="15" colorScheme="red">
                  Pengiriman Terlambat
                </Badge>
              ) : (
                <></>
              )}
            </Box>
            <Box>
              {isRejected === true ? (
                <Badge ml="15" colorScheme="red">
                  DITOLAK
                </Badge>
              ) : (
                <></>
              )}

              {isRejected === false ? (
                <Badge ml="15" colorScheme="green">
                  DITERIMA
                </Badge>
              ) : (
                <></>
              )}
            </Box>
          </Flex>
        </Stack>
        <Stack spacing={2}>
          <Heading
            color={useColorModeValue("gray.700", "white")}
            fontSize={"2xl"}
            fontFamily={"body"}
          >
            {kode}
          </Heading>
        </Stack>
        <Stack spacing={2}>
          <Text
            color={useColorModeValue("gray.700", "white")}
            fontSize={"sm"}
            fontFamily={"body"}
          >
            <CalendarIcon color={"#3B628B"} /> {deadlinePengiriman}
          </Text>
        </Stack>
        <Stack mt={6} direction={"row"} spacing={0} align={"center"}>
          <Stack direction={"column"} spacing={0} fontSize={"sm"}>
            <ModalPesanan idPesanan={idPesanan} />
          </Stack>
        </Stack>
      </Box>
    </Center>
  );
};

export default PesananCard;
