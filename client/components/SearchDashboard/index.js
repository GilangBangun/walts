import React, { useEffect, useState, useContext ,forwardRef } from 'react';
import {Input, InputGroup, InputLeftElement, FormLabel, Button, IconButton, border, Heading, Center } from "@chakra-ui/react";
import DashboardTable from "../../components/DashboardTable";

import FilterDashboard from '../FilterDashboard';
import api from "../../utils/api";
import { UserContext } from "../../utils/userContext";
import {SearchIcon, CalendarIcon} from "@chakra-ui/icons";

import "react-datepicker/dist/react-datepicker.css";
import DatePicker from 'react-datepicker';
import { relative } from 'path';

export default function SearchDashboard() {
    const { user, loading } = useContext(UserContext);

    const checkRole = () => {
    if (!loading) {
        if (user?.role?.roleName !== "ADMIN") {
        // nanti sesuaikan
        // router.push("/");
        }
    }
    };

    useEffect(() => {
    checkRole();
    }, [loading]);

    
    const [pesanan, setPesanan] = useState([]);
    const [produk, setProduk] = useState([]);
    const [kategori, setKategori] = useState([]);

    const [items, setItems] = useState([]);
    const [pesanan_temp, setpesanan_temp] = useState([]);
    const [pesananTemp, setpesananTemp] = useState([]);

    const [produk_temp, setproduk_temp] = useState([]);
    const [produkTemp, setprodukTemp] = useState([]);



    const [startDate, setStartDate] = useState("");
    const [endDate, setEndDate] = useState("");
    const [totalHarga, setTotalHarga] = useState(0);
    const [search, setSearch] = useState(true);
    const [periode, setPeriode]  = useState([]);
    const [allProduk, setAllProduk] = useState([]);

    const [filter, setFilter] = useState([]);
    const [jumlah, setJumlah] = useState([]);
    const [nominal, setNominal] = useState([]);

    const [filtered, setFiltered] = useState([]);

  







      const getPesanan = async () => {
        try {
          const { data } = await api().get("pesanan/all");
          setPesanan(data?.result);
    
            const filter_proses = data?.result.filter(
            (data2) => data2.status != "Sudah Dinilai" || data2.status != "Selesai"
            );
            const filter_diterima = data?.result.filter(
            (data2) => (data2.status === "Sudah Dinilai" && data2.isRejected === false) || (data2.status === "Sudah Dinilai" && data2.isRejected === null) || (data2.status === "Selesai" && data2.isRejected === false)
            );
    
            const filter_rejected = data?.result.filter(
            (data2) => (data2.status === "Sudah Dinilai" && data2.isRejected === true) || (data2.status === "Selesai" && data2.isRejected === true)
            );
    
            setItems((items) => []);
            setItems((items) => [...items, filter_proses.length]);
            setItems((items) => [...items, filter_diterima.length]);
            setItems((items) => [...items, filter_rejected.length]);
    
            let i = 0;
            let temp = 0;
    
            for (i; i < data?.result.length; i++) {        
            temp += data?.result[i].hargaTotal;
            }
    
            setTotalHarga(temp);
            } catch (error) {
                console.log("error pesanan");
            }
        };

        const getProduk = async () => {
            try{
                const { data } = await api().get("produk/all/produk");

                setAllProduk(data?.result);


            }catch (error){

            }
        }
         

        const getPeriode = async () => {
            try {
              const { data } = await api().get("pesanan/periode");
              setPeriode(data?.result);

            } catch (error) {
            }
          };


          //ini ambil semua kategori, default nilainya juga dapet
          const getKategori = async () => {
            try {
              const { data } = await api().get("produk/all/kategori");
              setKategori(data?.result[0]);
              setFilter(data?.result[0]);
              setFiltered(data?.result[0]);
              setJumlah(data?.result[1]);
              setNominal(data?.result[2]);


            } catch (error) {
              console.log("error kategori");
            }
          };

    useEffect(() => {
        getPesanan();
        getPeriode();
        getKategori();
        getProduk();
    }, []);

    
    const handleChangeDateStart = date =>{
        setStartDate(date)
        setEndDate("");

        const data_filter1   = pesanan.filter((item) => new Date(item.deadlinePengiriman) - date > 0 );
        setpesanan_temp(data_filter1);
        setpesananTemp(data_filter1);

        //ubah data total harga sama status pesanan
    
        const filter_proses = data_filter1?.filter(
            (data2) => data2.status != "Sudah Dinilai" || data2.status != "Selesai"
            );
            const filter_diterima = data_filter1?.filter(
            (data2) => (data2.status === "Sudah Dinilai" && data2.isRejected === false) || (data2.status === "Sudah Dinilai" && data2.isRejected === null) || (data2.status === "Selesai" && data2.isRejected === false)
            );
    
            const filter_rejected = data_filter1?.filter(
            (data2) => (data2.status === "Sudah Dinilai" && data2.isRejected === true) || (data2.status === "Selesai" && data2.isRejected === true)
            );
  
        setItems((items) => []);
        setItems((items) => [...items, filter_proses.length]);
        setItems((items) => [...items, filter_diterima.length]);
        setItems((items) => [...items, filter_rejected.length]);
  
        let i = 0;
        let temp = 0;
  
        for (i; i < data_filter1.length; i++) {
          temp += data_filter1[i].hargaTotal;
        }
  
        setTotalHarga(temp);

        
        //ubah data total kuantiti kategori dan harga kategori

            let it = 0;
            const filterTanggal = allProduk.filter((item) => new Date(item.waktuDibuat) - date > 0 );
            setproduk_temp(filterTanggal);
            setprodukTemp(filterTanggal);
            empty();
            for (it ; it < kategori.length ; it++){
                const filterKategori = filterTanggal.filter(
                    (produk) => produk.kategori ===  kategori[it]);

                if ( filterKategori.length != 0) {
                    var k = kategori[it]; 
                    updateFilter(k);
                    updateJumlah(filterKategori.length);
                    let x = 0;
                    let temp = 0;
                    for (x; x < filterKategori.length; x++) {
                        temp += filterKategori[x].totalHarga;
                    }

                    updateNominal(temp);

            
                }

                

            }

    }


    const empty = () => {
        setFilter((filter) => []);
        setJumlah((jumlah) => []);
        setNominal((nominal) => []);


    }
    const updateFilter = (value) => {
        let num = value;
        setFilter((filter) => [...filter, num]);

    }

    const updateJumlah = (value) => {
        let num = value;
        setJumlah((jumlah) => [...jumlah, num]);

    }

    const updateNominal = (value) => {
        let num = value;
        setNominal((nominal) => [...nominal, num]);

    }

    const handleChangeDateEnd = date =>{
        setEndDate(date)

        const data_filter1 = pesanan_temp.filter((item) => date - new Date(item.deadlinePengiriman) > 0 );
        setpesananTemp(data_filter1);

        const filter_proses = data_filter1?.filter(
            (data2) => data2.status != "Sudah Dinilai" || data2.status != "Selesai"
            );
            const filter_diterima = data_filter1?.filter(
            (data2) => (data2.status === "Sudah Dinilai" && data2.isRejected === false) || (data2.status === "Sudah Dinilai" && data2.isRejected === null) || (data2.status === "Selesai" && data2.isRejected === false)
            );
    
            const filter_rejected = data_filter1?.filter(
            (data2) => (data2.status === "Sudah Dinilai" && data2.isRejected === true) || (data2.status === "Selesai" && data2.isRejected === true)
            );
    
          setItems((items) => []);
          setItems((items) => [...items, filter_proses.length]);
          setItems((items) => [...items, filter_diterima.length]);
          setItems((items) => [...items, filter_rejected.length]);
    
          let i = 0;
          let temp = 0;

    
          for (i; i < data_filter1.length; i++) { 
            temp += data_filter1[i].hargaTotal;
          }
    
          setTotalHarga(temp);


            //ubah data total kuantiti kategori dan harga kategori

   
            let it = 0;


            const filterTanggal = produk_temp.filter((item) => date - new Date(item.waktuDibuat) > 0 );

            setprodukTemp(filterTanggal);    
            empty();
            for (it ; it < kategori.length ; it++){
                const filterKategori = filterTanggal.filter(
                    (produk) => produk.kategori ===  kategori[it]);

                if ( filterKategori.length != 0) {
                    
                    let k = kategori[it];


                    let x = 0;
                    let temp = 0;


                    for (x; x < filterKategori.length; x++) {
                        temp += filterKategori[x].totalHarga;
                    }


                    updateFilter(k);
                    updateJumlah(filterKategori.length);
                    updateNominal(temp);
            
                }

            }

    };


    const ExampleCustomInput = forwardRef(({ value, onClick }, ref) => (
        <div className='flex-container'>
            <IconButton icon={<CalendarIcon w={5} h={5}/>} onClick={onClick} ref={ref} bgColor={'white'} color={'#3B628B'}/> 
            <p className='mt-5 mr-5'>{value}</p>
        </div>
    ));
    return(
        <div>
                 <div>
                {search ===true ?
                <div>
                    <div className='flex-container'>
                        <style>
                            {`
                                .date-picker {
                                border: 1px solid lightgrey;
                                border-radius: 5px;
                                background-color: white;
                                
                                }

                                .flex-container{
                                display: flex;
                                justify-content: center;
                                color: #3B628B;
                                }
                                
                                .mt-5{
                                    margin-top:5px;
                                }

                                .mr-5{
                                    margin-right:5px;
                                }
                            `}
                        </style>
                        <div>
                            <DatePicker  
                                wrapperClassName="date-picker"
                                dateFormat="dd/MM/yyyy"
                                selected={startDate}
                                onChange={handleChangeDateStart}
                                selectsStart
                                startDate={startDate}
                                endDate={endDate}
                                customInput={<ExampleCustomInput />}
                            />
                        </div>
                        <div>
                                <p className='mt-5 mr-5'>
                                    <strong>&nbsp;-</strong>
                                </p>
                        </div>
                        <div>
                            <DatePicker  
                                wrapperClassName="date-picker"
                                dateFormat="dd/MM/yyyy"
                                selected={endDate}
                                onChange={handleChangeDateEnd}
                                selectsEnd
                                startDate={startDate}
                                endDate={endDate}
                                minDate={startDate}
                                customInput={<ExampleCustomInput />}
                            />
                        </div>
                    </div>
                </div> :<></>
            }</div>
            <FilterDashboard  totalHarga={totalHarga} status={items}  periode={periode} kategori={filter} jumlah={jumlah} nominal = {nominal}/>
        </div>
    );

}

