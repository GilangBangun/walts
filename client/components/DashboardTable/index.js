import * as React from "react";
import {
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  Center,
  HStack,
} from "@chakra-ui/react";

import PesananCard from "../PesananCard";

import { useEffect, useState, useContext } from "react";
import api from "../../utils/api";
import { UserContext } from "../../utils/userContext";

export default function BasicTable({
  pesanan1,
  pesanan2,
  pesanan3,
  pesanan4,
  pesanan5,
  pesanan6,
}) {
  const { user, loading } = useContext(UserContext);

  const checkRole = () => {
    if (!loading) {
      if (user?.role?.roleName !== "ADMIN") {
        // nanti sesuaikan
        // router.push("/");
      }
    }
  };

  useEffect(() => {
    checkRole();
  }, [loading]);

  function formatDate(string) {
    var options = { year: "numeric", month: "long", day: "numeric" };
    return new Date(string).toLocaleDateString([], options);
  }

  return (
    <div>
      <div>
        <style>
          {`
            .scrollmenu {
              overflow-x: scroll;  /* or auto */
              height : 500px;
              overflow-y: scroll;  /* or auto */
            }
            `}
        </style>
      </div>
      <div className="scrollmenu">
        {pesanan1.length !== 0 ||
        pesanan2.length !== 0 ||
        pesanan3.length !== 0 ||
        pesanan4.length !== 0 ||
        pesanan5.length !== 0 ||
        pesanan6.length !== 0 ? (
          <HStack align="flex-start">
            {user?.role?.roleName !== "TRANSPORTER" ? (
              <Table variant="simple">
                <Thead>
                  <Tr>
                    <Center>
                      <Th textColor={"#3B628B"}>Konfirmasi</Th>
                    </Center>
                  </Tr>
                </Thead>
                <Tbody>
                  {pesanan1.map((aPesanan) => (
                    <PesananCard
                      vendor={aPesanan?.listPengguna[1]?.namePengguna}
                      kode={aPesanan?.kode}
                      deadlinePengemasan={formatDate(
                        aPesanan.deadlinePengemasan
                      )}
                      deadlinePengiriman={formatDate(
                        aPesanan.deadlinePengiriman
                      )}
                      isUpdate={aPesanan.isUpdate}
                      isLatePengiriman={aPesanan.isLatePengiriman}
                      latePengiriman={aPesanan.latePengiriman}
                      isRejected={aPesanan.isRejected}
                      /*
                      catatan = {aPesanan?.catatan}
                      pic = {aPesanan?.listPengguna[0]?.namePengguna}
                      nomorKontrak = {aPesanan?.nomorKontrak}*/
                      idPesanan={aPesanan?.idPesanan}
                    />
                  ))}
                </Tbody>
              </Table>
            ) : (
              <></>
            )}

            {user?.role?.roleName !== "TRANSPORTER" ? (
              <Table variant="simple">
                <Thead>
                  <Tr>
                    <Center>
                      <Th textColor={"#3B628B"}>Fabrikasi</Th>
                    </Center>
                  </Tr>
                </Thead>
                <Tbody>
                  {pesanan2.map((aPesanan) => (
                    <PesananCard
                      vendor={aPesanan.listPengguna[1]?.namePengguna}
                      kode={aPesanan.kode}
                      deadlinePengemasan={formatDate(
                        aPesanan.deadlinePengemasan
                      )}
                      deadlinePengiriman={formatDate(
                        aPesanan.deadlinePengiriman
                      )}
                      isUpdate={aPesanan.isUpdate}
                      isLatePengiriman={aPesanan.isLatePengiriman}
                      latePengiriman={aPesanan.latePengiriman}
                      isRejected={aPesanan.isRejected}
                      /*
                      catatan = {aPesanan.catatan}
                      pic = {aPesanan.listPengguna[0].namePengguna}
                      nomorKontrak = {aPesanan.nomorKontrak}*/
                      idPesanan={aPesanan.idPesanan}
                    />
                  ))}
                </Tbody>
              </Table>
            ) : (
              <></>
            )}

            <Table variant="simple">
              <Thead>
                <Tr>
                  <Center>
                    <Th textColor={"#3B628B"}>Dikemas</Th>
                  </Center>
                </Tr>
              </Thead>
              <Tbody>
                {pesanan3.map((aPesanan) => (
                  <PesananCard
                    vendor={aPesanan.listPengguna[1]?.namePengguna}
                    kode={aPesanan.kode}
                    deadlinePengemasan={formatDate(aPesanan.deadlinePengemasan)}
                    deadlinePengiriman={formatDate(aPesanan.deadlinePengiriman)}
                    isUpdate={aPesanan.isUpdate}
                    isLatePengiriman={aPesanan.isLatePengiriman}
                    latePengiriman={aPesanan.latePengiriman}
                    isRejected={aPesanan.isRejected}
                    /*
                      catatan = {aPesanan.catatan}
                      pic = {aPesanan.listPengguna[0].namePengguna}
                      nomorKontrak = {aPesanan.nomorKontrak}*/
                    idPesanan={aPesanan.idPesanan}
                  />
                ))}
              </Tbody>
            </Table>

            <Table variant="simple">
              <Thead>
                <Tr>
                  <Center>
                    <Th textColor={"#3B628B"}>Dikirim</Th>
                  </Center>
                </Tr>
              </Thead>
              <Tbody>
                {pesanan4.map((aPesanan) => (
                  <PesananCard
                    vendor={aPesanan.listPengguna[1].namePengguna}
                    kode={aPesanan.kode}
                    deadlinePengemasan={formatDate(aPesanan.deadlinePengemasan)}
                    deadlinePengiriman={formatDate(aPesanan.deadlinePengiriman)}
                    isUpdate={aPesanan.isUpdate}
                    isLatePengiriman={aPesanan.isLatePengiriman}
                    latePengiriman={aPesanan.latePengiriman}
                    isRejected={aPesanan.isRejected}
                    /*
                      catatan = {aPesanan.catatan}
                      pic = {aPesanan.listPengguna[0].namePengguna}
                      nomorKontrak = {aPesanan.nomorKontrak}*/
                    idPesanan={aPesanan.idPesanan}
                  />
                ))}
              </Tbody>
            </Table>

            <Table variant="simple">
              <Thead>
                <Tr>
                  <Center>
                    <Th textColor={"#3B628B"}>Sudah Sampai</Th>
                  </Center>
                </Tr>
              </Thead>
              <Tbody>
                {pesanan5.map((aPesanan) => (
                  <PesananCard
                    vendor={aPesanan.listPengguna[1].namePengguna}
                    kode={aPesanan.kode}
                    deadlinePengemasan={formatDate(aPesanan.deadlinePengemasan)}
                    deadlinePengiriman={formatDate(aPesanan.deadlinePengiriman)}
                    isUpdate={aPesanan.isUpdate}
                    isLatePengiriman={aPesanan.isLatePengiriman}
                    latePengiriman={aPesanan.latePengiriman}
                    isRejected={aPesanan.isRejected}
                    /*
                       catatan = {aPesanan.catatan}
                       pic = {aPesanan.listPengguna[0].namePengguna}
                       nomorKontrak = {aPesanan.nomorKontrak}*/
                    idPesanan={aPesanan.idPesanan}
                  />
                ))}
              </Tbody>
            </Table>

            <Table variant="simple">
              <Thead>
                <Tr>
                  <Center>
                    <Th textColor={"#3B628B"}>Selesai</Th>
                  </Center>
                </Tr>
              </Thead>
              <Tbody>
                {pesanan6.map((aPesanan) => (
                  <PesananCard
                    vendor={aPesanan.listPengguna[1].namePengguna}
                    kode={aPesanan.kode}
                    deadlinePengemasan={formatDate(aPesanan.deadlinePengemasan)}
                    deadlinePengiriman={formatDate(aPesanan.deadlinePengiriman)}
                    isUpdate={aPesanan.isUpdate}
                    isLatePengiriman={aPesanan.isLatePengiriman}
                    latePengiriman={aPesanan.latePengiriman}
                    isRejected={aPesanan.isRejected}
                    /*
                      catatan = {aPesanan.catatan}
                      pic = {aPesanan.listPengguna[0].namePengguna}
                      nomorKontrak = {aPesanan.nomorKontrak}*/
                    idPesanan={aPesanan.idPesanan}
                  />
                ))}
              </Tbody>
            </Table>
          </HStack>
        ) : (
          <div>
            <HStack align="flex-start">
              {user?.role?.roleName !== "TRANSPORTER" ? (
                <Table variant="simple">
                  <Thead>
                    <Tr>
                      <Center>
                        <Th>Konfirmasi</Th>
                      </Center>
                    </Tr>
                  </Thead>
                </Table>
              ) : (
                <></>
              )}

              {user?.role?.roleName !== "TRANSPORTER" ? (
                <Table variant="simple">
                  <Thead>
                    <Tr>
                      <Center>
                        <Th>Fabrikasi</Th>
                      </Center>
                    </Tr>
                  </Thead>
                </Table>
              ) : (
                <></>
              )}

              <Table variant="simple">
                <Thead>
                  <Tr>
                    <Center>
                      <Th>Dikemas</Th>
                    </Center>
                  </Tr>
                </Thead>
              </Table>

              <Table variant="simple">
                <Thead>
                  <Tr>
                    <Center>
                      <Th>Dikirim</Th>
                    </Center>
                  </Tr>
                </Thead>
              </Table>

              <Table variant="simple">
                <Thead>
                  <Tr>
                    <Center>
                      <Th>Sudah Sampai</Th>
                    </Center>
                  </Tr>
                </Thead>
              </Table>

              <Table variant="simple">
                <Thead>
                  <Tr>
                    <Center>
                      <Th>Selesai</Th>
                    </Center>
                  </Tr>
                </Thead>
              </Table>
            </HStack>
            <br />
            <br />
            <br />
            <h3 style={{ color: "grey" }}>
              <Center>Pesanan Tidak Dapat Ditemukan</Center>
            </h3>
          </div>
        )}
      </div>
    </div>
  );
}
