import ReactLoading from "react-loading";
import { UserContext } from "/utils/userContext";
import api from "/utils/api";
import { useState, useEffect, useContext, useRef } from "react";
import { useRouter } from "next/router";
import {
  Link,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  HStack,
  Td,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
  Heading,
  Button,
  useDisclosure,
  Center,
  Container,
} from "@chakra-ui/react";
import { AddIcon } from "@chakra-ui/icons";
import Navbar from "../Navbar";

export default function DeleteModal(props) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const cancelRef = useRef();
  console.log(props);

  const deletePost = async (id) => {
    await api().get(`https://walts-waskita.herokuapp.com/api/pengguna/delete/${id}`, {
      method: "DELETE",
    });
    // getUsers();
  };

  return (
    <AlertDialog
      isOpen={isOpen}
      leastDestructiveRef={cancelRef}
      onClose={onClose}
    >
      <AlertDialogOverlay bg="blackAlpha.300">
        <AlertDialogContent>
          <AlertDialogHeader fontSize="lg" fontWeight="bold">
            Hapus Pengguna
          </AlertDialogHeader>

          <AlertDialogBody>
            Apakah anda yakin? Anda tidak dapat membatalkan tindakan ini.
          </AlertDialogBody>

          <AlertDialogFooter>
            <Button ref={cancelRef} onClick={onClose}>
              Batalkan
            </Button>
            <Button colorScheme="red" onClick={(e) => deletePost(props)} ml={3}>
              Hapus
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialogOverlay>
    </AlertDialog>
  );
}
