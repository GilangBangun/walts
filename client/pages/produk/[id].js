import ReactLoading from "react-loading";
import { UserContext } from "/utils/userContext";
import api from "/utils/api";
import { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import {
  Link,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  HStack,
  Td,
  Heading,
  Button,
  Center,
  Container,
  Box, 
  ButtonGroup, 
  Stack, Alert, AlertIcon, AlertTitle, CloseButton, FormControl, FormLabel, Input

  
} from "@chakra-ui/react";
import { AddIcon } from "@chakra-ui/icons";
import Navbar from "../../components/Navbar";
import FormPengguna from "../../components/FormPengguna";
import FormProduk from "../../components/Form Produk";
import ButtonGroupPesanan from "../../components/ButtonGroup/ButtonGroup";
import CreatableSelect from 'react-select/creatable';
import Select from 'react-select';
import React from "react";

//const defaultOptions = ["Semen", "Batu Bata", "Batako", "Pasir", "Besi"];

export default function TambahProduk(props) {
  document.title = "WALTS - Tambah Produk"

  const router = useRouter();
  const { id } = router.query;
  const [showAll, setShowAll] = useState(false);
  const [currentIdx, setCurrentIdx] = useState(0);
  const [showCurrent, setShowCurrent] = useState(false);

  const [display, setDisplay] = React.useState("none");

  const { user, loading } = useContext(UserContext);

  const [listProduk, setListProduk] = useState([]);
  const [listKategori, setListKategori] = useState([]);
  const [masterDataProduk, setMasterDataProduk] = useState([]);

  const checkRole = () => {
    if (!loading) {
      if (
        user?.role?.roleName === "TRANSPORTER" ||
        user?.role?.roleName === "VENDOR"
      ) {
        router.push("/");
      }
    }
  };

  let num_id = parseInt(id, 10);

  const toggleCurrent = () => {
    if (!showCurrent) {
      setShowCurrent(true);
      setShowAll(false);
      return;
    }
  };

  const setCurrent = (index) => {
    setCurrentIdx(index);
    toggleCurrent();
  };

  const [message, setMessage] = useState("");

  useEffect(() => {
    checkRole();
  }, []);

  const handlerProduk = async (e) => {
      e.preventDefault();
      const payload = {};
      for (let index = 0; index < e.target.length - 1; index++) {
        const { name, value } = e.target[index];
        payload[name] = value;
      }
      console.log(payload);
      payload.counter = 0;
    
      try {
        await api().post("/produk/create", payload);
        console.log(payload);
        setMessage("berhasil di buat");
        const ask = alert("Produk berhasil ditambahkan ke Pesanan!");
        router.push("/pesanan");
      } catch (error) {
        setMessage(error.response.data.message);
      }
  };

  console.log("halo masuk");

  const printButtonLabel = (event) => {
    <></>;
    console.log(event.target.name);
    //do some stuff here
  };

  const getListProduk = async () => {
    try {
      const { data } = await api().get("produk/all");
      const unique = [
        ...new Set(data?.result.map((produk) => produk.namaProduk)),
      ];
      setListProduk(unique);
      const kat = [...new Set(data?.result.map((k) => k.kategori))];
      setListKategori(kat);
    } catch (error) {
      setMessage("error getting list");
      console.log("error getting list");
    }
  };

  const getMasterDataProduk = async () => {
    try {
      const { data } = await api().get("produk/master");
      const unique = [...new Set(data?.result.map(produk => produk.namaMDProduk))];
      setMasterDataProduk(unique);
      console.log("master data produk:");
      console.log(unique);
    } catch (error) {
      setMessage("error getting list");
      console.log("error getting list");
    }
  };

  useEffect(() => {
    getListProduk();
    getMasterDataProduk();
  }, []);

  return (
    <div>
      <Navbar>
        <div>
          <Heading as="h2" size="lg">
            <Center>Tambah Produk</Center>
            <Center> Pesanan {id} </Center>
          </Heading>

          <br />

          <Container maxW="container.md">
            {/* <Alert status="error" display={display}>
              <AlertIcon />
              <AlertTitle mr={1}>{message}</AlertTitle>
              <CloseButton position="absolute" right="8px" top="8px" />
            </Alert> */}
            <form onSubmit={handlerProduk}>
              <FormControl>
                <Stack spacing={5}>
                  <div>
                    <FormLabel>Nama Produk <span style={{ color: "red" }}>*</span></FormLabel>
                    {/* <CreatableSelect
                      placeholder=""
                      options={listProduk.map(t=>({value: t, label: t}))}
                      name = "namaProduk"
                    /> */}
                    <Select
                      placeholder=""
                      options={masterDataProduk.map(t=>({value: t, label: t}))}
                      name = "namaProduk"
                    />
                  </div>
                  {/* <div>
                    <FormLabel>Deskripsi <span style={{ color: "red" }}>*</span></FormLabel>
                    <Input
                      name="deskripsi"
                      bg="white"
                      color="black"
                      type="text"
                      isRequired
                    />
                  </div> */}
                  <div>
                    <FormLabel>
                      Kategori <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <CreatableSelect
                      placeholder=""
                      options={listKategori.map((t) => ({
                        value: t,
                        label: t,
                      }))}
                      name="kategori"
                    />
                    {/* <Input
                      name="kategori"
                      bg="white"
                      color="black"
                      type="text"
                      isRequired
                    /> */}
                  </div>
                  {/* <div>
                    <FormLabel>Mutu <span style={{ color: "red" }}>*</span></FormLabel>
                    <Input
                      name="mutu"
                      bg="white"
                      color="black"
                      type="number"
                      isRequired
                    />
                    <br />
                  </div> */}

                  <div>
                    <FormLabel>
                      Satuan <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <Input
                      name="volume"
                      bg="white"
                      color="black"
                      type="number"
                      isRequired
                    />
                    <br />
                  </div>
                  <div>
                    <FormLabel>
                      Harga <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <Input
                      name="harga"
                      bg="white"
                      color="black"
                      type="number"
                      isRequired
                    />
                    <br />
                  </div>
                  <div>
                    <FormLabel>
                      Jumlah <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <Input
                      name="jumlah"
                      bg="white"
                      color="black"
                      type="number"
                      isRequired
                    />
                    <br />
                  </div>

                  <div>
                    <Input
                      name="pesanan"
                      bg="white"
                      color="black"
                      type="number"
                      value={num_id}
                      hidden
                    />
                    <br />
                  </div>
                </Stack>
              </FormControl>
              <br />
              <Button
                borderRadius={0}
                type="submit"
                variant="solid"
                width="full"
                onClick={() => setDisplay("")}
                style={{backgroundColor:'#3B628B', color: 'white'}}
              >
                Tambah Produk
              </Button>
            </form>
          </Container>
        </div>
      </Navbar>
    </div>
  );
}
