import ReactLoading from "react-loading";
import { UserContext } from "/utils/userContext";
import api from "/utils/api";
import { useState, useEffect, useContext, useRef } from "react";
import { useRouter } from "next/router";
import {
  Link,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  HStack,
  Td,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
  Heading,
  Button,
  useDisclosure,
  Center,
  Container,
} from "@chakra-ui/react";
import { AddIcon } from "@chakra-ui/icons";
import Navbar from "../../components/Navbar";

export default function ListPesanan() {
  document.title = "WALTS - List Produk"

  const [message, setMessage] = useState("");
  const { loading } = useContext(UserContext);
  const [pesananId, setPesananId] = useState(null);
  const [listPesanan, setListPesanan] = useState([]);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const cancelRef = useRef();


  ListPesanan.defaultProps = {
    data: [],
  };

  const getListPesanan = async () => {
    try {
      const { data } = await api().get("pesanan/all");
      setListPesanan(data?.result);
      console.log("error getting list");

    } catch (error) {
      setMessage("error getting list");
      console.log("error getting list");
    }
  };

  useEffect(() => {
    getListPesanan();
  }, []);
  
  const loppPost = (listPesanan) => {
    console.log(listPesanan);
    return listPesanan.map((it, index) => (
      <Tr key={it.idPesanan}>
        <Td>{it.kode}</Td>
        <Td>{it.listPengguna[1].namePengguna}</Td>
        <Td>{it.listPengguna[0].namePengguna}</Td>
        <Td>{it.status}</Td>
        <Td>
          <HStack spacing="24px">
            <Link href={`/produk/${it.idPesanan}`}>
              <Button colorScheme="blue" size="md">
                Tambah Produk
              </Button>
            </Link>
          </HStack>
      
           
            
        
        </Td>
      </Tr>
    ));
  };
  return (
    <div>
      {loading ? (
        <ReactLoading />
      ) : (
        <Navbar>
          <Heading as="h2" size="lg">
            <Center>Produk Pesanan Logistik</Center>
          </Heading>
          <br />
          <Container maxW="container.lg">
            <h1>{message}</h1>
         
            <br />
            <Table variant="striped" colorScheme="linkedin">
              <Thead>
                <Tr>
                  <Th>Kode</Th>
                  <Th>Vendor</Th>
                  <Th>PIC</Th>
                  <Th>Status</Th>
                  <Th>
                    Action
                  </Th>
                </Tr>
              </Thead>
              <Tbody>{loppPost(listPesanan)}</Tbody>
            </Table>
          </Container>
        </Navbar>
      )}
    </div>
  );
  
  }