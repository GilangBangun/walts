import {
  Link,
  Button,
  FormControl,
  FormLabel,
  Flex,
  useColorModeValue,
  Stack,
  Container,
  Center,
  Alert,
  AlertIcon,
  AlertTitle,
  CloseButton,
  AlertDescription,
  Heading,
  StackDivider,
  Input,
  Select,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import React, { useState, useEffect, useContext } from "react";
import ReactLoading from "react-loading";
import EditPassword from "../../components/EditPassword";
import { UserContext } from "/utils/userContext";

export default function ubahPassword() {
  document.title = "WALTS - Ubah Password"
  const { user, loading } = useContext(UserContext);

  console.log("pengguna", user);
  return <EditPassword penggunaUpdateData={user} />;
}
