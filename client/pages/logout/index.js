import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Center } from "@chakra-ui/react";

export default function Logout() {
  document.title = "WALTS"

  const router = useRouter();
  const [count, setCounter] = useState(3);
  useEffect(() => {
    localStorage.removeItem("user");
  }, []);

  useEffect(() => {
    setTimeout(() => setCounter(count - 1), 1000);
    if (!count) {
      router.push("/login");
    }
  }, [count]);

  return (
    <div>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>

      <Center>
        <b>
        <h1>Kamu berhasil logout</h1>
         <p>Redirecting in {count} ...</p>
        </b>
      </Center>
 
    </div>
  );
}
