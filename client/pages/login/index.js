import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  Checkbox,
  Stack,
  Link,
  Button,
  Heading,
  Image,
  Text,
  Center,
  useColorModeValue,
} from "@chakra-ui/react";
import NextLink from "next/link";
import { useState, useEffect } from "react";
import api from "../../utils/api";
import { useRouter } from "next/router";

export default function Login() {
  document.title = "WALTS"

  const router = useRouter();
  const [message, setMessage] = useState("");
  const [login, setLogin] = useState(false);
  const [count, setCounter] = useState(3);

  const lupaPassword = async (e) => {
    e.preventDefault();

    alert("Mohon hubungi admin dari pihak Waskita.");
  };
  useEffect(() => {
    if (login) {
      setMessage(`Berhasil login. redirecting in ${count}...`);
      setTimeout(() => {
        setCounter(count - 1);
      }, 1000);
      if (!count) {
        router.push("/");
      }
    }
  }, [count, login]);

  const handlerLogin = async (e) => {
    e.preventDefault();

    const payload = {
      username: e.target[0].value,
      password: e.target[1].value,
    };

    try {
      console.log("user nih", payload);
      const { data } = await api().post("/pengguna/login", payload);
      console.log(JSON.stringify(data));
      localStorage.setItem("user", JSON.stringify(data));
      console.log("localStorage", localStorage);
      setLogin(true);
    } catch (e) {
      console.log("terdapat error");
      setMessage("Username/Password salah. Coba ulangi.");
    }
  };

  return (
    <>
      {/* <Flex
          minH="100vh"
          align="center"
          justify="center"
          bg={useColorModeValue("gray.50", "gray.800")}
        >
          <Stack spacing={8} mx="auto" maxW="lg" py={12} px={6}>
            <Stack align="center">
              <Image
                width="120px"
                objectFit="cover"
                src="https://2.bp.blogspot.com/-DpOJ5_Ys9VY/W9wVNkZ4UnI/AAAAAAAAPhk/yWHgfRj01vILBybH6ReX-zdzCPe1tGFewCLcBGAs/s1600/Waskita.png"
              />
              <Heading fontSize="4xl">Login to your account</Heading>
              <Text fontSize="lg" color="gray.600">
                Selamat datang di{" "}
                <Link href="/" color="blue.400">
                  WALTS
                </Link>
              </Text>
            </Stack>
            <Box
              rounded="lg"
              bg={useColorModeValue("white", "gray.700")}
              boxShadow="lg"
              p={8}
            >
              <Stack spacing={4}>
              <form onSubmit={handlerLogin}>
                <FormControl id="username">
                  <FormLabel>Username</FormLabel>
                  <Input type="text" />
                </FormControl>
                <FormControl id="password">
                  <FormLabel>Password</FormLabel>
                  <Input type="password" />
                </FormControl>
                <Stack spacing={10}>
                  <Stack
                    direction={{ base: "column", sm: "row" }}
                    align="start"
                    justify="space-between"
                  >
                    <Link color="blue.400" onClick={lupaPassword}>Lupa Password?</Link>
                  </Stack>
                    <Button
                      type = "submit"
                      bg="blue.400"
                      color="white"
                      _hover={{
                        bg: "blue.500",
                      }}
                    >
                      Sign in
                    </Button>
                    <p><b>{message}</b></p>

                </Stack>
              </form>
              </Stack>
            </Box>
          </Stack>
        </Flex> */}

      <Stack minH={"100vh"} direction={{ base: "column", md: "row" }}>
        <Flex p={8} flex={1} align={"center"} justify={"center"}>
          <Stack spacing={4} w={"full"} maxW={"md"}>
            <Center>
              <Heading fontSize={"3xl"}>Selamat Datang di WALTS</Heading>
            </Center>
            <Center>
              <Heading fontSize={"2xl"}>Masuk dengan akun</Heading>
            </Center>
            <form onSubmit={handlerLogin}>
              <FormControl id="username">
                <FormLabel>Username</FormLabel>
                <Input type="text" />
              </FormControl>
              <FormControl id="password">
                <FormLabel>Password</FormLabel>
                <Input type="password" />
              </FormControl>
              <Stack spacing={6}>
                <Stack
                  direction={{ base: "column", sm: "row" }}
                  align={"start"}
                  justify={"space-between"}
                >
                  <Link onClick={lupaPassword} color={"#3B628B"}>
                    Forgot password?
                  </Link>
                </Stack>
                <Button style={{backgroundColor:'#3B628B', color: 'white'}} variant={"solid"} type="submit">
                  Masuk
                </Button>
                <p>
                  <b>{message}</b>
                </p>
              </Stack>
            </form>
          </Stack>
        </Flex>
        <Flex flex={1}>
          <Image
            alt={"Login Image"}
            objectFit={"cover"}
            src={
              "../../static/login.png"
            }
          />
        </Flex>
      </Stack>
    </>
  );
}
