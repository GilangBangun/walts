import Navbar from "../components/Navbar";
import DashboardTable from "../components/DashboardTable";
import Search from "../components/Search";
import { useRouter } from "next/router";
import { useEffect, useState, useContext } from "react";
import { UserContext } from "../utils/userContext";

export default function Index() {
  const router = useRouter();
  const user = useContext(UserContext);
  const pengguna = localStorage.getItem("user");
  const pengguna2 = JSON.parse(pengguna);
  // router.reload();

  useEffect(() => {

    if (user?.user?.role?.roleName !== pengguna2.role[0].authority) {
        router.reload();
      }

  }, []);  

  return (
    <Navbar>
      <Search />
    </Navbar>
  );
}
