import {
  Link,
  Button,
  FormControl,
  FormLabel,
  Flex,
  useColorModeValue,
  Stack,
  Container,
  Center,
  Alert,
  AlertIcon,
  AlertTitle,
  CloseButton,
  AlertDescription,
  Heading,
  StackDivider,
  Input,
  Select,
} from "@chakra-ui/react";
import Navbar from "../../components/Navbar";
import { useRouter } from "next/router";
import { useState, useEffect, useContext } from "react";
import ReactLoading from "react-loading";
import api from "/utils/api";
import { UserContext } from "/utils/userContext";
import React from "react";
import Head from "next/head";

export default function Register() {
  document.title = "WALTS - Tambah Pengguna";

  const router = useRouter();
  const [message, setMessage] = useState("");
  const { user, loading } = useContext(UserContext);
  const [role, setRole] = useState([]);

  const getRole = async () => {
    try {
      const { data } = await api().get("role/list");
      setRole(data?.result);
    } catch (error) {
      setMessage("error getting list");
    }
  };

  const checkRole = () => {
    if (!loading) {
      if (user?.role?.roleName === "STAFF LOGISTIK") {
        router.push("/");
      }

      if (user?.role?.roleName === "VENDOR") {
        router.push("/");
      }

      if (user?.role?.roleName === "TRANSPORTER") {
        router.push("/");
      }
    }
  };

  useEffect(() => {
    checkRole();
  }, [loading]);

  useEffect(() => {
    getRole();
  }, []);

  const handlerRegister = async (e) => {
    e.preventDefault();
    const payload = {};
    for (let index = 0; index < e.target.length - 1; index++) {
      const { name, value } = e.target[index];
      payload[name] = value;
    }
    payload.counter = 0;

    try {
      await api().post("/pengguna/register", payload);
      // console.log(payload);
      const ask = alert("Pengguna berhasil dibuat!");

      router.push("/users");
    } catch (error) {
      setMessage(error.response.data.message);
      const ask = alert("Pengguna gagal dibuat, silahkan ulangi.");
    }
  };

  const [display, setDisplay] = React.useState("none");

  return (
    <div>
      {loading ? (
        <ReactLoading />
      ) : (
        <Navbar>
          <Heading as="h2" size="lg">
            <Center>Register</Center>
          </Heading>
          <br />
          <Container maxW="container.md">
            {/* <Alert status="error" display={display}>
              <AlertIcon />
              <AlertTitle mr={1}>{message}</AlertTitle>
              <CloseButton position="absolute" right="8px" top="8px" />
            </Alert> */}

            <form onSubmit={handlerRegister}>
              <FormControl>
                <Stack spacing={5}>
                  <div>
                    <FormLabel>
                      Nama <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <Input
                      name="namePengguna"
                      bg="white"
                      color="black"
                      type="text"
                      isRequired
                    />
                  </div>
                  <div>
                    <FormLabel>
                      Nomor Telepon <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <Input
                      name="phoneNumber"
                      bg="white"
                      color="black"
                      type="number"
                      isRequired
                    />
                  </div>

                  <div>
                    <FormLabel>
                      Role <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <Select bg="white" name="role">
                      {role.map((it) => (
                        <option long value={it.idRole}>
                          {it?.roleName}
                        </option>
                      ))}
                    </Select>
                  </div>
                  <div>
                    <FormLabel>
                      NPWP{" "}
                      <span style={{ color: "red" }}>(Wajib untuk Vendor)</span>
                    </FormLabel>
                    <Input name="npwp" bg="white" color="black" type="text" />
                    <br />
                  </div>

                  <div>
                    <FormLabel>
                      Username <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <Input
                      name="username"
                      bg="white"
                      color="black"
                      type="text"
                      isRequired
                    />
                    <br />
                  </div>

                  <div>
                    <FormLabel>
                      Password <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <Input
                      name="password"
                      bg="white"
                      color="black"
                      type="password"
                      isRequired
                    />
                  </div>
                </Stack>
              </FormControl>
              <br />
              <Button
                borderRadius={0}
                type="submit"
                variant="solid"
                width="full"
                style={{ backgroundColor: "#3B628B", color: "white" }}
                onClick={() => setDisplay("")}
              >
                Register
              </Button>
            </form>
          </Container>
        </Navbar>
      )}
    </div>
  );
}
