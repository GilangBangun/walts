import {
  Link,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  HStack,
  Td,
  Heading,
  Button,
  Center,
  Container,
} from "@chakra-ui/react";
import FormPengguna from "../../components/FormPengguna";
import Navbar from "../../components/Navbar";

export default function createPengguna(props) {
  document.title = "WALTS - Tambah Pengguna"
  
  return (
    <div>
      <Navbar>
        <Heading as="h2" size="lg">
          <Center>Tambah Pengguna</Center>
        </Heading>
        <br />
        <FormPengguna action="add" />
      </Navbar>
    </div>
  );
}
