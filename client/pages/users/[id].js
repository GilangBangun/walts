import {
  Link,
  Button,
  FormControl,
  FormLabel,
  Flex,
  useColorModeValue,
  Stack,
  Container,
  Center,
  Alert,
  AlertIcon,
  AlertTitle,
  CloseButton,
  AlertDescription,
  Heading,
  StackDivider,
  Input,
  Select,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import React, { useState, useEffect, useContext } from "react";
import ReactLoading from "react-loading";
import EditPengguna from "../../components/EditPengguna";
import api from "/utils/api";
import { UserContext } from "/utils/userContext";

export default function updatePengguna({ user }) {
  document.title = "WALTS - Perbarui Pengguna"
  console.log("pengguna", user);
  return <EditPengguna penggunaUpdateData={user} />;
}

export async function getServerSideProps({ params }) {
  const res = await fetch(
    `https://walts-waskita.herokuapp.com/api/pengguna/detail/${params.id}`
  );
  console.log("masuk getserver");
  console.log(res);
  const user = await res.json();
  console.log("masuk 2", user);

  return {
    props: { user },
  };
}

// export default function EditPengguna(props) {
//   const { data } = props;
//   console.log(`${data}halo`);

//   const [fields, setFields] = useState({
//     username: data.username,
//     namaPengguna: data.namaPengguna,
//     npwp: data.npwp,
//   });

//   async function updateHandler(e) {
//     e.preventDefault();

//     const update = await fetch(`/api/posts/update/${data.idPengguna}`, {
//       method: "PUT",
//       headers: {
//         "Content-Type": "application/json",
//         Authorization: `Bearer ${token}`,
//       },
//       body: JSON.stringify(fields),
//     });

//     const res = await update.json();

//     Router.push("/posts");
//   }

//   function fieldHandler(e) {
//     const namePengguna = e.target.getAttribute("namePengguna");

//     setFields({
//       ...fields,
//       [namePengguna]: e.target.value,
//     });
//   }

//   console.log("halo masuk");
//   return (
//     <div>
//       <h3> Pengguna ID: {data.idPengguna}</h3>
//       <Navbar>
//         <Heading as="h2" size="lg">
//           <Center>Update Pengguna</Center>
//         </Heading>
//         <br />
//         {/* <FormPengguna action="update" dataPost={props.data} /> */}
//         <form onSubmit={updateHandler.bind(this)}>
//           <input
//             onChange={fieldHandler.bind(this)}
//             type="text"
//             placeholder="Title"
//             name="title"
//             defaultValue={pengguna.namePengguna}
//           />
//           <br />
//           <textarea
//             onChange={fieldHandler.bind(this)}
//             placeholder="Content"
//             name="content"
//             defaultValue={pengguna.username}
//           />
//           <br />

//           <button type="submit">Save Changes</button>
//         </form>
//       </Navbar>
//     </div>
//   );
// }

// // export async function getServerSideProps(context) {
// //   // const router = useRouter();
// //   const { idPengguna } = context.query;
// //   try {
// //     const { data } = await api().get(`pengguna/update/${idPengguna}`);
// //     console.log(`${data} halo`);
// //     return {
// //       props: { data }, // will be passed to the page component as props
// //     };
// //   } catch (error) {
// //     console.log("error nih");
// //     return {
// //       notFound: true,
// //     };
// //   }
// // }
