import ReactLoading from "react-loading";
import { UserContext } from "/utils/userContext";
import api from "/utils/api";
import { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import {
  Link,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  HStack,
  Td,
  Heading,
  Button,
  Center,
  Container,
  Box,
  ButtonGroup,
  Text,
  Grid,
  GridItem,
  Stack,
  SimpleGrid,
} from "@chakra-ui/react";
import { StarIcon } from "@chakra-ui/icons";
import Navbar from "../../../components/Navbar";
import FormPengguna from "../../../components/FormPengguna";
import { ExternalLinkIcon } from "@chakra-ui/icons";
import ButtonGroupPesanan from "../../../components/ButtonGroup/ButtonGroup";
import NumberFormat from "react-number-format";

export default function DetailVendor() {
  document.title = "WALTS - Detail Pengguna";

  const router = useRouter();
  const { id } = router.query;
  if (!id) {
    return null;
  }

  const [showAll, setShowAll] = useState(false);
  const [currentIdx, setCurrentIdx] = useState(0);
  const [showCurrent, setShowCurrent] = useState(false);
  const [detail, setDetail] = useState([]);

  const [penilaian, setPenilaian] = useState([]);

  const [utama, setUtama] = useState({
    username: "",
    password: "",
    namePengguna: "",
    phoneNumber: "",
    role: {
      idRole: null,
      roleName: "",
    },
    npwp: "",
  });

  const getUtama = async () => {
    try {
      const { data } = await api().get(`pengguna/detail/${id}`);

      setUtama(data?.result);
    } catch (error) {
      console.log("error getting list");
    }
  };

  const getPenilaian = async () => {
    try {
      const dataNilai = await api().get(`pengguna/detail/${id}/penilaian`);
      setPenilaian(dataNilai.data.result);
    } catch (error) {
      console.log("error getting list");
    }
  };

  useEffect(() => {
    // checkRole();
    getUtama();
    getPenilaian();
    detailUtamaDisplay();
  }, []);

  const DetailUtama = () => {
    return (
      <>
        <Box borderRadius="lg" w="100%" h="100%" p={4} padding={8}>
          <Table variant="simple" colorScheme={"facebook"}>
            <Tbody>
              <Tr>
                <Td color="black">Nama</Td>
                <Td color="black">{utama.namePengguna}</Td>
              </Tr>
              <Tr>
                <Td color="black">NPWP</Td>
                <Td color="black">{utama.npwp}</Td>
              </Tr>
              <Tr>
                <Td color="black">Nomor Telepon</Td>
                <Td color="black">{utama.phoneNumber}</Td>
              </Tr>
            </Tbody>
          </Table>
        </Box>
      </>
    );
  };

  const looping = (penilaian) => {
    return penilaian.map((it, index) => (
      <Tr key={it.idPenilaian}>
        <Td color="black">
          <Link href={`/pesanan/${it?.pesanan}`} isExternal>
            {it?.kodePesanan}
          </Link>
        </Td>
        <Td color="black">{it?.listPenerimaPenilai[1]?.namePengguna}</Td>
        <Td color="black">{it?.waktu}</Td>
        <Td color="black">
          <StarIcon w={5} h={5} color="#FDE65A" />
          &nbsp;{it?.nilai}
        </Td>
        <Td color="black">{it?.feedback}</Td>
      </Tr>
    ));
  };

  const DetailPenilaian = () => {
    return (
      <>
        <br></br>
        <Center>
          {utama.rataPenilaian === null ? (
            <></>
          ) : (
            <Box as="button" p={3} borderRadius="md" bg="#FDE65A" color="black">
              Nilai Keseluruhan
              <Text fontSize="3xl">
                <b>{utama.rataPenilaian}</b>
              </Text>
            </Box>
          )}
        </Center>
        {utama.rataPenilaian === null ? (
          <Center>
            <Text color="#696969" fontSize="1xl">
              <b>-Belum ada penilaian-</b>
            </Text>
          </Center>
        ) : (
          <Box
            borderRadius="lg"
            w="100%"
            h="100%"
            p={4}
            color="white"
            padding={8}
          >
            <Table size="md" variant="simple" colorScheme="facebook">
              <Thead style={{ backgroundColor: "#3B628B" }}>
                <Tr>
                  <Th style={{ color: "white" }}>Pesanan</Th>
                  <Th style={{ color: "white" }}>Penilai</Th>
                  <Th style={{ color: "white" }}>Waktu Nilai</Th>
                  <Th style={{ color: "white" }}>Nilai</Th>
                  <Th style={{ color: "white" }}>Komentar</Th>
                </Tr>
              </Thead>
              <Tbody>{looping(penilaian)}</Tbody>
            </Table>
          </Box>
        )}
      </>
    );
  };

  const toggleCurrent = () => {
    if (!showCurrent) {
      setShowCurrent(true);
      setShowAll(false);
      return;
    }
  };

  const detailUtamaDisplay = () => {
    setDetail("utama");
  };
  const detailPenilaianDisplay = () => {
    setDetail("penilaian");
  };

  const handleClick = (e, path) => {
    e.preventDefault();

    if (path === "/pengguna") {
      setDetail(detailUtama);
    }
    if (path === "/penilaian") {
      setDetail(DetailPenilaian);
      // then you can:
      // router.push(path)
    }
  };

  return (
    <div>
      <Navbar>
        <Center>
          <Box
            marginTop={10}
            borderRadius="lg"
            bg="#CBD5E0"
            w="100%"
            h="100%"
            p={4}
            padding={8}
            backgroundColor={"gray.200"}
          >
            <Box marginBottom={10}>
              <Center>
                <Heading as="h5" size="lg" color="black">
                  Vendor {utama.namePengguna}
                </Heading>
              </Center>
            </Box>
            <Center>
              <Stack direction="row" spacing={4} align="center">
                <Button
                  variant="link"
                  onClick={detailUtamaDisplay}
                  color="black"
                >
                  Detail Vendor
                </Button>
                <Button
                  variant="link"
                  onClick={detailPenilaianDisplay}
                  color="black"
                >
                  Penilaian
                </Button>
              </Stack>
            </Center>

            <div>
              <div>
                {showAll &&
                  info.map((el, i) => <p key={`content-${i}`}>{el}</p>)}
              </div>

              <div className="w-11/12 mx-auto mt-4 flex flex-wrap">
                {detail === "utama" && <DetailUtama />}
                {detail === "penilaian" && <DetailPenilaian />}
              </div>

              {showCurrent ? <div>{info[currentIdx]}</div> : null}
            </div>
          </Box>
        </Center>
      </Navbar>
    </div>
  );
}
