import ReactLoading from "react-loading";
import { UserContext } from "/utils/userContext";
import api from "/utils/api";
import { useState, useEffect, useContext, useRef, React } from "react";
import Router from "next/router";
import {
  Link,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  HStack,
  Td,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
  Heading,
  Button,
  useDisclosure,
  Center,
  Container,
} from "@chakra-ui/react";
import { AddIcon, StarIcon, ChevronRightIcon, EditIcon } from "@chakra-ui/icons";
import Navbar from "../../components/Navbar";
import DeleteModal from "../../components/DeleteModal";
import Loading from "react-loading";

import { useRouter } from "next/router";
export default function Users(props) {
  document.title = "WALTS - List Pengguna"

  const [message, setMessage] = useState("");
  const [userId, setUserId] = useState(null);
  const [users, setUsers] = useState([]);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const cancelRef = useRef();
  const { user, loading } = useContext(UserContext);

  const router = useRouter();

  Users.defaultProps = {
    data: [],
  };

  const getUsers = async () => {
    try {
      const { data } = await api().get("pengguna/all");
      setUsers(data?.result);
      console.log(data);
    } catch (error) {
      setMessage("error getting list");
      console.log("error getting list");
    }
  };

  const checkRole = () => {
    if (!loading) {
      if (user?.role?.roleName === "VENDOR") {
        router.push("/");
      }
    }
  };

  const deletePost = async (e, id) => {
    e.preventDefault();
    const ask = confirm("Apakah anda yakin ingin menghapus pengguna ini?");
    if (ask) {
      console.log("tes hapus");
      await api().get(`https://walts-waskita.herokuapp.com/api/pengguna/delete/${id}`, {
        method: "DELETE",
      });
    }
    getUsers();
  };

  useEffect(() => {
    getUsers();
    checkRole();
  }, []);

  function editHandler(e, id) {
    e.preventDefault();
    Router.push(`/users/${id}`);
  }

  const filter_vendor = users.filter(
    (data_vendor) => data_vendor.role?.roleName === "VENDOR"
  );

  const loppPost = (users) => {
    console.log(users);
    return users.map((it, index) => (
      <Tr key={it.idPengguna}>
        <Td>{it.namePengguna}</Td>
        <Td>{it.role.roleName}</Td>
        <Td>{it.username}</Td>
        <Td>{it.phoneNumber}</Td>
        <Td>
          <HStack spacing="24px">
            <Link href={`/users/${it.idPengguna}`}>
              <Button size="md" leftIcon={<EditIcon boxSize={3}/>} style={{backgroundColor:'#FDE65A'}}>
                Edit
              </Button>
            </Link>
          </HStack>
          <DeleteModal props={it.idPengguna} />
        </Td>
      </Tr>
    ));
  };

  const loopVendor = (users) => {
    console.log(users);
    return users.map((it, index) => (
      <Tr key={it.idPengguna}>
        <Td>{it.namePengguna}</Td>
        <Td>{it.username}</Td>
        <Td>{it.phoneNumber}</Td>
        {it.rataPenilaian === null ? (
          <Td>Tidak ada</Td>
        ) : (
          <Td>
            <StarIcon w={5} h={5} color="#FFD801" />
            {it.rataPenilaian}
          </Td>
        )}
        <Td>
          <Center>
            <Link href={`/users/detail/${it.idPengguna}`}>
              <Button style={{backgroundColor:'#74C365'}} size="md" rightIcon={<ChevronRightIcon boxSize={3}/>}>
                Detail
              </Button>
            </Link>
          </Center>
        </Td>
      </Tr>
    ));
  };

  return (
    <div>
      <style>
        {
          `
          .scrollmenu {
            overflow-x: scroll;  /* or auto */
            height : 400px;
            overflow-y: scroll;  /* or auto */
          }
          `
        }
      </style>
      {user?.role?.roleName !== "ADMIN" && user?.role?.roleName !== "VENDOR" ? (
        <Navbar>
          <Heading as="h2" size="lg">
            <Center>List Vendor</Center>
          </Heading>
          <br />
          <Container maxW="container.lg">
            <h1>{message}</h1>
            <br />
            <div className="scrollmenu">
              <Table variant="simple" colorScheme="facebook">
                <Thead style={{backgroundColor:'#3B628B'}}>
                  <Tr>
                    <Th style={{color:'white'}}>Nama</Th>
                    <Th style={{color:'white'}}>Username</Th>
                    <Th style={{color:'white'}}>No. Telepon</Th>
                    <Th style={{color:'white'}}>Rerata Nilai</Th>
                    <Th style={{color:'white'}}><center>Aksi</center></Th>
                  </Tr>
                </Thead>
                <Tbody>{loopVendor(filter_vendor)}</Tbody>
              </Table>
            </div>
          </Container>
        </Navbar>
      ) : (
        <Navbar>
          <Heading as="h2" size="lg">
            <Center>Pengelolaan Pengguna</Center>
          </Heading>
          <br />
          <Container maxW="container.lg">
            <h1>{message}</h1>
            <Center>
              <Link href="/register">
                <Button leftIcon={<AddIcon boxSize={3}/>} style={{backgroundColor:'#3B628B', color: 'white'}}>
                  Tambah Pengguna
                </Button>
              </Link>
            </Center>
            <br />
            <div className="scrollmenu">
              <Table variant="simple" colorScheme="facebook">
                <Thead style={{backgroundColor:'#3B628B'}}>
                  <Tr>
                    <Th style={{color:'white'}}>Nama</Th>
                    <Th style={{color:'white'}}>Role</Th>
                    <Th style={{color:'white'}}>Username</Th>
                    <Th style={{color:'white'}}>No. Telepon</Th>
                    <Th style={{color:'white'}}>Aksi</Th>
                  </Tr>
                </Thead>
                <Tbody>{loppPost(users)}</Tbody>
              </Table>  
            </div>
          </Container>
        </Navbar>
      )}
    </div>
  );
}
