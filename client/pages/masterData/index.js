import {
  Link,
  Button,
  FormControl,
  FormLabel,
  Flex,
  useColorModeValue,
  Stack,
  Container,
  Center,
  Alert,
  AlertIcon,
  AlertTitle,
  CloseButton,
  AlertDescription,
  Heading,
  StackDivider,
  Input,
  Select,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  HStack,
  Td,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import api from "/utils/api";
import React, { useState, useEffect, useContext } from "react";
import ReactLoading from "react-loading";
import { UserContext } from "/utils/userContext";
import Navbar from "../../components/Navbar";
import { AddIcon, StarIcon, ChevronRightIcon, EditIcon } from "@chakra-ui/icons";

export default function masterData() {
  document.title = "WALTS - Master Data Produk"
  const { user, loading } = useContext(UserContext);
  const [masterProduk, setMasterProduk] = useState([]);

  const checkRole = () => {
    if (!loading) {
      if (user?.role?.roleName !== "ADMIN") {
        router.push("/");
      }
    }
  };


  const getMaster = async () => {
    try {
      const { data } = await api().get(`produk/master`);
      setMasterProduk(data?.result);
      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    checkRole();
    getMaster();
  }, []);

  const loopMaster = (masterProduk) => {
    console.log(masterProduk);
    return masterProduk.map((it, index) => (
      <Tr key={it.idMDProduk}>
        <Td>{it.idMDProduk}</Td>
        <Td>{it.namaMDProduk}</Td>
      </Tr>
    ));
  };

//  return <FormMasterData />;
  return (
    <div>
      <style>
        {
          `
          .scrollmenu {
            overflow-x: scroll;  /* or auto */
            height : 400px;
            overflow-y: scroll;  /* or auto */
          }
          `
        }
      </style>
        <Navbar>
          <Heading as="h2" size="lg">
            <Center>Master Data Produk</Center>
          </Heading>
          <br />
          <Container maxW="container.lg">
            <Center>
              <Link href="/masterData/tambah">
                <Button leftIcon={<AddIcon boxSize={3}/>} style={{backgroundColor:'#3B628B', color: 'white'}}>
                  Tambah Produk
                </Button>
              </Link>
            </Center>
            <br />
            <div className="scrollmenu">
              <Table variant="simple" colorScheme="facebook">
                <Thead style={{backgroundColor:'#3B628B'}}>
                  <Tr>
                    <Th style={{color:'white'}}>No</Th>
                    <Th style={{color:'white'}}>Nama Produk</Th>
                  </Tr>
                </Thead>
                <Tbody>{loopMaster(masterProduk)}</Tbody>
              </Table>
            </div>
          </Container>
        </Navbar>
    </div>
  );
}
