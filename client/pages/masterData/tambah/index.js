import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Button,
  FormControl,
  FormLabel,
  Input,
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
  VStack,
  Stack,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  Heading,
  Center,
  Container
} from "@chakra-ui/react";
import { useEffect, useState, useContext, React } from "react";
import api from "/utils/api";
import { useRouter } from "next/router";
import { UserContext } from "/utils/userContext";
import { StarIcon } from "@chakra-ui/icons";
import Navbar from "../../../components/Navbar";

export default function tambah() {
  const router = useRouter();
  const { user, loading } = useContext(UserContext);
  const [message, setMessage] = useState("");
  const [display, setDisplay] = useState("none");

  const checkRole = () => {
    if (!loading) {
      if (user?.role?.roleName !== "ADMIN") {
        router.push("/");
      }
    }
  };

  useEffect(() => {
    checkRole();
  }, []);

  const handlerData = async (e) => {
    e.preventDefault();
    const payload = {};
    for (let index = 0; index < e.target.length - 1; index++) {
      const { name, value } = e.target[index];
      payload[name] = value;
    }
    console.log(payload);
    payload.counter = 0;

    try {
      await api().post(`/produk/master/add`, payload);
      console.log(payload);
      alert("Produk berhasil ditambahkan ke Master Data!");
      router.push("/masterData");

    } catch (error) {
      setMessage(error.response.data.message);
      alert("Produk gagal dikirimkan, silahkan ulangi!");
    }
  };

  return (
    <div>
      <Navbar>
        <Heading as="h2" size="lg">
          <Center>Tambah Master Data Produk</Center>
        </Heading>
        <br />
        <Container maxW="container.md">
          <form onSubmit={handlerData}>
            <FormControl>
              <Stack spacing={5}>
                <div>
                  <FormLabel>
                    Nama Produk <span style={{ color: "red" }}>*</span>
                  </FormLabel>
                  <Input
                    name="namaMDProduk"
                    placeholder="Nama disertai mutu produk (jika ada)"
                    bg="white"
                    color="black"
                    isRequired
                  />
                </div>
              </Stack>
            </FormControl>
            <br />
            <Button
              borderRadius={0}
              type="submit"
              variant="solid"
              width="full"
              style={{backgroundColor:'#3B628B', color: 'white'}}
            >
              Tambah Produk
            </Button>
          </form>
        </Container>
      </Navbar>
      )
    </div>
  );
}
