import Navbar from "../../components/Navbar";
import ProductTable from "../../components/ProductTable";
import { UserContext } from "/utils/userContext";
import api from "/utils/api";
import NumberFormat from "react-number-format";
import SearchDashboard from "../../components/SearchDashboard";
import Router from "next/router";
import { useRouter } from "next/router";
import {
  Link,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Stack,
  HStack,
  VStack,
  Td,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
  Heading,
  Box,
  StackDivider,
  Button,
  useDisclosure,
  Center,
  Container,
  Input,
  InputGroup,
  InputLeftElement,
  FormLabel,
  Stat,
  StatLabel,
  StatNumber,
  StatHelpText,
  StatArrow,
} from "@chakra-ui/react";
import { AddIcon } from "@chakra-ui/icons";
import { useState, useMemo, useEffect, useContext, useRef, React } from "react";
import ReactLoading from "react-loading";
import { Pie, Bar, defaults } from "react-chartjs-2";
import ChartDataLabels from "chartjs-plugin-datalabels";
import Chart from "chart.js/auto";
import { ArcElement } from "chart.js";
Chart.register(ArcElement);
Chart.register(ChartDataLabels);
// import { DataGrid, GridToolbar } from "@mui/x-data-grid";
// import { DataGrid, GridToolbar } from "@material-ui/data-grid";
// import MUIDataTable from "mui-datatables";
import { forwardRef } from 'react';
import MaterialTable from "material-table";
//icon
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

export default function Dashboard() {
  const [users, setUsers] = useState([]);
  const {user, loading} = useContext(UserContext);
  const [vendor, setVendor] = useState([]);
  const [vendorJenis, setVendorJenis] = useState([]);
  const [message, setMessage] = useState("");
  const [listPesanan, setListPesanan] = useState([]);
  const [Pesanan, setPesanan] = useState([]);
  const [periode, setPeriode] = useState([]);
  const [kategori, setKategori] = useState([]);
  const statusTitle = ["Progress", "Diterima", "Ditolak"];
  const [items, setItems] = useState([]);
  const [totalHarga, setTotalHarga] = useState(0);
  const [detail, setDetail] = useState([]);
  const router = useRouter();


  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };

  const kolom_2 = [
    {
      field: "namePengguna",
      title: "Nama",
      align: "left",
    },
    {
      field: "phoneNumber",
      title: "Kontak",
      align: "left",
    },
    {
      field: "jumlahPesanan",
      title: "Jumlah Pesanan",
      type: 'numeric',
      align: "left",
    },
    {
      field: "biayaTotal",
      title: "Biaya Total (Rp)",
      type: 'numeric',
      align: "left",      
    },
    {
      field: "rataPenilaian",
      type: 'numeric',
      title: "Nilai",
      align: "left",      
    },
  ];

  const getVendor = async () => {
    try {
      const { data } = await api().get("pengguna/all");

      const data_filter = data.result.filter(
        (data2) => data2.role.idRole === 3
      );

      setVendor(data_filter);
    } catch (error) {
      console.log("error getting list aaa");
    }
  };

  const getVendorJenis = async () => {
    try {
      const { data } = await api().get("pengguna/vendorJenis");
      const data_real = data.data;
      setVendorJenis(data_real);
    } catch (error) {
      console.log("error getting list aaa");
    }
  };

  useEffect(() => {
    checkRole();
    getVendor();
    getVendorJenis();
    detailVendorDisplay();
  }, []);

  const getUsers = async () => {
    try {
      const { data } = await api().get("pengguna/all");
      setUsers(data?.result);
    } catch (error) {
      setMessage("error getting list");
      console.log("error getting list");
    }
  };

  const checkRole = () => {
    if (!loading) {
      if (user?.role?.roleName === "VENDOR" || user?.role?.roleName === "TRANSPORTER") {
        router.push("/");
      }
    }
  };

  //TABEL VENDOR
  const columns = useMemo(
    () => [
      {
        Header: "Nama",
        accessor: "namePengguna",
      },
      {
        Header: "Nilai",
        accessor: "rataPenilaian",
      },
      {
        Header: "Jumlah Pesanan",
        accessor: "jumlahPesanan",
      },
      {
        Header: "Biaya Total",
        accessor: "biayaTotal",
        Cell: (props) => (
          <div>
            <NumberFormat
              value={props.value}
              displayType={"text"}
              thousandSeparator={true}
              prefix={"Rp "}
            />
          </div>
        ),
      },
    ],
    []
  );

  const [showAll, setShowAll] = useState(false);
  const [currentIdx, setCurrentIdx] = useState(0);
  const [showCurrent, setShowCurrent] = useState(false);

  const DetailVendor = () => {
    return (
      <>
        <div>
          <br></br>
          <Container maxW="container.lg">
            <VStack width="100%">
              <HStack divider={<StackDivider borderColor="gray.200" />}>
                <Box
                  p="6"
                  backgroundColor={"white"}
                  shadow="md"
                  borderRadius={"lg"}
                  width="100%"
                >
                  <div>
                    <Center>Jumlah Pesanan per Vendor</Center>
                    <br></br>
                  </div>
                  <div>
                    <Pie
                      data={{
                        labels: vendor?.map((x) => x.namePengguna),
                        datasets: [
                          {
                            label: "Jumlah Pesanan",
                            data: vendor?.map((x) => x.jumlahPesanan),
                            backgroundColor: [
                              "rgba(255, 99, 132, 0.2)",
                              "rgba(54, 162, 235, 0.2)",
                              "rgba(255, 206, 86, 0.2)",
                              "rgba(75, 192, 192, 0.2)",
                              "rgba(153, 102, 255, 0.2)",
                              "rgba(255, 159, 64, 0.2)",
                            ],
                            borderColor: [
                              "rgba(255, 99, 132, 1)",
                              "rgba(54, 162, 235, 1)",
                              "rgba(255, 206, 86, 1)",
                              "rgba(75, 192, 192, 1)",
                              "rgba(153, 102, 255, 1)",
                              "rgba(255, 159, 64, 1)",
                            ],
                            borderWidth: 3,
                          },
                        ],
                        hoverOffset: 4,
                      }}
                      height={200}
                      width={400}
                      options={{
                        plugins: {
                          legend: {
                            position: "right",
                          },
                          datalabels: {
                            formatter: (value, context) => {
                              const datapoints =
                                context.chart.data.datasets[0].data;
                              function totalSum(total, datapoint) {
                                return total + datapoint;
                              }
                              const totalvalue = datapoints.reduce(totalSum, 0);
                              const percentageValue = (
                                (value / totalvalue) *
                                100
                              ).toFixed(1);
                              if (percentageValue > 10) {
                                return `${percentageValue}%`;
                              } else {
                                return null;
                              }
                            },
                            color: "black",
                            labels: {
                              title: {
                                font: {
                                  weight: "bold",
                                },
                              },
                            },
                          },
                        },
                        maintainAspectRatio: false,
                      }}
                    />
                  </div>
                </Box>

                <Box
                  p="6"
                  backgroundColor={"white"}
                  shadow="md"
                  borderRadius={"lg"}
                  width="100%"
                >
                  <div>
                    <Center>Performa Vendor</Center>
                    <br></br>
                  </div>
                  <div>
                    <Pie
                      data={{
                        labels: vendorJenis?.map((x) => x.nama),
                        datasets: [
                          {
                            data: vendorJenis?.map((x) => x.jumlah),
                            backgroundColor: [
                              "rgba(255, 99, 132, 0.2)",
                              "rgba(54, 162, 235, 0.2)",
                              "rgba(255, 206, 86, 0.2)",
                              "rgba(75, 192, 192, 0.2)",
                              "rgba(153, 102, 255, 0.2)",
                              "rgba(255, 159, 64, 0.2)",
                            ],
                            borderColor: [
                              "rgba(255, 99, 132, 1)",
                              "rgba(54, 162, 235, 1)",
                              "rgba(255, 206, 86, 1)",
                              "rgba(75, 192, 192, 1)",
                              "rgba(153, 102, 255, 1)",
                              "rgba(255, 159, 64, 1)",
                            ],
                            borderWidth: 3,
                          },
                          // {
                          //   label: 'Quantity',
                          //   data: [47, 52, 67, 58, 9, 50],
                          //   backgroundColor: 'orange',
                          //   borderColor: 'red',
                          // },
                        ],
                        hoverOffset: 4,
                      }}
                      height={200}
                      width={400}
                      options={{
                        plugins: {
                          legend: {
                            position: "right",
                          },
                          datalabels: {
                            formatter: (value, context) => {
                              const datapoints =
                                context.chart.data.datasets[0].data;
                              function totalSum(total, datapoint) {
                                return total + datapoint;
                              }
                              const totalvalue = datapoints.reduce(totalSum, 0);
                              const percentageValue = (
                                (value / totalvalue) *
                                100
                              ).toFixed(1);
                              if (percentageValue > 10) {
                                return `${percentageValue}%`;
                              } else {
                                return null;
                              }
                            },
                            color: "black",
                            labels: {
                              title: {
                                font: {
                                  weight: "bold",
                                },
                              },
                            },
                          },
                        },
                        maintainAspectRatio: false,
                      }}
                    />
                  </div>
                </Box>
              </HStack>
            </VStack>
            <br></br>
            <br></br>
            <MaterialTable 
            data={vendor} columns={kolom_2} 
            title= "List Vendor"
            icons={tableIcons}
            options= {{
              filtering:true, sorting: true, exportButton: true, exportAllData: true,
              columnsButton: true
            }}
            />
            <br></br>
            <br></br>
          </Container>
        </div>
      </>
    );
  };

  const DetailPesanan = () => {
    return (
      <>
        <div>
          <br></br>
          <SearchDashboard></SearchDashboard>
        </div>
      </>
    );
  };

  const toggleCurrent = () => {
    if (!showCurrent) {
      setShowCurrent(true);
      setShowAll(false);
      return;
    }
  };

  const detailVendorDisplay = () => {
    setDetail("vendor");
  };
  const detailPesananDisplay = () => {
    setDetail("pesanan");
  };

  const handleClick = (e, path) => {
    e.preventDefault();

    if (path === "/vendor") {
      setDetail(detailVendor);
    }
    if (path === "/pesanan") {
      setDetail(DetailPesanan);
    }
  };

  return (
    <Navbar>
      <Center>
        <Box
          borderRadius="lg"
          bg="##edf2f6"
          w="100%"
          h="100%"
          p={4}
          padding={4}
        >
          <Box marginBottom={5}>
            <Center>
              <Heading as="h5" size="lg" color="black">
                Dashboard
              </Heading>
            </Center>
          </Box>
          <Center>
            {detail === "vendor" ? (
              <Stack direction="row" spacing={8} align="center">
                <Button
                  variant="outline"
                  outlineColor="black"
                  size="sm"
                  color="#3c648c"
                  onClick={detailVendorDisplay}
                >
                  Vendor
                </Button>

                <Button
                  backgroundColor="#3c648c"
                  onClick={detailPesananDisplay}
                  size="sm"
                  color="#edf2f7"
                >
                  Pesanan
                </Button>
              </Stack>
            ) : (
              <Stack direction="row" spacing={8} align="center">
                <Button
                  backgroundColor="#3c648c"
                  size="sm"
                  color="#e4ebf2"
                  onClick={detailVendorDisplay}
                >
                  Vendor
                </Button>

                <Button
                  variant="outline"
                  outlineColor="black"
                  onClick={detailPesananDisplay}
                  size="sm"
                  color="#3c648c"
                >
                  Pesanan
                </Button>
              </Stack>
            )}
          </Center>

          <div>
            <div>
              {showAll && info.map((el, i) => <p key={`content-${i}`}>{el}</p>)}
            </div>

            <div className="w-11/12 mx-auto mt-4 flex flex-wrap">
              {detail === "vendor" && <DetailVendor />}
              {detail === "pesanan" && <DetailPesanan />}
            </div>

            {showCurrent ? <div>{info[currentIdx]}</div> : null}
          </div>
        </Box>
      </Center>
    </Navbar>
  );
}
