import ReactLoading from "react-loading";
import { UserContext } from "/utils/userContext";
import api from "/utils/api";
import { useState, useEffect, useContext, useRef, forwardRef } from "react";
import { useRouter } from "next/router";
import NumberFormat from "react-number-format";
import { SearchIcon, CalendarIcon } from "@chakra-ui/icons";
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";

import {
  Link,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  HStack,
  Td,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
  Heading,
  Button,
  useDisclosure,
  Center,
  Container,
  Input,
  InputGroup,
  InputLeftElement,
  FormLabel,
  IconButton,
} from "@chakra-ui/react";
import { AddIcon, ChevronRightIcon } from "@chakra-ui/icons";
import Navbar from "../../components/Navbar";

export default function ListPesanan() {
  document.title = "WALTS - List Pesanan"

  const [message, setMessage] = useState("");
  const [pesananId, setPesananId] = useState(null);
  const [listPesanan, setListPesanan] = useState([]);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const cancelRef = useRef();

  const router = useRouter();
  const { user, loading } = useContext(UserContext);
  const [searchInput, setSearchInput] = useState("");

  const checkRole = () => {
    if (!loading) {
      if (
        user?.role?.roleName === "TRANSPORTER" ||
        user?.role?.roleName === "VENDOR"
      ) {
        router.push("/");
      }
    }
  };

  ListPesanan.defaultProps = {
    data: [],
  };

  const getListPesanan = async () => {
    try {
      const { data } = await api().get(
        `pengguna/detail/${user?.idPengguna}/pesanan`
      );
      setListPesanan(data?.result);
      setFilteredResults(data?.result);
      setFilteredDateResults(data?.result);
      setFilteredDate2Results(data?.result);      
    } catch (error) {
      setMessage("error getting list");
      console.log(error);
    }
  };

  useEffect(() => {
    checkRole();
    getListPesanan();
  }, []);

  const [filteredResults, setFilteredResults] = useState([]);
  const [filteredDateResults, setFilteredDateResults] = useState([]);
  const [filteredDate2Results, setFilteredDate2Results] = useState([]);

  const handleChange = (e) => {
    const target = e.target.value;
    setSearchInput(target);
    setSearch(true);
    setStartDate("");
    setEndDate("");

    if (target !== "") {
      const filteredData = listPesanan.filter(
        (item) =>
          item.listPengguna[1].namePengguna
            .toLowerCase()
            .includes(target.toLowerCase()) ||
          item.kode.toLowerCase().includes(target.toLowerCase())
      );
      setFilteredResults(filteredData);
      setFilteredDateResults(filteredData);
      setFilteredDate2Results(filteredData);
    } else {
      setFilteredResults(listPesanan);
      setFilteredDateResults(listPesanan);
      setFilteredDate2Results(listPesanan);
    }
  };

  const [search, setSearch] = useState(false);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  const handleChangeDateStart = (date) => {
    setStartDate(date);
    setEndDate("");

    const filteredData = filteredResults.filter(
      (item) => new Date(item.deadlinePengiriman) - date > 0
    );
    setFilteredDateResults(filteredData);
    setFilteredDate2Results(filteredData);
  };

  const handleChangeDateEnd = (date) => {
    setEndDate(date);

    const filteredData = filteredDateResults.filter(
      (item) => date - new Date(item.deadlinePengiriman) > 0
    );
    setFilteredDate2Results(filteredData);
  };

  const ExampleCustomInput = forwardRef(({ value, onClick }, ref) => (
      <div className='flex-container'>
          <IconButton icon={<CalendarIcon w={5} h={5}/>} onClick={onClick} ref={ref} bgColor={'white'} color={'#3B628B'}/> 
          <p className='mt-5 mr-5'>{value}</p>
      </div>
  ));

  const loppPost = (filteredDate2Results) => {
    return filteredDate2Results.map((it, index) => (
      <Tr key={it.idPesanan}>
        <Td>{it.kode}</Td>
        <Td>{it.listPengguna[1]?.namePengguna}</Td>
        <Td>{it.listPengguna[2]?.namePengguna}</Td>
        <Td>
          <NumberFormat
            value={it.hargaTotal}
            displayType={"text"}
            thousandSeparator={true}
            prefix={"Rp "}
          />
        </Td>
        <Td>{it.status}</Td>
        <Td>
          <HStack spacing="24px">
            <Link href={`/pesanan/${it.idPesanan}`}>
              <Button style={{backgroundColor:'#74C365'}} size="md" rightIcon={<ChevronRightIcon boxSize={3}/>}>
                Detail
              </Button>
            </Link>
          </HStack>
        </Td>
        <Td>
          <HStack spacing="24px">
            {(it.status !== "Selesai") && (it.status !== "Sudah Dinilai") ? (
              <Link href={`/produk/${it.idPesanan}`}>
            <Button  size="md" leftIcon={<AddIcon boxSize={3}/>} style={{backgroundColor:'#FDE65A'}}>
            Tambah Produk
          </Button>
          </Link>
        ) : (
          <Link href={`/produk/${it.idPesanan}`}>
          <Button  size="md" leftIcon={<AddIcon boxSize={3}/>} style={{backgroundColor:'#FDE65A'}} isDisabled>
          Tambah Produk
        </Button>
        </Link>
        )}
          </HStack>
        </Td>
      </Tr>
    ));
  };
  return (
    <div>
      {loading ? (
        <ReactLoading />
      ) : (
        <Navbar>
          <Heading as="h2" size="lg" >
            <Center>Pesanan Logistik</Center>
          </Heading>
          <br />
          <Container maxW="container.lg">
            {/* <h1>{message}</h1> */}
            <Center>
              <Link href="/pesanan/create">
                <Button leftIcon={<AddIcon boxSize={3}/>} style={{backgroundColor:'#3B628B', color: 'white'}}>
                  Tambah Pesanan
                </Button>
              </Link>
            </Center>
            <br />
            <div>
              <div>
                <InputGroup>
                  <InputLeftElement
                    className="InputLeft"
                    pointerEvents="none"
                    children={
                      <SearchIcon
                        className="SearchIcon"
                        color="gray.300"
                        size="xs"
                      />
                    }
                  />
                  <Input
                    variant='flushed'
                    backgroundColor={'white'}
                    type="search"
                    placeholder="Cari Pesanan Berdasarkan Nama Vendor atau Kode Pesanan"
                    onChange={handleChange}
                    mb={3}
                  />
                </InputGroup>
              </div>
              <style>
                {
                  `
                  .scrollmenu {
                    overflow-x: scroll;  /* or auto */
                    height : 400px;
                    overflow-y: scroll;  /* or auto */
                  }
                  `
                }
              </style>
              <div>
                {search === true ? (
                  <div>
                    <FormLabel mb={2} fontSize={15} color="#3B628B"><center>Filter Pesanan Berdasarkan Waktu Deadline Pengiriman</center></FormLabel>
                    <div className="flex-container">
                      <style>
                        {`
                          .date-picker {
                            border: 1px solid lightgrey;
                            border-radius: 5px;
                            background-color: white;
                            }

                            .flex-container{
                            display: flex;
                            justify-content: center;
                            color:#3B628B;
                            }
                            
                            .mt-5{
                                margin-top:5px;
                            }

                            .mr-5{
                                margin-right:5px;
                            }
                            `}
                      </style>
                      <div>
                        <DatePicker
                          wrapperClassName="date-picker"
                          dateFormat="dd/MM/yyyy"
                          selected={startDate}
                          onChange={handleChangeDateStart}
                          selectsStart
                          startDate={startDate}
                          endDate={endDate}
                          customInput={<ExampleCustomInput />}
                        />
                      </div>
                      <div>
                              <p className='mt-5 mr-5'>
                                  <strong>&nbsp;-</strong>
                              </p>
                      </div>
                      <div>
                        <DatePicker
                          wrapperClassName="date-picker"
                          dateFormat="dd/MM/yyyy"
                          selected={endDate}
                          onChange={handleChangeDateEnd}
                          selectsEnd
                          startDate={startDate}
                          endDate={endDate}
                          minDate={startDate}
                          customInput={<ExampleCustomInput />}
                        />
                      </div>
                    </div>
                  </div>
                ) : (
                  <></>
                )}
              </div>
            </div>
            <br />
            <div  className="scrollmenu">
              <Table variant="simple" colorScheme="facebook">
                <Thead style={{backgroundColor:'#3B628B'}}>
                  <Tr>
                    <Th style={{color:'white'}}><center>Kode</center></Th>
                    <Th style={{color:'white'}}><center>Vendor</center></Th>
                    <Th style={{color:'white'}}><center>Transporter</center></Th>
                    <Th style={{color:'white'}}><center>Harga Pesanan</center></Th>
                    <Th style={{color:'white'}}><center>Status</center></Th>
                    <Th style={{color:'white'}}><center>Detail</center></Th>
                    <Th style={{color:'white'}}><center>Tambah Produk</center></Th>
                  </Tr>
                </Thead>
                <Tbody>{loppPost(filteredDate2Results)}</Tbody>
              </Table>
            </div>
          </Container>
        </Navbar>
      )}
    </div>
  );
}
