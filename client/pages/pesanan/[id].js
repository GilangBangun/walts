import ReactLoading from "react-loading";
import { UserContext } from "/utils/userContext";
import api from "/utils/api";
import { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import {
  Link,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  HStack,
  Td,
  Heading,
  Button,
  Center,
  Container,
  Box,
  ButtonGroup,
  Grid,
  GridItem,
  Stack,
  SimpleGrid,
} from "@chakra-ui/react";
import { AddIcon } from "@chakra-ui/icons";
import Navbar from "../../components/Navbar";
import FormPengguna from "../../components/FormPengguna";
import { ExternalLinkIcon } from "@chakra-ui/icons";
import ButtonGroupPesanan from "../../components/ButtonGroup/ButtonGroup";
import NumberFormat from "react-number-format";
// import DetailProduk from "../../components/DetailPesanan/produk";
// import DetailStatus from "../../components/DetailPesanan/status";
// import DetailUtama from "../../components/DetailPesanan";

export default function DetailPesanan() {
  document.title = "WALTS - Detail Pesanan";

  const router = useRouter();
  const { id } = router.query;
  if (!id) {
    return null;
  }
  function formatDate(string) {
    var options = { year: "numeric", month: "long", day: "numeric" };
    return new Date(string).toLocaleDateString([], options);
  }
  const { user, loading } = useContext(UserContext);
  const checkRole = () => {
    if (!loading) {
      if (
        user?.role?.roleName === "TRANSPORTER" ||
        user?.role?.roleName === "VENDOR"
      ) {
        router.push("/");
      }
    }
  };

  const [showAll, setShowAll] = useState(false);
  const [currentIdx, setCurrentIdx] = useState(0);
  const [showCurrent, setShowCurrent] = useState(false);

  const [detail, setDetail] = useState([]);
  const [validasi, setValidasi] = useState({
    catatan: "",
    fileValidasi: "",
    idValidasi: null,
    statusPesanan: "",
    waktuValidasi: "",
  });

  const [produk, setProduk] = useState({
    deskripsi: "",
    harga: null,
    idProduk: null,
    jumlah: null,
    kategori: "",
    mutu: null,
    namaProduk: "",
    pesanan: {
      catatan: "",
      deadlinePengemasan: null,
      deadlinePengiriman: null,
      hargaTotal: 0,
      idPesanan: 0,
      kode: "",
      listPengguna: {
        idPengguna: "",
        namePengguna: "",
        npwp: "",
        password: "",
        phoneNumber: "",
        role: {
          idRole: null,
          username: "",
          roleName: {
            idRole: null,
            roleName: "",
          },
        },
      },
      nomorKontrak: null,
      status: "",
      waktuDibuat: null,
      volume: null,
    },
  });

  const [utama, setUtama] = useState({
    catatan: "",
    deadlinePengemasan: null,
    deadlinePengiriman: null,
    hargaTotal: null,
    idPesanan: null,
    kode: "",
    listPengguna: {
      idPengguna: "",
      namePengguna: "",
      npwp: "",
      password: "",
      phoneNumber: "",
      role: {
        idRole: null,
        roleName: "",
      },
      username: "",
    },
    nomorKontrak: null,
    status: "",
    waktuDibuat: null,
  });

  const DetailUtama = () => {
    return (
      <>
        <Box borderRadius="lg" w="100%" h="100%" p={4} padding={8}>
          <Table variant="simple" colorScheme={"facebook"}>
            <Tbody>
              <Tr>
                <Td color="black">Kode</Td>
                <Td color="black">{utama.kode}</Td>
              </Tr>
              <Tr>
                <Td color="black">Nomor Kontrak</Td>
                <Td color="black">{utama.nomorKontrak}</Td>
              </Tr>
              <Tr>
                <Td color="black">Vendor</Td>
                <Td color="black">{utama.listPengguna[1]?.namePengguna}</Td>
              </Tr>
              <Tr>
                <Td color="black">Harga Total</Td>
                <Td color="black">
                  <NumberFormat
                    value={utama.hargaTotal}
                    displayType={"text"}
                    thousandSeparator={true}
                    prefix={"Rp "}
                  />
                </Td>
              </Tr>

              <Tr>
                <Td color="black">Deadline Pengemasan</Td>

                {utama.deadlinePengemasan === null ? (
                  <Td color="black"> - </Td>
                ) : (
                  <Td color="black">{formatDate(utama.deadlinePengemasan)}</Td>
                )}
              </Tr>
              <Tr>
                <Td color="black">Deadline Pengiriman</Td>
                {utama.deadlinePengiriman === null ? (
                  <Td color="black"> - </Td>
                ) : (
                  <Td color="black"> {formatDate(utama.deadlinePengiriman)}</Td>
                )}
              </Tr>
              <Tr>
                <Td color="black">Transporter</Td>
                <Td color="black">{utama.listPengguna[2]?.namePengguna}</Td>
              </Tr>

              <Tr>
                <Td color="black">PIC</Td>
                <Td color="black">{utama.listPengguna[0]?.namePengguna}</Td>
              </Tr>

              <Tr>
                <Td color="black">Catatan</Td>
                <Td color="black">{utama.catatan}</Td>
              </Tr>
            </Tbody>
          </Table>
        </Box>
      </>
    );
  };

  const DetailStatus = () => {
    return (
      <>
        <Box borderRadius="lg" w="100%" h="100%" p={4} padding={8}>
          <Table variant="simple" colorScheme="facebook">
            <Thead style={{ backgroundColor: "#3B628B" }}>
              <Tr>
                <Th style={{ color: "white" }}>Status</Th>
                <Th style={{ color: "white" }}>Bukti Validasi</Th>
              </Tr>
            </Thead>
            <Tbody>{loopValidasi(validasi)}</Tbody>
          </Table>
        </Box>
      </>
    );
  };

  const DetailProduk = () => {
    return (
      <>
        <Box borderRadius="lg" w="100%" h="100%" p={4} padding={8}>
          <Table size="md" variant="simple" colorScheme="facebook">
            <Thead style={{ backgroundColor: "#3B628B" }}>
              <Tr>
                <Th style={{ color: "white" }}>Nama</Th>
                {/* <<Th style={{color:'white'}}>Deskripsi</Th> */}
                <Th style={{ color: "white" }}>Kategori</Th>
                {/* <<Th style={{color:'white'}}>Mutu</Th> */}
                <Th style={{ color: "white" }}>Satuan</Th>
                <Th style={{ color: "white" }}>Jumlah</Th>
                <Th style={{ color: "white" }}>Harga</Th>
              </Tr>
            </Thead>
            <Tbody>{loppPost(produk)}</Tbody>
          </Table>
        </Box>
      </>
    );
  };

  const toggleCurrent = () => {
    if (!showCurrent) {
      setShowCurrent(true);
      setShowAll(false);
      return;
    }
  };

  const loppPost = (produk) => {
    return produk.map((it, index) => (
      <Tr key={it.idProduk}>
        <Td color="black">{it.namaProduk}</Td>
        {/*<Td color="black">{it.deskripsi}</Td>*/}
        <Td color="black">{it.kategori}</Td>
        {/*<Td color="black">{it.mutu}</Td>*/}
        <Td color="black">{it.volume}</Td>
        <Td color="black">{it.jumlah}</Td>
        <Td color="black">
          <NumberFormat
            value={it.harga}
            displayType={"text"}
            thousandSeparator={true}
            prefix={"Rp "}
          />
        </Td>
      </Tr>
    ));
  };

  const detailUtamaDisplay = () => {
    setDetail("utama");
  };
  const detailProdukDisplay = () => {
    setDetail("produk");
  };

  const detailStatusDisplay = () => {
    setDetail("status");
  };

  const [message, setMessage] = useState("");

  const getUtama = async () => {
    try {
      const { data } = await api().get(`pesanan/detail/${id}`);
      setUtama(data?.result);
    } catch (error) {
      setMessage("error getting list");
      console.log("error getting list");
    }
  };

  const getValidasi = async () => {
    try {
      const { data } = await api().get(`pesanan/detail/${id}/bukti`);

      setValidasi(data?.result);
    } catch (error) {
      setMessage("error getting list");
    }
  };

  const getProduk = async () => {
    try {
      const { data } = await api().get(`pesanan/detail/${id}/produk`);
      setProduk(data?.result);
    } catch (error) {
      console.log("error getting list");
    }
  };

  const loopValidasi = (validasi) => {
    return validasi.map((it, index) => (
      <Tr key={it.idProduk}>
        {it.statusPesanan === "Proses Fabrikasi" ? (
          <Td color="black">Proses Fabrikasi</Td>
        ) : (
          <></>
        )}

        {it.statusPesanan === "Sudah Dikemas" ? (
          <Td color="black">Sudah Dikemas</Td>
        ) : (
          <></>
        )}

        {it.statusPesanan === "Proses Pengiriman" ? (
          <Td color="black">Proses Pengiriman</Td>
        ) : (
          <></>
        )}

        {it.statusPesanan === "Sudah Sampai" ? (
          <Td color="black">Sudah Sampai</Td>
        ) : (
          <></>
        )}

        {it.statusPesanan === "Selesai" ? (
          <Td color="black">Selesai</Td>
        ) : (
          <></>
        )}

        <Td color="black">
          <Link href={it.fileValidasi} isExternal>
            Lihat Bukti <ExternalLinkIcon mx="2px" />
          </Link>
        </Td>
      </Tr>
    ));
  };

  const handleClick = (e, path) => {
    e.preventDefault();

    if (path === "/pesanan") {
      setDetail(detailUtama);
    }
    if (path === "/produk") {
      setDetail(DetailProduk);
    }
    if (path === "/status") {
      setDetail(DetailStatus);
    }
  };

  useEffect(() => {
    checkRole();
    getUtama();
    getProduk();
    getValidasi();
    detailUtamaDisplay();
  }, []);

  return (
    <div>
      <Navbar>
        <Center>
          <Box
            marginTop={10}
            borderRadius="lg"
            bg="#CBD5E0"
            w="100%"
            h="100%"
            p={4}
            backgroundColor={"gray.200"}
            padding={8}
          >
            <Box marginBottom={10}>
              <Center>
                <Heading as="h5" size="lg" color="black">
                  Pesanan {utama.kode}
                </Heading>
              </Center>
            </Box>
            <Center>
              <Stack direction="row" spacing={4} align="center">
                <Button
                  variant="link"
                  onClick={detailUtamaDisplay}
                  color="black"
                >
                  Detail Pesanan
                </Button>
                <Button
                  variant="link"
                  onClick={detailProdukDisplay}
                  color="black"
                >
                  Detail Produk
                </Button>
                <Button
                  variant="link"
                  onClick={detailStatusDisplay}
                  color="black"
                >
                  Bukti Pengubahan Status
                </Button>
              </Stack>
            </Center>

            <div>
              <div>
                {showAll &&
                  info.map((el, i) => <p key={`content-${i}`}>{el}</p>)}
              </div>

              <div className="w-11/12 mx-auto mt-4 flex flex-wrap">
                {detail === "utama" && <DetailUtama />}
                {detail === "produk" && <DetailProduk />}
                {detail === "status" && <DetailStatus />}
              </div>

              {showCurrent ? <div>{info[currentIdx]}</div> : null}
            </div>
          </Box>
        </Center>
      </Navbar>
    </div>
  );
}
