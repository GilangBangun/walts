import ReactLoading from "react-loading";
import { UserContext } from "/utils/userContext";
import api from "/utils/api";
import { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import {
  Link,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  HStack,
  Td,
  Heading,
  Button,
  Center,
  Container,
  Box,
  ButtonGroup,
  Select,
  Stack,
  FormControl,
  FormLabel,
  Input,
  FormHelperText,
} from "@chakra-ui/react";
import { AddIcon } from "@chakra-ui/icons";
import Navbar from "../../components/Navbar";
import FormPengguna from "../../components/FormPengguna";
import ButtonGroupPesanan from "../../components/ButtonGroup/ButtonGroup";
import FormPesanan from "../../components/FormPesanan";

import DatepickerComponent from "../../components/DatePicker";

export default function DetailPesanan(props) {
  document.title = "WALTS - Tambah Pesanan"

  const router = useRouter();
  const { user, loading } = useContext(UserContext);

  const checkRole = () => {
    if (!loading) {
      if (
        user?.role?.roleName === "TRANSPORTER" ||
        user?.role?.roleName === "VENDOR"
      ) {
        router.push("/");
      }
    }
  };

  const getTransporter = async () => {
    try {
      const { data } = await api().get("pengguna/all");
      setUsers(data?.result);
      console.log(data);
    } catch (error) {
      // setMessage("error getting list");
      console.log("error getting list");
    }
  };

  useEffect(() => {
    checkRole();
  }, []);

  return (
    <div>
      <Navbar>
        <FormPesanan></FormPesanan>
      </Navbar>
    </div>
  );
}

// "kode": "string",
// "deadlinePengemasan": "2022-03-19",
// "deadlinePengiriman": "2022-03-19",
// "hargaTotal": 0,
// "catatan": "string",
// "nomorKontrak": 0
// Vendor --dropdown
// Transporter - dropdown
