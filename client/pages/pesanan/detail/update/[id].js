import {
  Link,
  Button,
  FormControl,
  FormLabel,
  Flex,
  useColorModeValue,
  Stack,
  Container,
  Center,
  Alert,
  AlertIcon,
  AlertTitle,
  CloseButton,
  AlertDescription,
  Heading,
  StackDivider,
  Input,
  Select,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import React, { useState, useEffect, useContext } from "react";
import ReactLoading from "react-loading";
import EditPesanan from "../../../../components/EditPesanan";
import api from "/utils/api";
import { UserContext } from "/utils/userContext";

export default function updatePesanan({ pesanan }) {
  document.title = "WALTS - Perbarui Pesanan"

  const router = useRouter();
  const { user, loading } = useContext(UserContext);
  const checkRole = () => {
    if (!loading) {
      if (
        user?.role?.roleName === "TRANSPORTER" ||
        user?.role?.roleName === "VENDOR"
      ) {
        router.push("/");
      }
    }
  };

  useEffect(() => {
    checkRole();
  }, []);

  return <EditPesanan pesananUpdateData={pesanan} />;
}

export async function getServerSideProps({ params }) {
  console.log("masuk getserver" + params.id);
  const res = await fetch(
    `https://walts-waskita.herokuapp.com/api/pesanan/detail/${params.id}`
  );
  console.log("masuk getserver");
  console.log(res);
  const pesanan = await res.json();
  console.log("masuk 2", pesanan);

  return {
    props: { pesanan },
  };
}
