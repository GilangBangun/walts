# **Waskita Logistic Tracking System (WALTS)**

## Propensi B-03

Anggota Kelompok:

| NPM | Nama | Role  |
| ----------| --- | ---------- | 
| 1906299105 | Rezaldy Ahmad Maulana | **Project Manager** |
| 1906399474 | Alina Dhifan Ajriya | **System Analyst** |
| 1906298840 | Andira Anggun Maharani S. Darmadji | **Design Lead** |
| 1906399700 | Gilang Matahati Bangun | **Tech Lead** |
| 1906299055 | Naifathiya Langitadiva | **Scrum Master** |