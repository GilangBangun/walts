package b03.propensi.walts.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import b03.propensi.walts.model.Produk;

@Repository
public interface ProdukDb extends JpaRepository<Produk, Long>{
    Optional<Produk> findByIdProduk(Long idProduk);
}
