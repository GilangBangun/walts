package b03.propensi.walts.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import b03.propensi.walts.model.Role;

@Repository
public interface RoleDb extends JpaRepository<Role, Long>{
    Optional<Role> findByIdRole(Long idRole);
}
