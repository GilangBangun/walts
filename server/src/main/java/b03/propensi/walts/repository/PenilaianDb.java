package b03.propensi.walts.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import b03.propensi.walts.model.Penilaian;

@Repository
public interface PenilaianDb extends JpaRepository<Penilaian, Long>{
    Optional<Penilaian> findByIdPenilaian(Long idPenilaian);
}