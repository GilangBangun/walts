package b03.propensi.walts.repository;

import java.util.Optional;

import b03.propensi.walts.model.ProdukPesanan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import b03.propensi.walts.model.Produk;

@Repository
public interface ProdukPesananDb extends JpaRepository<ProdukPesanan, Long>{
    Optional<ProdukPesanan> findByIdProduk(Long idProduk);
}
