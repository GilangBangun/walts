package b03.propensi.walts.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import b03.propensi.walts.model.ValidasiStatus;

@Repository
public interface ValidasiStatusDb extends JpaRepository<ValidasiStatus, Long>{
    Optional<ValidasiStatusDb> findByIdValidasi(Long idValidasi);
}
