package b03.propensi.walts.repository;

import b03.propensi.walts.model.MasterDataProduk;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MasterDataProdukDb extends JpaRepository<MasterDataProduk, Long> {
    Optional<MasterDataProduk> findByIdMDProduk(Long idMDProduk);
}
