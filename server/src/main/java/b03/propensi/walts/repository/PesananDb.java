package b03.propensi.walts.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import b03.propensi.walts.model.Pesanan;

@Repository
public interface PesananDb extends JpaRepository<Pesanan, Long>{
    Optional<Pesanan> findByIdPesanan(Long idPesanan);
}


