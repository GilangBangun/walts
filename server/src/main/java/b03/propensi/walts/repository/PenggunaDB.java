package b03.propensi.walts.repository;

import b03.propensi.walts.model.Pengguna;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PenggunaDB extends JpaRepository<Pengguna, Long> {
    Optional<Pengguna> findByIdPengguna(String idPengguna);
    Optional<Pengguna> findByUsername(String username);
}
