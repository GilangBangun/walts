package b03.propensi.walts.repository;

import b03.propensi.walts.model.Notifikasi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NotifikasiDb extends JpaRepository<Notifikasi, Long> {
    Optional<Notifikasi> findByIdNotifikasi(Long idNotifikasi);

}
