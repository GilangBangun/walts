package b03.propensi.walts.rest;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;

import b03.propensi.walts.model.Pengguna;
import b03.propensi.walts.model.Pesanan;
import b03.propensi.walts.model.Produk;
import java.time.format.DateTimeFormatter;
import b03.propensi.walts.service.PenggunaRestService;
import b03.propensi.walts.service.PesananRestService;
import b03.propensi.walts.service.ProdukRestService;
import lombok.*;

@AllArgsConstructor
@Setter
@Getter
public class PesananRest {
    @Autowired
    PesananRestService pesananRestService;

    @Autowired
    ProdukRestService produkRestService;

    @Autowired
    PenggunaRestService penggunaRestService;

    String status;
    LocalDate waktuDibuat;
    String deadlinePengemasan;
    String deadlinePengiriman;
    String catatan;
    String nomorKontrak;
    String pembuat;
    String vendor;
    String transporter;

    public Pesanan convertToPesanan(Long counter, Pengguna pembuat, Pengguna vendor, Pengguna transporter){
        Pesanan pesanan = new Pesanan();
        pesanan.setKode(assignCode(vendor, counter));
        pesanan.setStatus(status);
        pesanan.setWaktuDibuat(waktuDibuat);
        System.out.println(deadlinePengemasan);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate tglPengemasan = LocalDate.parse(deadlinePengemasan, formatter);
        LocalDate tglPengiriman = LocalDate.parse(deadlinePengiriman, formatter);

        pesanan.setDeadlinePengemasan(tglPengemasan);
        pesanan.setDeadlinePengiriman(tglPengiriman);

        System.out.println("KEMAS: " + tglPengemasan);
        System.out.println("KIRIM: " + tglPengiriman);

        pesanan.setHargaTotal(Long.valueOf(0)); //assign nilai total harga
        pesanan.setCatatan(catatan);
        pesanan.setNomorKontrak(nomorKontrak);
        pesanan.setListPengguna(assignPengguna(pembuat, vendor, transporter));
        System.out.println("Jumlah pengguna" + pesanan.getListPengguna().size());
        //pesanan.setListProduk(assignProduk(idPesanan));
        return pesanan;
    }

    public List<Pengguna> assignPengguna(Pengguna pembuat,Pengguna vendor, Pengguna transporter){
        List<Pengguna> daftarPengguna = new ArrayList<Pengguna>();
        daftarPengguna.add(pembuat);
        daftarPengguna.add(vendor);
        daftarPengguna.add(transporter);
        return daftarPengguna;
    }

    /*public List<Produk> assignProduk(Long idPesanan){
        List<Produk> daftarProduk = new ArrayList<Produk>();
        List<Produk> allProduk = produkRestService.getAllProduk();
        for (int i = 0; i< allProduk.size(); i++){
            if(allProduk.get(i).getPesanan().getIdPesanan().equals(idPesanan)){
                daftarProduk.add(allProduk.get(i));
            }
        }
        return daftarProduk;
    }*/

    public String assignCode(Pengguna vendor, Long counter){
        String temp = vendor.getNamePengguna().toUpperCase();
        temp = temp.replaceAll("\\s", "");

        Random random = new Random();
        int x = random.nextInt(100);
        char[] ch = temp.toCharArray();  
        String kode = "";

        kode += ch[0];
        kode += ch[ch.length/2];
        kode += ch[ch.length-1];
        kode += x;
        System.out.println("kode pesanan: "+kode);

//        for (int i = 0; i < 3; i++) {
//            kode += ch[i];
//        }
//        kode += counter;
        return kode;  
    }
}
