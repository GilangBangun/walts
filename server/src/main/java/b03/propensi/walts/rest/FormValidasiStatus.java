package b03.propensi.walts.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FormValidasiStatus {
    @JsonProperty("password")
    String fileValidasi;

    public String getFileValidasi() {
        return fileValidasi;
    }

    public void setFileValidasi(String password) {
        this.fileValidasi = fileValidasi;
    }
}
