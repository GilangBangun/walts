package b03.propensi.walts.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import lombok.*;

import java.util.Hashtable;
import java.util.List;

@NoArgsConstructor
public class JsonResponse<T> {
    public ResponseEntity<Result<T>> response(String message) {
        var data = new Result<T>(message, HttpStatus.OK.value(), null);
        ResponseEntity<Result<T>> res = new ResponseEntity<Result<T>>(data, HttpStatus.OK);
        return res;
    }

    public ResponseEntity<Result<T>> response(String message, HttpStatus status) {
        var data = new Result<T>(message, status.value(), null);
        ResponseEntity<Result<T>> res = new ResponseEntity<Result<T>>(data, status);
        return res;
    }

    public ResponseEntity<Result<T>> response(String message, T result) {
        var data = new Result<T>(message, HttpStatus.OK.value(), result);
        ResponseEntity<Result<T>> res = new ResponseEntity<Result<T>>(data, HttpStatus.OK);
        return res;
    }

    public ResponseEntity<Result<T>> response(String message, T result, HttpStatus status) {
        var data = new Result<T>(message, status.value(), result);
        ResponseEntity<Result<T>> res = new ResponseEntity<Result<T>>(data, status);
        return res;
    }

    public ResponseEntity<Result<Hashtable<String, Integer>>> response(String message, Hashtable<String, Integer> result, HttpStatus status) {
        var data = new Result< Hashtable<String, Integer> >(message, status.value(), result);
        ResponseEntity<Result< Hashtable<String, Integer> >> res = new ResponseEntity<Result< Hashtable<String, Integer> >>(data, status);
        return res;
    }


}
