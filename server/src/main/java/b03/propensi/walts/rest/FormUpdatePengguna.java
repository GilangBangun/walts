package b03.propensi.walts.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

import b03.propensi.walts.model.Role;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Lob;
import java.time.LocalDate;

@AllArgsConstructor

@Setter
@Getter
public class FormUpdatePengguna {

   String username;
   String password;
   String namePengguna;
   String phoneNumber;
   String npwp;
}
