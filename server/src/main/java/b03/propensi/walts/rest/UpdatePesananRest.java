package b03.propensi.walts.rest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import b03.propensi.walts.model.Pengguna;
import b03.propensi.walts.model.Pesanan;
import b03.propensi.walts.model.Produk;
import b03.propensi.walts.service.PenggunaRestService;
import b03.propensi.walts.service.PesananRestService;
import b03.propensi.walts.service.ProdukRestService;
import lombok.*;

@AllArgsConstructor
@Setter
@Getter
public class UpdatePesananRest {
    String deadlinePengemasan;
    String deadlinePengiriman;
    String catatan;

}
