package b03.propensi.walts.util.deserializer;

import b03.propensi.walts.model.Pesanan;
import b03.propensi.walts.repository.PenggunaDB;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import b03.propensi.walts.repository.PesananDb;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

public class PesananJsonDeserializer extends JsonDeserializer<Pesanan> {

    @Autowired
    PesananDb pesananDb;
    @Override
    public Pesanan deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException, JsonProcessingException {
        if (jsonParser == null) {
            return null;
        }

        Pesanan pesanan = pesananDb.getById(Long.valueOf(jsonParser.getText()));
        return pesanan;
    }
}