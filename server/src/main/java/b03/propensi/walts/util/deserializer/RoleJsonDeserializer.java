package b03.propensi.walts.util.deserializer;

import java.io.IOException;
import b03.propensi.walts.model.Role;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
public class RoleJsonDeserializer extends JsonDeserializer<Role> {
    @Override
    public Role deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException, JsonProcessingException {
        if (jsonParser == null) {
            return null;
        }

        Role role = new Role();
        role.setIdRole(Long.valueOf(jsonParser.getText()));
        return role;
    }
}