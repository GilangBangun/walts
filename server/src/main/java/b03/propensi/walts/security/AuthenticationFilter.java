package b03.propensi.walts.security;

import b03.propensi.walts.repository.PenggunaDB;
import b03.propensi.walts.model.Pengguna;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    PenggunaDB penggunaDB;

    private AuthenticationManager authenticationManager;

    public AuthenticationFilter(AuthenticationManager authenticationManager) {
    }

    public AuthenticationFilter(AuthenticationManager authenticationManager, ApplicationContext ctx) {
        this.authenticationManager = authenticationManager;
        this.penggunaDB = (PenggunaDB) ctx.getBean(PenggunaDB.class);
        setFilterProcessesUrl("/api/pengguna/login");
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
            throws AuthenticationException {
        try {
            Pengguna temp = new ObjectMapper().readValue(req.getInputStream(), Pengguna.class);
            Optional<Pengguna> optUser = penggunaDB.findByUsername(temp.getUsername());

            Pengguna applicationUser = optUser.get();
            applicationUser.setPassword(temp.getPassword());
            Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
            grantedAuthorities.add(new SimpleGrantedAuthority(applicationUser.getRole().getRoleName()));

            return authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(applicationUser.getUsername(),
                            applicationUser.getPassword(), grantedAuthorities));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
            Authentication auth) throws IOException, ServletException {
        User user = ((User) auth.getPrincipal());
        Pengguna pengguna = penggunaDB.findByUsername(user.getUsername()).get();
        String token = JWT.create()
                .withClaim("sub", pengguna.getUsername())
                .withClaim("id", pengguna.getIdPengguna())
                .withClaim("role", pengguna.getRole().getRoleName())
                .withClaim("nama", pengguna.getNamePengguna())
                .sign(Algorithm.HMAC512(SecurityConstants.KEY.getBytes()));
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("token", token);
        data.put("role", user.getAuthorities());
        String body = new ObjectMapper().writeValueAsString(data);

        res.getWriter().write(body);
        res.getWriter().flush();
    }
}
