package b03.propensi.walts.security;

import b03.propensi.walts.model.Pengguna;
import b03.propensi.walts.repository.PenggunaDB;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.Optional;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private PenggunaDB penggunaDB;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Pengguna> userWrap = penggunaDB.findByUsername(username);
        if (userWrap.isEmpty()) {
            throw new UsernameNotFoundException(username);
        }
        Pengguna user = userWrap.get();
        Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
        grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole().getRoleName()));

        return new User(user.getUsername(), user.getPassword(), grantedAuthorities);
    }
}
