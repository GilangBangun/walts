package b03.propensi.walts.model;

import b03.propensi.walts.util.deserializer.RoleJsonDeserializer;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.sun.istack.NotNull;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
@Entity
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Table(name = "Pengguna")
public class Pengguna implements Serializable {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @JsonDeserialize(using = StringDeserializer.class)
    private String idPengguna;

    @NotNull
    @Column(name = "username", nullable = false)
    private String username;

    @NotNull
    @Lob
    @Column(name = "password", length = 3000, nullable = false)
    private String password;

    @NotNull
    @JsonDeserialize(using = StringDeserializer.class) //
    @Column(name = "namePengguna", nullable = false)
    private String namePengguna;

    @NotNull
    @Column(name = "phoneNumber")
    private String phoneNumber;

    @NotNull
    @Column(name="rataPenilaian")
    private Long rataPenilaian;

    @NotNull // harusnya boleh null
    @Column(name = "npwp")
    private String npwp;

    // (NEW) for dashboard
    @Column(name="jumlahPesanan")
    private Long jumlahPesanan;

    // (NEW) for dashboard
    @Column(name="biayaTotal", nullable = true)
    private Long biayaTotal;

    //ke role
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idRole", referencedColumnName = "idRole", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonDeserialize(using = RoleJsonDeserializer.class)
    private Role role;

    // OLD VERSION
    @ManyToMany(mappedBy = "listPengguna", fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    List<Pesanan> listPesanan;

    // ke penilaian
    @ManyToMany(mappedBy = "listPenerimaPenilai")
    @JsonIgnore
    List<Penilaian> listPenilaian;

    // ke notif
    /*
     * @OneToMany(mappedBy = "pengguna", fetch = FetchType.LAZY)
     * 
     * @OnDelete(action = OnDeleteAction.CASCADE)
     * 
     * @JsonIgnore
     * private List<Notifikasi> listNotifikasi;
     * 
     * //ke validasi
     * 
     * @OneToMany(mappedBy = "pengunggah", fetch = FetchType.LAZY)
     * 
     * @OnDelete(action = OnDeleteAction.CASCADE)
     * 
     * @JsonIgnore
     * private List<ValidasiStatus> listValidasi;
     */

}
