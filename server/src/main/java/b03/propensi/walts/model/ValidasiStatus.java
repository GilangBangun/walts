package b03.propensi.walts.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "ValidasiStatus")
public class ValidasiStatus implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idValidasi;

    @NotNull
    @Column(name="fileValidasi", nullable = false)
    private String fileValidasi;
    //Sementara gini dulu

    @NotNull
    @Column(name="status", nullable = false)
    private String statusPesanan;

    @NotNull
    @Column(name = "waktuValidasi", nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate waktuValidasi;

    @NotNull
    @Column(name="catatan")
    private String catatan;

    // ke pesanan
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idPesanan", referencedColumnName = "idPesanan", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Pesanan pesanan;

    //ke penggunna
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idPengguna", referencedColumnName = "idPengguna", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Pengguna pengunggah;
}
