package b03.propensi.walts.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "Notifikasi")
public class Notifikasi implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idNotifikasi;

    @NotNull
    @Column(name="nameNotifikasi", nullable = false)
    private String nameNotifikasi;

    @NotNull
    @Column(name="konten", nullable = false)
    private String konten;

    @NotNull
    @Column(name="idPesanan", nullable = false)
    private String kodePesanan;

    @NotNull
    @Column(name="tipe", nullable = false)
    private String tipe;

    @NotNull
    @Column(name = "waktuKirim", nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate waktuKirim;

    @NotNull
    @Column(name="isTriggered", nullable = false)
    private Boolean isTriggered;

    //ke pengguna
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idPengguna", referencedColumnName = "idPengguna", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Pengguna pengguna;

}
