package b03.propensi.walts.model;

import b03.propensi.walts.util.deserializer.RoleJsonDeserializer;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers;
import com.sun.istack.NotNull;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)


@Table(name = "Pesanan")
public class Pesanan implements Serializable, Comparable<Pesanan> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonDeserialize(using = NumberDeserializers.LongDeserializer.class)
    private Long idPesanan;

    @NotNull
    @Column(name="kode", nullable = false)
    private String kode;

    @NotNull
    @Column(name="status", nullable = false)
    private String status;

    @NotNull
    @Column(name = "waktuDibuat", nullable = true)
    private LocalDate waktuDibuat;

    @NotNull
    @Column(name = "deadlinePengemasan", nullable = true)
    private LocalDate deadlinePengemasan;

    @NotNull
    @Column(name = "deadlinePengiriman", nullable = false)
    private LocalDate deadlinePengiriman;

    @NotNull
    @Column(name="hargaTotal", nullable = false)
    private Long hargaTotal;

    @NotNull
    @Column(name="catatan")
    private String catatan;

    @NotNull
    @Column(name="nomorKontrak", nullable = false)
    private String nomorKontrak;

    @NotNull // harusnya boleh null
    @Column(name="isUpdate")
    private  Boolean isUpdate;

    @NotNull // harusnya boleh null
    @Column(name="isLatePengiriman")
    private  Boolean isLatePengiriman;

    @NotNull // harusnya boleh null
    @Column(name="isRejected")
    private  Boolean isRejected;

    @NotNull // harusnya boleh null
    @Column(name="latePengiriman")
    private  Long latePengiriman;

    // OLD VERSION
    @ManyToMany
    @JoinTable(
            name = "pesanan_pengguna",
            joinColumns = @JoinColumn(name = "idPengguna"),
            inverseJoinColumns = @JoinColumn(name = "idPesanan")
    )
    List<Pengguna> listPengguna;

    @OneToMany(mappedBy = "pesanan", fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private List<Produk> listProduk;

    // ke validasiStatus
    @OneToMany(mappedBy = "pesanan", fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private List<ValidasiStatus> listValidasi;

//    // (NEW) ke penilaian (1-1)
//    @OneToOne(mappedBy = "pesanan")
//    @JsonIgnore
//    private Penilaian penilaian;

    // Comparator
    @Override
    public int compareTo(Pesanan pesanan) {
        return this.getIdPesanan().compareTo(pesanan.getIdPesanan());
    }
}
