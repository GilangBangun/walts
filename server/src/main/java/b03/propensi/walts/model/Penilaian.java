package b03.propensi.walts.model;

import b03.propensi.walts.util.deserializer.PesananJsonDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "Penilaian")
public class Penilaian implements Serializable, Comparable<Penilaian> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPenilaian;

    // di-comment
//    @NotNull
//    @Column(name="nameNotifikasi")
//    private String nameNotifikasi;

    @NotNull
    @Column(name = "waktu")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate waktu;

    @NotNull
    @Column(name="nilai")
    private Long nilai;

    @NotNull
    @Column(name="feedback")
    private String feedback;

    // (NEW) id pesanan tanpa relasi
    @NotNull
    @Column(name="pesanan")
    private Long pesanan;

    // (NEW) kode pesanan tanpa relasi
    @NotNull
    @Column(name="kodePesanan")
    private String kodePesanan;

    // ke pengguna (m-n)
    @ManyToMany
    @JoinTable(
            name = "penilaian_pengguna",
            joinColumns = @JoinColumn(name = "idPengguna"),
            inverseJoinColumns = @JoinColumn(name = "idPenilaian")
    )
    List<Pengguna> listPenerimaPenilai;

//    // (HOLD) ke pesanan (1-1)
//    @OneToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "idPesanan", referencedColumnName = "idPesanan")
//    @JsonDeserialize(using = PesananJsonDeserializer.class)
//    private Pesanan pesanan;

    @Override
    public int compareTo(Penilaian penilaian) {
        return this.getIdPenilaian().compareTo(penilaian.getIdPenilaian());
    }

//    // ke pengguna (n-1)
//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "idPengguna", referencedColumnName = "idPengguna", nullable = false)
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    private Pengguna penerima;

}
