package b03.propensi.walts.model;

import b03.propensi.walts.util.deserializer.PesananJsonDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import b03.propensi.walts.util.deserializer.PesananJsonDeserializer;


import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;


@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "Produk")
public class Produk implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idProduk;

    @NotNull
    @Column(name="namaProduk", nullable = false)
    private String namaProduk;

    @NotNull
    @Column(name="jumlah", nullable = false)
    private Long jumlah;

    @NotNull
    @Column(name="harga", nullable = false)
    private Long harga;

    @NotNull
    @Column(name="mutu", nullable = true)
    private Long mutu;

    @NotNull
    @Column(name="volume", nullable = false)
    private Long volume;

    @NotNull
    @Column(name="kategori", nullable = false)
    private String kategori;

    @NotNull
    @Column(name="deskripsi", nullable = true)
    private String deskripsi;


    // ke pesanan
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idPesanan", referencedColumnName = "idPesanan", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonDeserialize(using = PesananJsonDeserializer.class)
    private Pesanan pesanan;
}
