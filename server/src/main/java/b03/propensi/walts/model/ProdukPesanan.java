package b03.propensi.walts.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter @Getter
@Entity
@Table(name = "ProdukPesanan")

public class ProdukPesanan implements Serializable {
    @Id
    @Column(name="idProdukPesanan", nullable = false)
    private Long idProdukPesanan;

    @NotNull
    @Column(name="idProduk", nullable = false)
    private Long idProduk;

    @NotNull
    @Column(name="idPesanan", nullable = false)
    private Long idPesanan;

    @NotNull
    @Column(name="namaProduk", nullable = false)
    private String namaProduk;

    @NotNull
    @Column(name="hargaProduk", nullable = false)
    private Long hargaProduk;

    @NotNull
    @Column(name="kategori", nullable = false)
    private String kategori;

    @NotNull
    @Column(name="jumlahProduk", nullable = false)
    private Long jumlahProduk;

    @NotNull
    @Column(name="totalHarga", nullable = false)
    private Long totalHarga;

    @NotNull
    @Column(name = "waktuDibuat", nullable = false)
    private LocalDate waktuDibuat;

}