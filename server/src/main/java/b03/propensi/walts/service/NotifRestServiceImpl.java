package b03.propensi.walts.service;

import b03.propensi.walts.model.Notifikasi;
import b03.propensi.walts.model.Pengguna;
import b03.propensi.walts.model.Pesanan;
import b03.propensi.walts.model.ValidasiStatus;
import b03.propensi.walts.repository.NotifikasiDb;
import b03.propensi.walts.repository.PenggunaDB;

import b03.propensi.walts.repository.PesananDb;
import b03.propensi.walts.repository.ValidasiStatusDb;
import b03.propensi.walts.security.SecurityConstants;

import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
@Transactional

public class NotifRestServiceImpl  implements  NotifRestService{

    @Autowired
    PenggunaDB penggunaDB;

    @Autowired
    PesananDb pesananDb;

    @Autowired
    NotifikasiDb notifikasiDb;

    @Autowired
    ValidasiStatusDb validasiStatusDb;

    @Override
    public void createNotifStatus(Long idPesanan)  {
        Pesanan pesanan = pesananDb.getById(idPesanan);
        String name = String.valueOf(pesanan.getIdPesanan()) ;
        String kode = String.valueOf(pesanan.getKode()) ;


        List<Pengguna> penggunaList = pesanan.getListPengguna();

        for (Pengguna p : penggunaList) {
            Notifikasi notifikasi = new Notifikasi();
            if (pesanan.getStatus().equals("Menunggu Konfirmasi")){
                // vendor
                if (p.getRole().getRoleName().equals("VENDOR")){
                    notifikasi.setNameNotifikasi(name);
                    notifikasi.setKonten("Pesanan sedang menunggu konfirmasi dari vendor");
                    notifikasi.setPengguna(p);
                    notifikasi.setKodePesanan(kode);
                }
                else {
                    continue;
                }


            } else if (pesanan.getStatus().equals("Proses Fabrikasi") ){
                if ( !(p.getRole().getRoleName().equals("TRANSPORTER")) && !(p.getRole().getRoleName().equals("VENDOR"))){
                    notifikasi.setNameNotifikasi(name);
                    notifikasi.setKonten("Pesanan sedang melalui proses fabrikasi");
                    notifikasi.setPengguna(p);
                    notifikasi.setKodePesanan(kode);
                } else {
                    continue;

                }
            } else if (pesanan.getStatus().equals("Sudah Dikemas")) {

                if (!p.getRole().getRoleName().equals("VENDOR")){
                    notifikasi.setNameNotifikasi(name);
                    notifikasi.setKonten("Pesanan sudah selesai dikemas");
                    notifikasi.setPengguna(p);
                    notifikasi.setKodePesanan(kode);
                }
                else {
                    continue;
                }

            } else if (pesanan.getStatus().equals("Proses Pengiriman") ) {

                if (!p.getRole().getRoleName().equals("VENDOR")){
                    notifikasi.setNameNotifikasi(name);
                    notifikasi.setKonten("Pesanan sedang dikirim");
                    notifikasi.setPengguna(p);
                    notifikasi.setKodePesanan(kode);

                }
                else {
                    continue;
                }

            } else if (pesanan.getStatus().equals("Sudah Sampai")) {

                if (!p.getRole().getRoleName().equals("TRANSPORTER")){
                    notifikasi.setNameNotifikasi(name);
                    notifikasi.setKonten("Pesanan sudah sampai di tujuan");
                    notifikasi.setPengguna(p);
                    notifikasi.setKodePesanan(kode);

                }
                else {
                    continue;
                }

            } else if (pesanan.getStatus().equals("Diterima") ) {
                if ((!p.getRole().getRoleName().equals("TRANSPORTER"))) {
                    notifikasi.setNameNotifikasi(name);
                    notifikasi.setKonten("Pesanan sudah diterima");
                    notifikasi.setPengguna(p);
                    notifikasi.setKodePesanan(kode);
                }else {
                    continue;
                }

            } else {
                continue;
            }
            notifikasi.setTipe("Perubahan Status");
            notifikasi.setIsTriggered(true);
            notifikasi.setWaktuKirim(LocalDate.now());
            notifikasiDb.save(notifikasi);

        }

    }

    @Override
    public void createNotifPengirimanTelat(Long idPesanan) {
        Pesanan pesanan = pesananDb.getById(idPesanan);
        String name = String.valueOf(pesanan.getIdPesanan() ) ;

        List<Pengguna> penggunaList = pesanan.getListPengguna();

        String kode = String.valueOf(pesanan.getKode()) ;

        for (Pengguna p : penggunaList) {
            Notifikasi notifikasi = new Notifikasi();
            System.out.println("pengiriman telat");
            System.out.println(p.getNamePengguna());
            notifikasi.setNameNotifikasi(name);
            notifikasi.setKonten("Pesanan melebihi batas pengiriman");
            notifikasi.setPengguna(p);
            notifikasi.setKodePesanan(kode);
            notifikasi.setTipe("Pengiriman Telat");
            notifikasi.setIsTriggered(true);
            notifikasi.setWaktuKirim(LocalDate.now());
            notifikasiDb.save(notifikasi);

            //kecuali transporter

        }

    }

    @Override
    public void checkIsNotifTelat() {
        List<Pesanan> pesanan = pesananDb.findAll();
        List<Pesanan> pesananToday = new ArrayList<Pesanan>();

        for (Pesanan p: pesanan){
            if(p.getDeadlinePengiriman() != null ){
                System.out.println("HAI");
                LocalDate tglPengiriman = p.getDeadlinePengiriman();
                LocalDate now = LocalDate.now();


                System.out.println(tglPengiriman);
                System.out.println(now);

                System.out.println("MAMAA");
                Long telat = tglPengiriman.until(now, ChronoUnit.DAYS);

                if (telat >= 1){
                    System.out.println("ADA SIH MASUK SINI");

                    System.out.println(p.getDeadlinePengiriman().equals(LocalDate.now()));
                    List<ValidasiStatus> validasi = p.getListValidasi();
                    List<String> status = new ArrayList<String>();
                    for (ValidasiStatus v : validasi){
                        status.add(v.getStatusPesanan());
                    }

                    Boolean sudahKirim = status.contains("Proses Pengiriman");

                    if (!sudahKirim){
                        System.out.println("ini juga adaa paketnya belom dikirim");

                        List<Notifikasi> notifAll = notifikasiDb.findAll();
                        String name = String.valueOf(p.getIdPesanan() ) ;
                        List<String> tipeNotif = new ArrayList<String>();

                        String tipe = "Pengiriman Telat";

                        for (Notifikasi n : notifAll){
                            if(n.getNameNotifikasi().equals(name)){
                                tipeNotif.add(n.getTipe());
                            }
                        }


                        Boolean sudahAdaNotif = tipeNotif.contains("Pengiriman Telat");
                        System.out.println(tipeNotif);
                        System.out.println("JANJI: " + sudahAdaNotif);

                        if(!sudahAdaNotif){
                            System.out.println("ADA YANG TELAAT");
                            createNotifPengirimanTelat(p.getIdPesanan());

                        }


                    }





                }
            }



        }

    }

    @Override
    public void isOpen(Long idNotifikasi) {
        Notifikasi notif = notifikasiDb.getById(idNotifikasi);

        notif.setIsTriggered(false);

        notifikasiDb.save(notif);
    }

    @Override
    public Notifikasi findNotifikasiById(Long idNotifikasi) {
        return notifikasiDb.getById(idNotifikasi);
    }


    @Override
    public List<Notifikasi> getAllNotif(String idPengguna) {
        List<Notifikasi> notifAll = notifikasiDb.findAll();
        List<Notifikasi> notifUser = new ArrayList<>();

        for (Notifikasi n : notifAll) {
            if (n.getPengguna().getIdPengguna().equals(idPengguna) && n.getIsTriggered() == true) {
                notifUser.add(n);
            }
        }

        Collections.reverse(notifUser);
        return notifUser;
    }
}
