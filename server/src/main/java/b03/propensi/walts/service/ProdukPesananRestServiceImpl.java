package b03.propensi.walts.service;
import b03.propensi.walts.model.MasterDataProduk;
import b03.propensi.walts.model.Pesanan;
import b03.propensi.walts.model.ProdukPesanan;
import b03.propensi.walts.repository.MasterDataProdukDb;
import b03.propensi.walts.repository.PesananDb;
import b03.propensi.walts.repository.ProdukPesananDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import b03.propensi.walts.model.Produk;
import b03.propensi.walts.repository.ProdukDb;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import java.time.LocalDate;
import java.util.*;

@Service
@Transactional
public class ProdukPesananRestServiceImpl  implements ProdukPesananRestService{
    @Autowired
    PesananRestService pesananRestService;



    @Autowired
    ProdukPesananDb produkPesananDb;

    @Override
    public void createProdukPesanan(Long idPesanan,Long idProduk) {
        Pesanan pesanan = pesananRestService.getPesananById(idPesanan);

        System.out.println("Masuk Create Produk Pesanan");
        System.out.println(idPesanan);
        System.out.println(idProduk);




        for (Produk p : pesanan.getListProduk()) {
            if(p.getIdProduk().equals(idProduk)) {
                ProdukPesanan produkPesanan = new ProdukPesanan();
                produkPesanan.setIdProdukPesanan(idProduk);
                produkPesanan.setIdPesanan(idPesanan);
                produkPesanan.setIdProduk(idProduk);
                produkPesanan.setHargaProduk(p.getHarga());
                produkPesanan.setJumlahProduk(p.getJumlah());
                produkPesanan.setNamaProduk(p.getNamaProduk());

                if(pesanan.getWaktuDibuat() == null){
                    produkPesanan.setWaktuDibuat(pesanan.getDeadlinePengiriman());
                } else {
                    produkPesanan.setWaktuDibuat(pesanan.getWaktuDibuat());

                }


                produkPesanan.setTotalHarga(p.getHarga() * p.getJumlah());
                produkPesanan.setKategori(p.getKategori());

                produkPesananDb.save(produkPesanan);
            }
        }
    }



    @Override
    public List<ProdukPesanan> getAllProdukPesanan() {
        return produkPesananDb.findAll();
    }


    @Override
    public ProdukPesanan findProdukPesananbyIdProduk(Long idProdukPesanan) {
        Optional<ProdukPesanan> produkPesanan = produkPesananDb.findById(idProdukPesanan);
        if (produkPesanan.isPresent()) return produkPesanan.get();
        else return null;

    }


    @Override
    public void updateProdukPesananSementara() {
        List<Pesanan> listPesanan = pesananRestService.getAllPesanan();
        List<ProdukPesanan> listProdukPesanan = getAllProdukPesanan();

        for (Pesanan p : listPesanan){
            for (Produk pr : p.getListProduk()){
                Optional<ProdukPesanan> produkPesanan  = produkPesananDb.findByIdProduk(pr.getIdProduk());
                System.out.println("KITA DI UPDATE SEMENTARA");
                System.out.println("ADA GAK "  +  produkPesanan);

                if (produkPesanan.isEmpty()){
                    createProdukPesanan( p.getIdPesanan(), pr.getIdProduk());
                }
            }

            if (p.getWaktuDibuat() == null){
                p.setWaktuDibuat(p.getDeadlinePengiriman());
            }
        }
    }
}
