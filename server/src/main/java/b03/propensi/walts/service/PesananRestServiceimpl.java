package b03.propensi.walts.service;

import b03.propensi.walts.model.ValidasiStatus;
import b03.propensi.walts.repository.ValidasiStatusDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import b03.propensi.walts.model.Pengguna;
import b03.propensi.walts.model.Pesanan;
import b03.propensi.walts.model.Produk;
import b03.propensi.walts.repository.PesananDb;
import b03.propensi.walts.rest.PesananRest;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Service
@Transactional
public class PesananRestServiceimpl implements PesananRestService{
    @Autowired
    PesananDb pesananDb;

    @Autowired
    ValidasiStatusDb validasiStatusDb;

    @Autowired
    PenggunaRestService penggunaRestService;

    @Autowired
    NotifRestService notifRestService;

    Long counter = Long.valueOf(1);

    @Override
    public Pesanan getPesananById(Long idPesanan){
        Optional<Pesanan> pesanan = pesananDb.findByIdPesanan(idPesanan);
        if (pesanan.isPresent()) return pesanan.get();
        else return null;
    }

    @Override
    public Pesanan createPesanan(PesananRest pesanan) throws Exception{
        Pengguna pembuat = penggunaRestService.getPenggunaById(pesanan.getPembuat());
        Pengguna vendor = penggunaRestService.getPenggunaById(pesanan.getVendor());
        Pengguna transporter = penggunaRestService.getPenggunaById(pesanan.getTransporter());
        System.out.println(" Pembuat: " + pembuat.getNamePengguna());
        System.out.println(" Transporter: " + transporter.getNamePengguna());
        Pesanan newPesanan = pesanan.convertToPesanan(counter, pembuat, vendor, transporter);
        newPesanan.setIsUpdate(true);
        newPesanan.setIsLatePengiriman(false);

        LocalDate now = LocalDate.now();
        newPesanan.setWaktuDibuat(now);
        Pesanan savePesanan =  pesananDb.save(newPesanan);
        System.out.println("List pengguna (size): "+savePesanan.getListPengguna().size());
        System.out.println(savePesanan.getIdPesanan());
        notifRestService.createNotifStatus(savePesanan.getIdPesanan());
        counter ++;
        vendor.setJumlahPesanan((long) vendor.getListPesanan().size());
        return pesananDb.save(newPesanan);
    }

    @Override
    public List<Pesanan> getAllPesanan(){
        return pesananDb.findAll();
    }

    @Override
    public List<Object> getTotalPesananByMonth() {
        List<String> month = new ArrayList<String>(
                Arrays.asList("April","May","June", "July", "August", "September", "October","November","December"));
        List<Long> pesanan = new ArrayList<>();
        System.out.println(month);
        List<Pesanan> allPesanan = getAllPesanan();
        for (int i = 0; i < month.size(); i++){
            pesanan.add(Long.valueOf("0"));
        }
        for (Pesanan p : allPesanan) {
            if (p.getDeadlinePengiriman() != null) {
                String m = p.getDeadlinePengiriman().getMonth().toString().toLowerCase(Locale.ROOT);
                m = m.substring(0, 1).toUpperCase() + m.substring(1);
                int idx = month.indexOf(m);
                pesanan.set(idx, pesanan.get(idx) + 1);
            }
        }
        List<Object> res = new ArrayList<>();
        res.add(month);
        res.add(pesanan);

        return res;
    }

    @Override
    public void updatePesanan(Pesanan pesanan){
        pesananDb.save(pesanan);
    }

    @Override
    public void updateStatusPesanan(Pesanan pesanan, ValidasiStatus validasiStatus) {

        if (pesanan.getStatus().equalsIgnoreCase("Menunggu Konfirmasi")) {
            pesanan.setStatus("Proses Fabrikasi");
        } else if (pesanan.getStatus().equalsIgnoreCase("Proses Fabrikasi")) {
            pesanan.setStatus("Sudah Dikemas");
        } else if (pesanan.getStatus().equalsIgnoreCase("Sudah Dikemas")) {
            pesanan.setStatus("Proses Pengiriman");
            LocalDate tglPengiriman = pesanan.getDeadlinePengiriman();
            LocalDate now = LocalDate.now();
            System.out.println(tglPengiriman);
            System.out.println(now);
            Long telat = tglPengiriman.until(now, ChronoUnit.DAYS);
            if (telat > 0) {
                pesanan.setLatePengiriman(telat);
            }
        } else if (pesanan.getStatus().equalsIgnoreCase("Proses Pengiriman")) {
            pesanan.setStatus("Sudah Sampai");
        } else if (pesanan.getStatus().equalsIgnoreCase("Sudah Sampai")) {
            pesanan.setStatus("Selesai");
        } else {
            pesanan.setStatus("-");
        }

        pesananDb.save(pesanan);
        notifRestService.createNotifStatus(pesanan.getIdPesanan());
        pesanan.setIsUpdate(true);

        validasiStatus.setStatusPesanan(pesanan.getStatus());
        validasiStatusDb.save(validasiStatus);

    }

    @Override
    public void openNotif(Long idPesanan) {
        Pesanan pesanan = pesananDb.getById(idPesanan);
        pesanan.setIsUpdate(false);
        pesananDb.save(pesanan);
    }

    @Override
    public void updateHasilPesanan(Pesanan pesanan, String hasil) {
        if (hasil.equalsIgnoreCase("Diterima")) {
            pesanan.setIsRejected(false);
            // pesanan.setStatus("Diterima");
        } else if (hasil.equalsIgnoreCase("Ditolak")) {
            pesanan.setIsRejected(true);
            // pesanan.setStatus("Ditolak");
        }

        pesananDb.save(pesanan);
    }

}