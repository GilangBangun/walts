package b03.propensi.walts.service;

import b03.propensi.walts.model.Pengguna;
import b03.propensi.walts.model.Pesanan;
import b03.propensi.walts.repository.PenggunaDB;

import b03.propensi.walts.security.SecurityConstants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PenggunaRestServiceImpl implements PenggunaRestService {

    @Autowired
    PenggunaDB penggunaDB;

    @Override
    public Pengguna getPenggunaById(String idPengguna) {
        Optional<Pengguna> pengguna = penggunaDB.findByIdPengguna(idPengguna);
        if (pengguna.isPresent()) return pengguna.get();
        else return null;
    }

    @Override
    public List<Pengguna> getAllPengguna() {
        return penggunaDB.findAll();
    }

    @Override
    public String encrypt(String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String hash = encoder.encode(password);
        return hash;
    }

    @Override
    public void createPengguna(Pengguna pengguna) throws Exception {
        if (penggunaDB.findByUsername(pengguna.getUsername()).isPresent()) {
            throw new Exception("Username already exist");
        }

        String rawPass = pengguna.getPassword();
        pengguna.setPassword(encrypt(rawPass));

        pengguna.setListPesanan(new ArrayList<Pesanan>());
        pengguna.setJumlahPesanan((long) pengguna.getListPesanan().size());
        
        penggunaDB.save(pengguna);
    }
    
    @Override
    public Pengguna getPenggunaFromJWT(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        if (token != null && token.length() != 0) {
          String user = JWT.require(Algorithm.HMAC512(SecurityConstants.KEY.getBytes()))
              .build()
              .verify(token.replace("Bearer ", ""))
              .getSubject();
          return penggunaDB.findByUsername(user).get();
        }
        return null;
      }

    @Override
    public void updatePengguna(Pengguna pengguna){
        pengguna.setPassword(encrypt(pengguna.getPassword()));
        penggunaDB.save(pengguna);
    }

    @Override
    public void deletePengguna(Pengguna pengguna){
        penggunaDB.delete(pengguna);
    }
    
}
