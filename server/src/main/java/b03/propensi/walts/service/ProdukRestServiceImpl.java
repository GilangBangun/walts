package b03.propensi.walts.service;


import b03.propensi.walts.model.MasterDataProduk;
import b03.propensi.walts.model.Pesanan;
import b03.propensi.walts.model.ProdukPesanan;
import b03.propensi.walts.repository.MasterDataProdukDb;
import b03.propensi.walts.repository.PesananDb;
import b03.propensi.walts.repository.ProdukPesananDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import b03.propensi.walts.model.Produk;
import b03.propensi.walts.repository.ProdukDb;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import java.time.LocalDate;
import java.util.*;

@Service
@Transactional
public class ProdukRestServiceImpl implements ProdukRestService{
    @Autowired
    ProdukDb produkDb;

    @Autowired
    PesananDb pesananDb;

    @Autowired
    ProdukPesananRestService produkPesananRestService;

    @Autowired
    MasterDataProdukDb masterDataProdukDb;



    @Override
    public Produk getProdukById(Long idProduk){
        Optional<Produk> produk = produkDb.findByIdProduk(idProduk);
        if (produk.isPresent()) return produk.get();
        else return null;
    }

    @Override
    public Produk createProduk(Produk produk, Long idPesanan) throws Exception{
        produkDb.save(produk);

        System.out.println("Berhasil create");
        produkPesananRestService.createProdukPesanan(idPesanan, produk.getIdProduk());
        return produk;
    }

    @Override
    public List<Produk> getAllProduk(){
        return produkDb.findAll();
    }

    @Override
    public List<String> getAllNamaProduk() {
        List<Produk> listProduk = getAllProduk();
        List<String> namaProduk = new ArrayList<>();
        for (Produk produk : listProduk) {
            namaProduk.add(produk.getNamaProduk());
        }
        return namaProduk;
    }

    @Override
    public  List<Object> getAllKategoriProduk() {

        Hashtable<String, Integer> finalKategori = new Hashtable<String, Integer>();
        List<Produk> listProduk = getAllProduk();
        List <String> kategori = new ArrayList<String>();

        List<Pesanan> listPesanan = pesananDb.findAll();


        for (Produk produk : listProduk) {
            if (!kategori.contains(produk.getKategori())) {
                kategori.add(produk.getKategori());
            }
        }

        System.out.println("INI ARRAYLIST" +  kategori);

        List<Long> totalKategori = new ArrayList<>();
        List<Long> hargaKategori = new ArrayList<>();


        for (int x = 0; x < kategori.size(); x++){
            totalKategori.add(Long.parseLong("0"));
            hargaKategori.add(Long.parseLong("0"));
        }

        for (Pesanan p : listPesanan){
            for (Produk pro : p.getListProduk()){
                if (kategori.contains(pro.getKategori())){
                    int idx = kategori.indexOf(pro.getKategori());
                    System.out.println("KATEGORI: " + pro.getKategori());
                    totalKategori.set(idx, totalKategori.get(idx) + pro.getJumlah());
                    Long harga = Long.valueOf ( pro.getHarga() * pro.getJumlah());

                    hargaKategori.set(idx, hargaKategori.get(idx) + harga);

                }

            }
        }


        List<Object> res = new ArrayList<Object>();
        res.add(kategori);
        res.add(totalKategori);
        res.add(hargaKategori);
        System.out.println("FINAL KATEGORI: " + res);
        System.out.println("FINAL KATEGORI[0]: " + res.get(0));
        System.out.println("FINAL KATEGORI[1]: " + res.get(1));


        return res;
    }

    // (NEW) Method get nama produk yang ada di Master Data
    @Override
    public List<MasterDataProduk> getMasterDataProduk() {
        List<MasterDataProduk> listMDProduk = masterDataProdukDb.findAll();
        List<String> namaMDProduk = new ArrayList<>();
        for (MasterDataProduk produk : listMDProduk) {
            namaMDProduk.add(produk.getNamaMDProduk());
        }
        return listMDProduk;
    }


    // (NEW) Method tambah nama produk ke Master Data
    @Override
    public MasterDataProduk addMDProduk(MasterDataProduk produk) throws Exception{
        return masterDataProdukDb.save(produk);
    }

    @Override
    public List<Produk> getAllProdukByName(String temp){
        List<Produk> result = new ArrayList<Produk>();
        temp = temp.toLowerCase();

        if(temp.equals("") || temp.equals(".")){
            System.out.println("Masuk ke all nih");
            return getAllProduk();
        }

        for (Produk p : getAllProduk()){
            if(p.getNamaProduk().toLowerCase().contains(temp)){
                result.add(p);
            }
        }
        if(result.size() != 0){
            return result;
        } return null;
    }
}
