package b03.propensi.walts.service;

import b03.propensi.walts.model.Pengguna;

import javax.servlet.http.HttpServletRequest;


import java.util.List;

public interface PenggunaRestService {
    Pengguna getPenggunaById(String idPengguna);

    List<Pengguna> getAllPengguna();

    String encrypt(String password);

    void createPengguna(Pengguna pengguna) throws Exception;
    void updatePengguna(Pengguna pengguna);
    void deletePengguna(Pengguna pengguna);

    Pengguna getPenggunaFromJWT(HttpServletRequest request);
}
