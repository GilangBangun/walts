package b03.propensi.walts.service;

import b03.propensi.walts.model.Notifikasi;
import b03.propensi.walts.model.Pengguna;
import b03.propensi.walts.model.Pesanan;
import b03.propensi.walts.model.ValidasiStatus;
import b03.propensi.walts.rest.PesananRest;

import javax.servlet.http.HttpServletRequest;


import java.util.List;

public interface NotifRestService {
    void createNotifStatus(Long idPesanan);
    void createNotifPengirimanTelat(Long idPesanan);
    void checkIsNotifTelat();
    void isOpen(Long idNotifikasi);
    Notifikasi findNotifikasiById (Long idNotifikasi);


    List<Notifikasi> getAllNotif (String idPengguna);

}
