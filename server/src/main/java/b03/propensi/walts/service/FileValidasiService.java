package b03.propensi.walts.service;

import org.springframework.web.multipart.MultipartFile;

public interface FileValidasiService {
    String uploadFile(MultipartFile file);
}
