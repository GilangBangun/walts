package b03.propensi.walts.service;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import b03.propensi.walts.model.Role;
import b03.propensi.walts.repository.RoleDb;

@Service
@Transactional
public class RoleRestServiceImpl implements RoleRestService{
    @Autowired
    RoleDb roleDb;

    @Override
    public List<Role> getListRole() {
        return roleDb.findAll();
    }

    @Override
    public Role getRoleById(Long idRole){
        return roleDb.findByIdRole(idRole).get();
    }
}
