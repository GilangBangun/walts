package b03.propensi.walts.service;

import b03.propensi.walts.model.Pengguna;
import b03.propensi.walts.model.Penilaian;

import java.util.List;

public interface PenilaianRestService {

    List<Penilaian> getPenilaianVendor(Pengguna vendor);
    void createPenilaian(Penilaian penilaian) throws Exception;
}
