package b03.propensi.walts.service;

import b03.propensi.walts.model.Pengguna;
import b03.propensi.walts.model.Penilaian;
import b03.propensi.walts.repository.PenggunaDB;
import b03.propensi.walts.repository.PenilaianDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PenilaianRestServiceImpl implements PenilaianRestService {

    @Autowired
    PenilaianDb penilaianDb;

    @Override
    public List<Penilaian> getPenilaianVendor(Pengguna vendor) {
        return null;
    }

    @Override
    public void createPenilaian(Penilaian penilaian) throws Exception {
        penilaianDb.save(penilaian);
    }
}
