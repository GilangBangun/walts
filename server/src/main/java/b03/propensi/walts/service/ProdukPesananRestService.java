package b03.propensi.walts.service;

import b03.propensi.walts.model.ProdukPesanan;

import java.util.List;

public interface ProdukPesananRestService {

    void createProdukPesanan (Long idPesanan, Long idProduk);
    List<ProdukPesanan> getAllProdukPesanan();
    void updateProdukPesananSementara ();
    ProdukPesanan findProdukPesananbyIdProduk (Long idProdukPesanan);
}
