package b03.propensi.walts.service;

import org.springframework.web.multipart.MultipartFile;

import b03.propensi.walts.model.FileStorage;

public interface FileStorageRestService {
    public void FileStorageService(FileStorage fileStorage) throws Exception;
    public String storeFile(MultipartFile file);
    public org.springframework.core.io.Resource loadFileAsResource(String fileName);
}
