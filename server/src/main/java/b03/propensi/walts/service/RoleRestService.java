package b03.propensi.walts.service;

import b03.propensi.walts.model.Role;

import java.util.List;

public interface RoleRestService {
    List<Role> getListRole();
    Role getRoleById(Long idRole);
}
