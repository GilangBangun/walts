package b03.propensi.walts.service;

import javax.servlet.http.HttpServletRequest;

import b03.propensi.walts.model.Pesanan;
import b03.propensi.walts.model.ValidasiStatus;
import b03.propensi.walts.rest.*;

import java.util.List;

public interface PesananRestService {
    Pesanan getPesananById(Long idPesanan);
    Pesanan createPesanan(PesananRest pesanan) throws Exception;

    void updatePesanan(Pesanan pesanan);
    void updateStatusPesanan(Pesanan pesanan, ValidasiStatus validasiStatus);
    void openNotif(Long idPesanan);
    void updateHasilPesanan(Pesanan pesanan, String hasil);

    List<Pesanan> getAllPesanan();

    List<Object> getTotalPesananByMonth();


}
