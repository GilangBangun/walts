package b03.propensi.walts.service;

import javax.servlet.http.HttpServletRequest;

import b03.propensi.walts.model.MasterDataProduk;
import b03.propensi.walts.model.Produk;
import b03.propensi.walts.model.ProdukPesanan;

import java.util.Hashtable;
import java.util.List;
import java.util.Objects;

public interface ProdukRestService {
    Produk getProdukById(Long idProduk);
    Produk createProduk(Produk pesanan, Long idPesanan) throws Exception;
    MasterDataProduk addMDProduk(MasterDataProduk produk) throws Exception;

    List<Produk> getAllProduk();
    List<String> getAllNamaProduk();
    List<Object> getAllKategoriProduk();
    List<Produk> getAllProdukByName(String temp);
    List<MasterDataProduk> getMasterDataProduk();

}
