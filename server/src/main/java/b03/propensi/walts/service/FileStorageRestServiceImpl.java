package b03.propensi.walts.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import b03.propensi.walts.model.FileStorage;

@Service
public class FileStorageRestServiceImpl implements FileStorageRestService{
    private Path fileStorageLocation;

    @Override
	public void FileStorageService(FileStorage fileStorage){
		this.fileStorageLocation = Paths.get(fileStorage.getUploadDir()).toAbsolutePath().normalize();
		
		try {
			Files.createDirectories(this.fileStorageLocation);
			
		}catch(Exception e) {
			throw new FileStorageException("Could not create the directory to upload");
		}
	}

	@Override
    public String storeFile(MultipartFile file) {
		
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		try {
			Path targetLocation = this.fileStorageLocation.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation,StandardCopyOption.REPLACE_EXISTING);
			
			return fileName;
		}catch(IOException e) {
			throw new FileStorageException("Could not store file"+fileName + ". Please try again!",e);
		}
	}

	@Override
    public Resource loadFileAsResource(String fileName) {
		try {
			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if(resource.exists()) {
				return resource;
			}else {
				throw new MyFileNotFoundException("File not found " + fileName);
			}
		}catch(Exception e) {
			throw new MyFileNotFoundException("File not found " + fileName);
		}
	}
}
