package b03.propensi.walts.controller;
import b03.propensi.walts.model.Role;
import b03.propensi.walts.rest.JsonResponse;
import b03.propensi.walts.rest.Result;
import b03.propensi.walts.service.RoleRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "https://walts.vercel.app")
@RequestMapping("/api/role")
public class RoleController {

    @Autowired
    private RoleRestService roleService;

    @GetMapping(value = "/list")
    private ResponseEntity<Result<List<Role>>> healthCheck() {
        JsonResponse resGenerator = new JsonResponse<List<Role>>();
        return resGenerator.response("List Role", roleService.getListRole());
    }
}
