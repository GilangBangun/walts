package b03.propensi.walts.controller;

import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import b03.propensi.walts.model.*;
import b03.propensi.walts.service.PenggunaRestService;
import b03.propensi.walts.service.ProdukPesananRestService;
import org.apache.tomcat.util.json.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import b03.propensi.walts.rest.JsonResponse;
import b03.propensi.walts.rest.Result;
import b03.propensi.walts.service.PesananRestService;
import b03.propensi.walts.service.ProdukRestService;

@RestController
@CrossOrigin(origins = "https://walts.vercel.app")
@RequestMapping("/api/produk")
public class ProdukController {
    @Autowired
    ProdukRestService produkRestService;

    @Autowired
    PesananRestService pesananRestService;

    @Autowired
    PenggunaRestService penggunaRestService;

    @Autowired
    ProdukPesananRestService produkPesananRestService;

    //create pesanan
    @PostMapping(value = "/create")
    public ResponseEntity<Result<Produk>> registerProduk(
            @RequestBody Produk produk,
            HttpServletRequest request
    ) throws Exception {
        System.out.println("Masuk backend");
        JsonResponse<Produk> result = new JsonResponse<Produk>();
        try {
            Pesanan pesanan =  produk.getPesanan();
            System.out.println("Get pesanan " + pesanan.getIdPesanan());
            System.out.println(produk.getNamaProduk());
            System.out.println("Update pesanan");
            produkRestService.createProduk(produk, pesanan.getIdPesanan());
            pesanan.setHargaTotal(pesanan.getHargaTotal() + (produk.getHarga() *  produk.getJumlah()));
            pesananRestService.updatePesanan(pesanan);
            System.out.println("Create produk");

            for (Pengguna vendor: produk.getPesanan().getListPengguna()
            ) {
                if (vendor.getRole().getIdRole() == 3) {
                    if(vendor.getBiayaTotal() == null) {
                        System.out.println("masuk null");
                        vendor.setBiayaTotal(produk.getHarga() * produk.getJumlah());
                        System.out.println("dapet 1 Rp"+ vendor.getBiayaTotal());
                    } else {
                        System.out.println("masuk tidak null");
                        vendor.setBiayaTotal(vendor.getBiayaTotal() + (produk.getHarga() * produk.getJumlah()));
                        System.out.println("dapet 2 Rp"+ vendor.getBiayaTotal());
                    }

                    penggunaRestService.updatePengguna(vendor);
                    System.out.println("!!dapet nih!! Rp"+ vendor.getBiayaTotal());
                    System.out.println("masuk ke if 2");
                }
            }
        } catch (Exception e) {
            System.out.println("error ya");
            System.out.println(e.getMessage());

            return result.response(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return result.response("Produk berhasil dibuat!");

    }

    // list produk
    @GetMapping(value = "/all")
    public ResponseEntity<Result<List<Produk>>> getAllProduk() throws Exception {
        JsonResponse<List<Produk>> result = new JsonResponse<List<Produk>>();
        List<Produk> listProduk = produkRestService.getAllProduk();
        return result.response("List produk", listProduk);
    }

    // list nama produk
    @GetMapping(value = "/all/name")
    public ResponseEntity<Result<List<String>>> getAllNamaProduk() throws Exception {
        JsonResponse<List<String>> result = new JsonResponse<>();
        List<String> listProduk = produkRestService.getAllNamaProduk();
        return result.response("List nama produk", listProduk);
    }


    // list nama produk
    @GetMapping(value = "/all/kategori")
    public ResponseEntity<Result<List<Object>>> getAllKategori() throws Exception {
        JsonResponse<List<Object>> result = new JsonResponse<>();
        List<Object> listKategori = produkRestService.getAllKategoriProduk();
        return result.response("List  kategori", listKategori);
    }

    // (NEW) API list nama produk di Master Data
    @GetMapping(value = "/master")
    public ResponseEntity<Result<List<MasterDataProduk>>> getMasterDataProduk() throws Exception {
        JsonResponse<List<MasterDataProduk>> result = new JsonResponse<>();
        List<MasterDataProduk> listMDProduk = produkRestService.getMasterDataProduk();
        return result.response("Master data produk", listMDProduk);
    }

    // (NEW) API tambah nama produk ke Master Data
    @PostMapping(value = "/master/add")
    public ResponseEntity<Result<MasterDataProduk>> addMasterDataProduk(
            @RequestBody MasterDataProduk masterDataProduk,
            HttpServletRequest request
    ) throws Exception {
        JsonResponse<MasterDataProduk> result = new JsonResponse<MasterDataProduk>();
        try {
            produkRestService.addMDProduk(masterDataProduk);
        } catch (Exception e) {
            return result.response(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return result.response("Produk berhasil ditambahkan ke Master Data!", masterDataProduk);
    }

    // list pesanan by search string
    @GetMapping(value = "/search")
    public ResponseEntity<Result<List<Produk>>> getAllPesananBySearch(@RequestParam String q) throws Exception {
        JsonResponse<List<Produk>> result = new JsonResponse<List<Produk>>();
        try {
            List<Produk> listProduk = produkRestService.getAllProdukByName(q);
            
            if(listProduk == null){
                throw new Exception();
            }

            return result.response("List produk", listProduk);

        } catch (Exception e) {
            return result.response("List tidak tersedia");
        }
    }


    @GetMapping(value = "/all/produk")
    public ResponseEntity<Result<List<ProdukPesanan>>> getAllProdukPesanan() throws Exception {
        JsonResponse<List<ProdukPesanan>> result = new JsonResponse<>();
        List<ProdukPesanan> listProdukPesanan = produkPesananRestService.getAllProdukPesanan();
        return result.response("List  produk pesanan", listProdukPesanan);
    }
}
