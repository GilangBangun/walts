package b03.propensi.walts.controller;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import b03.propensi.walts.model.*;
import b03.propensi.walts.rest.FormUpdatePengguna;
import b03.propensi.walts.service.NotifRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import b03.propensi.walts.rest.Result;
import b03.propensi.walts.rest.JsonResponse;
import b03.propensi.walts.service.PenggunaRestService;
import b03.propensi.walts.service.PesananRestService;
import b03.propensi.walts.service.RoleRestService;

@RestController
@CrossOrigin(origins = "https://walts.vercel.app/")
@RequestMapping("/api/pengguna")
public class PenggunaController {

    @Autowired
    PenggunaRestService penggunaRestService;

    @Autowired
    RoleRestService roleRestService;

    @Autowired
    NotifRestService notifRestService;

    // Create pengguna (don)
    @PostMapping(value = "/register")
    public ResponseEntity<Result<Pengguna>> registerPengguna(
            @RequestBody Pengguna pengguna,
            HttpServletRequest request) throws Exception {
        JsonResponse<Pengguna> result = new JsonResponse<Pengguna>();
        try {
            penggunaRestService.createPengguna(pengguna);
        } catch (Exception e) {
            return result.response(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return result.response("Pengguna berhasil dibuat!", pengguna);
    }

    // Read all pengguna (done)
    @GetMapping(value = "/all")
    public ResponseEntity<Result<List<Pengguna>>> getAllPengguna() throws Exception {
        JsonResponse<List<Pengguna>> result = new JsonResponse<List<Pengguna>>();
        List<Pengguna> listPengguna = penggunaRestService.getAllPengguna();
        return result.response("List pengguna", listPengguna);
    }

    // Read detail pengguna (done)
    @GetMapping(value = "detail/{idPengguna}")
    public ResponseEntity<Result<Pengguna>> getDetailPengguna(
            @PathVariable String idPengguna,
            HttpServletRequest request) {
        JsonResponse<Pengguna> result = new JsonResponse<Pengguna>();
        Pengguna pengguna = penggunaRestService.getPenggunaById(idPengguna);
        return result.response("Data pengguna", pengguna);
    }

    // Update pengguna (perlu diperiksa lagi)
    @PutMapping(value = "/update/{idPengguna}")
    public ResponseEntity<Result<Pengguna>> updatePengguna(
            @PathVariable String idPengguna,
            @RequestBody FormUpdatePengguna updatePengguna,
            HttpServletRequest request) {
        Pengguna pengguna = penggunaRestService.getPenggunaById(idPengguna);

        pengguna.setUsername(updatePengguna.getUsername());

        pengguna.setPassword(updatePengguna.getPassword());
        pengguna.setNpwp(updatePengguna.getNpwp());
        pengguna.setNamePengguna(updatePengguna.getNamePengguna());
        pengguna.setPhoneNumber(updatePengguna.getPhoneNumber());

        penggunaRestService.updatePengguna(pengguna);

        JsonResponse<Pengguna> result = new JsonResponse<Pengguna>();
        return result.response("Pengguna berhasil diperbarui!", pengguna);
    }

    // Delete pengguna (done)
    @GetMapping(value = "/delete/{idPengguna}")
    public ResponseEntity<Result<Pengguna>> updatePengguna(
            @PathVariable String idPengguna,
            HttpServletRequest request) {
        Pengguna pengguna = penggunaRestService.getPenggunaById(idPengguna);

        penggunaRestService.deletePengguna(pengguna);

        JsonResponse<Pengguna> result = new JsonResponse<Pengguna>();
        return result.response("Pengguna berhasil dihapus!");
    }

    // Read diri sendiri (hold)
    @GetMapping(value = "/self")
    public ResponseEntity<Result<Pengguna>> getPengguna(
            HttpServletRequest request) throws Exception {
        JsonResponse<Pengguna> result = new JsonResponse<Pengguna>();

        Pengguna user = penggunaRestService.getPenggunaFromJWT(request);
        System.out.println(user.getNamePengguna());
        return result.response("Data pengguna", user);
    }

    // Read pesanan pengguna (done)
    @GetMapping(value = "detail/{idPengguna}/pesanan")
    public ResponseEntity<Result<List<Pesanan>>> getPesananPengguna(
            @PathVariable String idPengguna,
            HttpServletRequest request) {
        JsonResponse<List<Pesanan>> result = new JsonResponse<>();
        Pengguna pengguna = penggunaRestService.getPenggunaById(idPengguna);

        List<Pesanan> listPesanan = pengguna.getListPesanan();
        // System.out.println("List pesanan pengguna (raw): " +
        // listPesanan.get(0).getIdPesanan());

        Collections.sort(pengguna.getListPesanan(), Collections.reverseOrder());
        // System.out.println("List pesanan pengguna (sorted): " +
        // listPesanan.get(0).getIdPesanan());
        notifRestService.checkIsNotifTelat();

        notifRestService.checkIsNotifTelat();
        return result.response("List pesanan pengguna", listPesanan);
    }

    // Read penilaian suatu vendor
    @GetMapping(value = "detail/{idPengguna}/penilaian")
    public ResponseEntity<Result<List<Penilaian>>> getPenilaianVendor(
            @PathVariable String idPengguna,
            HttpServletRequest request) {
        JsonResponse<List<Penilaian>> result = new JsonResponse<>();
        Pengguna pengguna = penggunaRestService.getPenggunaById(idPengguna);

        List<Penilaian> listPenilaian = pengguna.getListPenilaian();
        System.out.println("List penilaian vendor (raw): " + listPenilaian.get(0).getIdPenilaian());

        Collections.sort(pengguna.getListPenilaian(), Collections.reverseOrder());
        System.out.println("List penilaian vendor (sorted): " + listPenilaian.get(0).getIdPenilaian());

        return result.response("List penilaian vendor" + pengguna.getNamePengguna(), listPenilaian);
    }

    @GetMapping(value = "detail/{idPengguna}/notif")
    public ResponseEntity<Result<List<Notifikasi>>> getNotifPengguna(
            @PathVariable String idPengguna,
            HttpServletRequest request) {
        JsonResponse<List<Notifikasi>> result = new JsonResponse<>();
        Pengguna pengguna = penggunaRestService.getPenggunaById(idPengguna);

        List<Notifikasi> listNotifikasi = notifRestService.getAllNotif(idPengguna);
        System.out.println("List pesanan pengguna (raw): " + listNotifikasi);

        return result.response("List Notifikasi", listNotifikasi);
    }

    @GetMapping(value = "/vendorJenis")
    public HashMap<String, Object> getJenisVendor() {
        ArrayList<Object> al = new ArrayList<Object>();

        List<Pengguna> listVendor = penggunaRestService.getAllPengguna();
        int counterVendorBaik = 0;
        int counterVendorBuruk = 0;

        System.out.println("list Pengguna: " + listVendor);
        for (Pengguna v : listVendor) {
            if (v.getRataPenilaian() != null) {
                if (v.getRole().getIdRole() == 3 && v.getRataPenilaian() >= 75) {
                    counterVendorBaik++;
                } else if (v.getRole().getIdRole() == 3) {
                    counterVendorBuruk++;
                }
            }
        }

        // Vendor Performa Baik
        HashMap<String, Object> vendorBaik = new HashMap<>();
        vendorBaik.put("nama", "Vendor Performa Baik");
        vendorBaik.put("jumlah", counterVendorBaik);
        al.add(vendorBaik);

        // Vendor Performa Buruk
        HashMap<String, Object> vendorBuruk = new HashMap<>();
        vendorBuruk.put("nama", "Vendor Performa Buruk");
        vendorBuruk.put("jumlah", counterVendorBuruk);
        al.add(vendorBuruk);

        HashMap<String, Object> finalMap = new HashMap<>();
        finalMap.put("data", al);
        return finalMap;
    }
}
