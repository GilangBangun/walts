package b03.propensi.walts.controller;

import b03.propensi.walts.model.Pengguna;
import b03.propensi.walts.model.Pesanan;
import b03.propensi.walts.model.ValidasiStatus;
import b03.propensi.walts.repository.ValidasiStatusDb;
import b03.propensi.walts.service.FileValidasiService;
import b03.propensi.walts.service.PenggunaRestService;
import b03.propensi.walts.service.PesananRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/file")
public class FileValidasiController {

    @Autowired
    FileValidasiService fileValidasiService;

    @Autowired
    PesananRestService pesananRestService;

    @Autowired
    PenggunaRestService penggunaRestService;

    @PostMapping(value = "/{idPesanan}")
    public ResponseEntity<Map<String, String>> uploadFile(
            @PathVariable Long idPesanan,
            @RequestParam("file") MultipartFile file,
            @RequestParam String catatan,
            @RequestParam String hasil,
            HttpServletRequest request
    ) {
        String publicURL = fileValidasiService.uploadFile(file);

        Pengguna user =  penggunaRestService.getPenggunaFromJWT(request);   // get current user (self)
        Pesanan pesanan = pesananRestService.getPesananById(idPesanan);     // get pesanan yang bersangkutan

        ValidasiStatus validasiStatus = new ValidasiStatus();   // buat objek validasi
        validasiStatus.setPengunggah(user);                     // set pengunggah
        validasiStatus.setPesanan(pesanan);                     // set pesanan
        validasiStatus.setWaktuValidasi(LocalDate.now());       // set waktu
        validasiStatus.setFileValidasi(publicURL);              // set url file validasi
        validasiStatus.setCatatan(catatan);                     // set catatan

        pesananRestService.updateStatusPesanan(pesanan, validasiStatus);
        if (!hasil.equalsIgnoreCase("")) {
            pesananRestService.updateHasilPesanan(pesanan, hasil);
        }

        Map<String, String> response = new HashMap<>();
        response.put("publicURL", publicURL);
        return new ResponseEntity<Map<String, String>>(response, HttpStatus.CREATED);
    }
}
