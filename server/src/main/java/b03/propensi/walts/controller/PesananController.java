package b03.propensi.walts.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import java.time.format.DateTimeFormatter;

import b03.propensi.walts.model.*;
import b03.propensi.walts.rest.*;
import b03.propensi.walts.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/api/pesanan")
public class PesananController {
    @Autowired
    PesananRestService pesananRestService;

    @Autowired
    PenggunaRestService penggunaRestService;

    @Autowired
    ProdukPesananRestService produkPesananRestService;

    @Autowired
    NotifRestService notifRestService;

    @Autowired
    private FileStorageRestService fileStorageRestService;

    //create pesanan (hold :perlu kejelasan hubungan produk dan pesanan)
    @PostMapping(value = "/create")
    public ResponseEntity<Result<Pesanan>> registerPesanan(
            @RequestBody PesananRest pesanan,
            HttpServletRequest request
    ) throws Exception {
        JsonResponse<Pesanan> result = new JsonResponse<Pesanan>();
        try {
            Pengguna vendor = penggunaRestService.getPenggunaById(pesanan.getVendor());
            System.out.println("jumlah sebelum" + vendor.getListPesanan().size());
            pesananRestService.createPesanan(pesanan);
            // (New) for dashboard
            vendor.setJumlahPesanan((long) vendor.getListPesanan().size() + 1);
            System.out.println("jumlah sesudah" + vendor.getListPesanan().size());
            penggunaRestService.updatePengguna(vendor);
        } catch (Exception e) {
            System.out.println("errorrr");
            System.out.println(e.getMessage());
            return result.response(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return result.response("Pesanan berhasil dibuat!");
    }

    // list pesanan
    @GetMapping(value = "/all")
    public ResponseEntity<Result<List<Pesanan>>> getAllPesanan() throws Exception {
        JsonResponse<List<Pesanan>> result = new JsonResponse<List<Pesanan>>();
        List<Pesanan> listPesanan = pesananRestService.getAllPesanan();
        notifRestService.checkIsNotifTelat();
        System.out.println("SEMUA PESANAN YANG ADA");
        System.out.println(listPesanan);
        return result.response("List pesanan", listPesanan);

    }

    // Read detail pesanan
    @GetMapping(value = "detail/{idPesanan}")
    public ResponseEntity<Result<Pesanan>> getDetailPesanan(
        @PathVariable Long idPesanan,
        HttpServletRequest request
    ){
        JsonResponse<Pesanan> result = new JsonResponse<Pesanan>();
        Pesanan pesanan = pesananRestService.getPesananById(idPesanan);
        System.out.println("LIST PRODUK~ : " + pesanan.getListProduk());
        System.out.println("LIST PENGGUNA (size)~ : " + pesanan.getListPengguna().size());

        for (Produk p : pesanan.getListProduk()) {
            System.out.println(p.getNamaProduk() );

        }
        return result.response("Data pesanan", pesanan);


    }

    // Update detail pesanan
    @PutMapping(value = "update/{idPesanan}")
    public ResponseEntity<Result<Pesanan>> updateDetailPesanan(
        @PathVariable Long idPesanan,
        @RequestBody UpdatePesananRest pesananDiubah,
        HttpServletRequest request
    ){
        System.out.println("UBAH PESANAN");

        Pesanan pesanan = pesananRestService.getPesananById(idPesanan);

        System.out.println("CATATAN BARUUU: " + pesananDiubah.getCatatan());
        pesanan.setCatatan(pesananDiubah.getCatatan());


        /*DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate tglPengemasan = LocalDate.parse(pesananDiubah.getDeadlinePengemasan(), formatter);
        LocalDate tglPengiriman = LocalDate.parse(pesananDiubah.getDeadlinePengiriman(), formatter);

        pesanan.setDeadlinePengemasan(tglPengemasan);
        pesanan.setDeadlinePengiriman(tglPengiriman);*/

        System.out.println("PESANAN: " + pesanan.getIdPesanan());
        
        pesananRestService.updatePesanan(pesanan);

        JsonResponse<Pesanan> result = new JsonResponse<Pesanan>();
        System.out.println("PESANAN: " + pesanan.getCatatan());

        return result.response("Pesanan berhasil diperbarui!", pesanan);

    }

    // Update status pesanan (asumsi: status diubah setelah bukti di-submit)
    @PutMapping(value = "status/{idPesanan}")
    public ResponseEntity<Result<Pesanan>> updateStatusPesanan(
            @PathVariable Long idPesanan,
            @RequestBody FormValidasiStatus formValidasiStatus,
            HttpServletRequest request
    ){
        Pengguna user =  penggunaRestService.getPenggunaFromJWT(request); // (belum tau bisa jalan apa tdk)

        Pesanan pesanan = pesananRestService.getPesananById(idPesanan);
        ValidasiStatus validasiStatus = new ValidasiStatus();
        validasiStatus.setFileValidasi(formValidasiStatus.getFileValidasi());
//        validasiStatus.setPengunggah(user);
        validasiStatus.setPesanan(pesanan);
        validasiStatus.setWaktuValidasi(LocalDate.now());

        pesananRestService.updateStatusPesanan(pesanan, validasiStatus);



        JsonResponse<Pesanan> result = new JsonResponse<Pesanan>();

        return result.response("Status pesanan berhasil diperbarui!", pesanan);
    }

  


  

    @GetMapping(value = "detail/{idPesanan}/produk")
    public ResponseEntity<Result<List<Produk>>> getProdukPesanan(
            @PathVariable Long idPesanan,
            HttpServletRequest request
    ){
        JsonResponse<List<Produk>> result = new JsonResponse<>();
        Pesanan pesanan = pesananRestService.getPesananById(idPesanan);
        return result.response("List produk pesanan", pesanan.getListProduk());
    }

    // Read bukti pesanan
    @GetMapping(value = "detail/{idPesanan}/bukti")
    public ResponseEntity<Result<List<ValidasiStatus>>> getBuktiValidasiPesanan(
            @PathVariable Long idPesanan,
            HttpServletRequest request
    ){
        JsonResponse<List<ValidasiStatus>> result = new JsonResponse<>();
        Pesanan pesanan = pesananRestService.getPesananById(idPesanan);
        return result.response("List produk pesanan", pesanan.getListValidasi());
    }

    @PutMapping(value = "updateOpened/{idPesanan}")
    public ResponseEntity<Result<Pesanan>> updateNotifStatusPesanan(
            @PathVariable Long idPesanan,
            HttpServletRequest request
    ){

        Pesanan pesanan = pesananRestService.getPesananById(idPesanan);
        pesananRestService.openNotif(idPesanan);

        JsonResponse<Pesanan> result = new JsonResponse<Pesanan>();
        return result.response("Update status pesanan sudah dilihat!", pesanan);
    }

    @PutMapping(value = "closeNotif/{idNotifikasi}")
    public ResponseEntity<Result<Pesanan>> closeNotifPesanan(
            @PathVariable Long idNotifikasi,
            HttpServletRequest request
    ){

        Notifikasi notif = notifRestService.findNotifikasiById(idNotifikasi);
        notifRestService.isOpen(idNotifikasi);

        JsonResponse<Pesanan> result = new JsonResponse<Pesanan>();
        return result.response("Notifikasi disembunyikan");
    }

    // list nama produk
    @GetMapping(value = "/periode")
    public ResponseEntity<Result<List<Object>>> getAllKategori() throws Exception {
        JsonResponse<List<Object>> result = new JsonResponse<>();
        List<Object> listPesananPeriode = pesananRestService.getTotalPesananByMonth();
        produkPesananRestService.updateProdukPesananSementara();
        return result.response("List  kategori", listPesananPeriode);
    }

}
