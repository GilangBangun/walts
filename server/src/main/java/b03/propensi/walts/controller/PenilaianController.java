package b03.propensi.walts.controller;

import b03.propensi.walts.model.Pengguna;
import b03.propensi.walts.model.Penilaian;
import b03.propensi.walts.model.Pesanan;
import b03.propensi.walts.rest.JsonResponse;
import b03.propensi.walts.rest.Result;
import b03.propensi.walts.service.PenggunaRestService;
import b03.propensi.walts.service.PenilaianRestService;
import b03.propensi.walts.service.PesananRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/penilaian")
public class PenilaianController {

    @Autowired
    PenilaianRestService penilaianRestService;

    @Autowired
    PenggunaRestService penggunaRestService;

    @Autowired
    PesananRestService pesananRestService;

    // Create penilaian
    @PostMapping(value = "/{idPengguna}/{idPesanan}")
    public ResponseEntity<Result<Penilaian>> createPenilaian(
            @PathVariable String idPengguna, // penerima nilai (vendor)
            @PathVariable Long idPesanan, // pesanan yang dinilai
            @RequestBody Penilaian penilaian,
            HttpServletRequest request
    ) throws Exception {
        JsonResponse<Penilaian> result = new JsonResponse<Penilaian>();
        List<Pengguna> daftarPengguna = new ArrayList<Pengguna>();
        try {
            Pengguna user =  penggunaRestService.getPenggunaFromJWT(request); // pemberi nilai
            Pengguna penerima = penggunaRestService.getPenggunaById(idPengguna);
            // Pengguna user =  penggunaRestService.getPenggunaById(idPengguna);    // sementara (utk cek di postman)

            Pesanan pesanan = pesananRestService.getPesananById(idPesanan);
            pesanan.setStatus("Sudah Dinilai"); // update status terakhir (hilang dari dashboard)
            pesananRestService.updatePesanan(pesanan);

            daftarPengguna.add(penerima);
            daftarPengguna.add(user);
            System.out.println("Index 0 " + daftarPengguna.get(0).getNamePengguna());

            penilaian.setListPenerimaPenilai(daftarPengguna);
            penilaian.setWaktu(LocalDate.now());
//            penilaian.setPesanan(pesanan);  // (HOLD) set pesanan yang dinilai
            penilaian.setPesanan(idPesanan);
            penilaian.setKodePesanan(pesanan.getKode());
            penilaianRestService.createPenilaian(penilaian);

            //untuk set rata2 penilaian di vendor
            List<Penilaian> listPenilaian = penerima.getListPenilaian();
            int rataPenilaianSementara = 0;
            for (Penilaian nilai: listPenilaian
            ) {
                rataPenilaianSementara += nilai.getNilai();
            }
            long rataPenilaianAkhir = rataPenilaianSementara/penerima.getListPenilaian().size();
            penerima.setRataPenilaian(rataPenilaianAkhir);
            penggunaRestService.updatePengguna(penerima);
        } catch (Exception e) {
            return result.response(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return result.response("Penilaian berhasil disimpan!", penilaian);
    }

}
